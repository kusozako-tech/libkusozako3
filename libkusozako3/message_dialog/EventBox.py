
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .event_controllers.EventControllers import DeltaEventControllers
from .widgets.Widgets import EchoWidgets


class DeltaEventBox(Gtk.EventBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.add(widget)

    def _delta_info_event_box(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self._box.set_border_width(Unit(1))
        self.add(self._box)
        DeltaEventControllers(self)
        EchoWidgets(self)
        self._raise("delta > add to container", self)
