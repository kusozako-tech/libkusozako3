
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _on_realize(self, label):
        self.set_markup(self._enquiry("delta > model", "message"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            vexpand=True,
            wrap=True,
            selectable=True,
            use_markup=True,
            margin=Unit(2),
            justify=Gtk.Justification.CENTER
            )
        self.set_markup(self._enquiry("delta > model", "message"))
        # self.connect("realize", self._on_realize)
        self._raise("delta > add to container", self)
