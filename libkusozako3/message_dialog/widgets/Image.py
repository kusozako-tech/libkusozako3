
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

ICON_SIZE = Gtk.IconSize.DIALOG


class DeltaImage(DeltaEntity):

    def _try_set_icon_name(self):
        icon_name = self._enquiry("delta > model", "icon-name")
        if icon_name is None:
            return
        image = Gtk.Image.new_from_icon_name(icon_name, ICON_SIZE)
        pixel_size = self._enquiry("delta > model", "pixel-size")
        if pixel_size is not None:
            image.set_pixel_size(pixel_size)
        image.set_margin_top(Unit(2))
        self._raise("delta > add to container", image)

    def __init__(self, parent):
        self._parent = parent
        self._try_set_icon_name()
