
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


class FoxtrotModel:

    def __getitem__(self, key):
        return self._model[key] if key in self._model else None

    def _set_default_value(self, key, default_value):
        if key not in self._model:
            self._model[key] = default_value

    def __init__(self, base_model):
        self._model = base_model.copy()
        self._set_default_value("default-response", 0)
        self._set_default_value("message", "")
        self._set_default_value("buttons", (_("Close"),))
