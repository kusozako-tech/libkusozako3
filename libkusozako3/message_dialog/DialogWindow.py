
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .EventBox import DeltaEventBox


class DeltaDialogWindow(Gtk.Dialog, DeltaEntity):

    def _delta_call_dialog_response(self, response):
        self.response(response)

    def _delta_call_add_to_container(self, widget):
        container = self.get_content_area()
        container.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Dialog.__init__(
            self,
            "",
            self._enquiry("delta > application window"),
            self._enquiry("delta > model", "default-response")
            )
        content_area = self.get_content_area()
        content_area.set_border_width(0)
        self._raise("delta > css", (content_area, "kusozako-headerbar"))
        self.set_decorated(False)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_keep_above(True)
        self.set_size_request(Unit(40), Unit(40))
        self.stick()
        DeltaEventBox(self)
        self.show_all()
