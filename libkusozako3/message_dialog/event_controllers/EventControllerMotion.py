
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .EdgeType import FoxtrotEdgeType
from .CursorPointer import FoxtrotCursorPointer

POINTER_NAMES = [
    ["nw-resize", "n-resize", "ne-resize"],
    ["w-resize", "default", "e-resize"],
    ["sw-resize", "s-resize", "se-resize"]
    ]


class DeltaEventControllerMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _on_motion(self, event_controller, x, y, widget):
        name = self._edge_type.get(x, y)
        self._cursor_pointer.set_for_name(name)

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry("delta > event box")
        self._edge_type = FoxtrotEdgeType(event_source, POINTER_NAMES)
        self._cursor_pointer = FoxtrotCursorPointer(event_source)
        Gtk.EventControllerMotion.__init__(self, widget=event_source)
        self.connect("motion", self._on_motion, event_source)
