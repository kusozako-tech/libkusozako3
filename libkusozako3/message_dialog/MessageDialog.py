
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .DialogWindow import DeltaDialogWindow
from .Model import FoxtrotModel


class DeltaMessageDialog(Gtk.Dialog, DeltaEntity):

    @classmethod
    def run_for_model(cls, parent, model):
        dialog = cls(parent)
        return dialog.set_model(model)

    def _delta_info_model(self, key):
        return self._model[key]

    def set_model(self, model):
        self._model = FoxtrotModel(model)
        dialog_window = DeltaDialogWindow(self)
        response = dialog_window.run()
        dialog_window.destroy()
        return response

    def __init__(self, parent):
        self._parent = parent
