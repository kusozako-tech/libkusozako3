
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math
from gi.repository import Pango
from gi.repository import PangoCairo
from libkusozako3.Ux import Unit
from libkusozako3.gtk_font import GtkFont

COLOR = (0.5, 1, 0, 1)
TILE_SIZE = Unit(16)
SIZE_04 = TILE_SIZE*0.4
SIZE_06 = TILE_SIZE*0.6
SIZE_10 = TILE_SIZE
OFFSET_RATE = 1/math.sqrt(2)
FONT_DESCRIPTION = GtkFont.get_description(relative_size=-1, weight=500)
TRIANGLE_HEIGHT = TILE_SIZE*0.6
TRIANGLE_WIDTH = math.sqrt((TRIANGLE_HEIGHT*TRIANGLE_HEIGHT)*2)
ANGLE = math.radians(45)


class FoxtrotSymlinkTag:

    def _paint_shade(self, cairo_context):
        cairo_context.set_source_rgba(*COLOR)
        cairo_context.move_to(SIZE_10, 0)
        cairo_context.line_to(SIZE_10, SIZE_06)
        cairo_context.line_to(SIZE_04, 0)
        cairo_context.fill()

    def _get_offset_positions(self, height):
        offset = height*OFFSET_RATE
        return SIZE_04+offset, 0-offset

    def _get_triangle_layout(self, cairo_context, markup):
        layout = PangoCairo.create_layout(cairo_context)
        layout.set_alignment(Pango.Alignment.CENTER)
        layout.set_font_description(FONT_DESCRIPTION)
        layout.set_width(TRIANGLE_WIDTH*Pango.SCALE)
        layout.set_height(-1)
        layout.set_wrap(Pango.WrapMode.WORD_CHAR)
        layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        layout.set_markup(markup)
        return *layout.get_pixel_size(), layout

    def _paint_markup(self, cairo_context, markup):
        self._paint_shade(cairo_context)
        _, height, layout = self._get_triangle_layout(cairo_context, markup)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(*self._get_offset_positions(height))
        cairo_context.save()
        cairo_context.rotate(ANGLE)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)
        cairo_context.restore()

    def paint(self, cairo_context, file_info):
        if file_info.get_is_symlink():
            self._paint_markup(cairo_context, "SYMLINK")
