
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Ux import Unit

THUMBNAL_SIZE = Unit(16)
INTERP = GdkPixbuf.InterpType.BILINEAR


class AlfaImage:

    def _get_scaled(self, gio_file, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(1, max(THUMBNAL_SIZE/width, THUMBNAL_SIZE/height))
        thumbnail = pixbuf.scale_simple(width*scale, height*scale, INTERP)
        primary_info = "{}x{}".format(width, height)
        thumbnail.set_option("tEXt::Kusozako::PrimaryInfo", primary_info)
        thumbnail.set_option("tExt::Thumb::Width", str(width))
        thumbnail.set_option("tExt::Thumb::Height", str(height))
        thumbnail.set_option("tEXt::Thumb::URI", gio_file.get_uri())
        file_info = gio_file.query_info("time::modified", 0)
        mtime = file_info.get_attribute_as_string("time::modified")
        thumbnail.set_option("tEXt::Thumb::MTime", mtime)
        return thumbnail

    def _save_async(self, pixbuf, thumbnail_path):
        pixbuf.savev(thumbnail_path, "png", [], [])

    def get_thumbnail(self, gio_file, thumbnail_path):
        raise NotImplementedError
