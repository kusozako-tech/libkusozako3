
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.mime_type import MimeType
from .ImageWebP import FoxtrotImageWebP
# from .ImageSvg import FoxtrotImageSvg
from .ImageGeneric import FoxtrotImageGeneric
from .Audio import FoxtrotAudio
from .video.Video import FoxtrotVideo
from .pdf.Pdf import FoxtrotPdf

CHECK_LIST = {
    "image/webp": "_image_webp",
    "image": "_image_generic",
    "audio": "_audio",
    "video": "_video",
    "application/pdf": "_pdf"
    }


class FoxtrotBuilders:

    def _get_builder(self, gio_file):
        mime_type = MimeType.from_gio_file(gio_file)
        for key, builder_name in CHECK_LIST.items():
            if mime_type.startswith(key):
                return getattr(self, builder_name)
        return None

    def build_(self, gio_file, thumbnail_path):
        builder = self._get_builder(gio_file)
        if builder is None:
            return None
        return builder.get_thumbnail(gio_file, thumbnail_path)

    def __init__(self):
        self._image_webp = FoxtrotImageWebP()
        self._image_generic = FoxtrotImageGeneric()
        self._audio = FoxtrotAudio()
        self._video = FoxtrotVideo()
        self._pdf = FoxtrotPdf()
