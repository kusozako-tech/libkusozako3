
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Ux import Unit
from libkusozako3.audio_thumbnail.AudioThumbnail import FoxtrotAudioThumbnail

THUMBNAIL_SIZE = Unit(16)
INTERP = GdkPixbuf.InterpType.BILINEAR


class FoxtrotAudio:

    def _get_scaled(self, gio_file, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(1, max(THUMBNAIL_SIZE/width, THUMBNAIL_SIZE/height))
        thumbnail = pixbuf.scale_simple(width*scale, height*scale, INTERP)
        thumbnail.set_option("tEXt::Thumb::URI", gio_file.get_uri())
        file_info = gio_file.query_info("time::modified", 0)
        mtime = file_info.get_attribute_as_string("time::modified")
        thumbnail.set_option("tEXt::Thumb::MTime", mtime)
        return thumbnail

    def get_thumbnail(self, gio_file, thumbnail_path):
        uri = gio_file.get_uri()
        pixbuf = self._audio_thumbnail.get_for_uri(uri)
        if pixbuf is None:
            return None
        thumbnail = self._get_scaled(gio_file, pixbuf)
        thumbnail.savev(thumbnail_path, "png", [], [])
        return thumbnail

    def __init__(self):
        self._audio_thumbnail = FoxtrotAudioThumbnail.new_for_maximum_size(
            THUMBNAIL_SIZE
            )
