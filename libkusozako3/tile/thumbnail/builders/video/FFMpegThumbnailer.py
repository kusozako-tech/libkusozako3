
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

DEFAULT_POSITION = 10   # percent for video duration


class FoxtrotFFMpegThumbnailer:

    def _call_sync(self, position, video_context):
        args = [
            "ffmpegthumbnailer",
            "-i", video_context.input_path,
            "-o", video_context.output_path,
            "-t", "{}%".format(position),
            "-s", video_context.thumbnail_size_in_string
            ]
        subprocess = Gio.Subprocess.new(args, Gio.SubprocessFlags.NONE)
        success, _, _ = subprocess.communicate_utf8()
        return success

    def try_create_preview(self, video_context, position=DEFAULT_POSITION):
        if not video_context.has_video_track:
            return False
        return self._call_sync(position, video_context)
