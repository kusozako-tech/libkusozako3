
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from gi.repository import GLib
from gi.repository import GdkPixbuf
# from kusozako_files import Unit
from libkusozako3 import Ux


class FoxtrotVideoContext:

    def _try_get_video_track(self, input_path):
        media_info = MediaInfo.parse(input_path)
        for track in media_info.tracks:
            if track.track_type == "Video":
                return track
        return None

    def _set_thumbnail_size(self, video_track=None):
        if video_track is None:
            return 0
        edge_length = max(video_track.width, video_track.height)
        # scale = Unit.TILE_SIZE/min(video_track.width, video_track.height)
        scale = Ux.TILE_SIZE/min(video_track.width, video_track.height)
        return edge_length*scale

    def get_pixbuf(self):
        try:
            return GdkPixbuf.Pixbuf.new_from_file(self._output_path)
        except GLib.Error:
            print("broken file ?", self._input_path)
            return None

    def _get_readable(self, millisecond):
        total_seconds = millisecond/1000
        seconds = int(total_seconds % 60)
        if 10 > seconds:
            seconds = "0"+str(seconds)
        return "{}:{}".format(int(total_seconds//60), seconds)

    def __init__(self, input_path, output_path):
        self._input_path = input_path
        self._output_path = output_path
        video_track = self._try_get_video_track(input_path)
        self._has_video_track = video_track is not None
        self._thumbnail_size = self._set_thumbnail_size(video_track)

    @property
    def input_path(self):
        return self._input_path

    @property
    def output_path(self):
        return self._output_path

    @property
    def thumbnail_size_in_string(self):
        return str(self._thumbnail_size)

    @property
    def has_video_track(self):
        return self._has_video_track
