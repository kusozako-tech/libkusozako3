
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

DEFAULT_SUB_DIRECTORY = "kusozako-files"
DEFAULT_SUFFIX = ".png"


class FoxtrotSafePath:

    def get_safe_path(self, suffix=DEFAULT_SUFFIX):
        while True:
            basename = str(GLib.random_int())+str(GLib.random_int())
            names = [self._cache_directory, basename+suffix]
            safe_path = GLib.build_filenamev(names)
            if not GLib.file_test(safe_path, GLib.FileTest.EXISTS):
                return safe_path

    def __init__(self, sub_directory=DEFAULT_SUB_DIRECTORY):
        names = [GLib.get_tmp_dir(), sub_directory]
        self._cache_directory = GLib.build_filenamev(names)
        if not GLib.file_test(self._cache_directory, GLib.FileTest.EXISTS):
            gio_file = Gio.File.new_for_path(self._cache_directory)
            gio_file.make_directory_with_parents(None)
