
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from .Directories import FoxtrotDirectories
from .builders.Builders import FoxtrotBuilders


class FoxtrotThumbnail:

    def _get_mtime(self, gio_file):
        file_info = gio_file.query_info("time::modified", 0)
        return file_info.get_attribute_uint64("time::modified")

    def _is_valid(self, gio_file, thumbnail_path):
        if not GLib.file_test(thumbnail_path, GLib.FileTest.EXISTS):
            return False
        source_mtime = self._get_mtime(gio_file)
        thumbnail_gio_file = Gio.File.new_for_path(thumbnail_path)
        thumbnail_mtime = self._get_mtime(thumbnail_gio_file)
        is_valid = (thumbnail_mtime > source_mtime)
        if not is_valid:
            thumbnail_gio_file.delete(None)
        return is_valid

    def load_from_gio_file(self, gio_file):
        thumbnail_path = self._directories.get_thumbnail_path(gio_file)
        if self._is_valid(gio_file, thumbnail_path):
            return GdkPixbuf.Pixbuf.new_from_file(thumbnail_path)
        else:
            return self._builders.build_(gio_file, thumbnail_path)

    def __init__(self):
        self._directories = FoxtrotDirectories()
        self._builders = FoxtrotBuilders()
