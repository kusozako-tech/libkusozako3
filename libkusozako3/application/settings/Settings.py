
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from libkusozako3.application.css.Css import DeltaCss
from .KeyFile import FoxtrotKeyFile


class DeltaSettings(DeltaEntity):

    def _delta_call_register_settings_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_settings(self, user_data):
        self._key_file.set(*user_data)
        self._transmitter.transmit(user_data)

    def _delta_call_settings_transaction(self, user_data):
        self._key_file.set(*user_data)

    def _delta_call_settings_queue(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_settings(self, user_data):
        return self._key_file.get(*user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        query = "delta > user resource path", "application.conf"
        path = self._enquiry(*query)
        self._key_file = FoxtrotKeyFile(path)
        DeltaCss(self)
        self._key_file.save_to_file(path)
