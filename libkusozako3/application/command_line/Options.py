
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Files import DeltaFiles
from .OptionValues import DeltaOptionValues


class DeltaOptions(DeltaEntity):

    def _on_command_line(self, application, command_line):
        options = command_line.get_options_dict()
        self._files.parse(command_line)
        self._option_values.parse(options)
        if not options.lookup_value("version"):
            application.activate()
        else:
            version = self._enquiry("delta > application data", "version")
            print("delta > version :", version)
        return 0

    def get_files(self):
        return self._files.get_files()

    def get_option(self, key=None):
        return self._option_values.get_option(key)

    def get_is_non_gui(self):
        return self._option_values.get_is_non_gui()

    def __init__(self, parent):
        self._parent = parent
        for main_option in self._enquiry("delta > command line options"):
            self._raise("delta > add main option", main_option)
        gtk_application = self._enquiry("delta > gtk application")
        gtk_application.set_option_context_summary(
            self._enquiry("delta > application data", "long-description")
            )
        self._files = DeltaFiles(self)
        self._option_values = DeltaOptionValues(self)
        gtk_application.connect("command-line", self._on_command_line)
