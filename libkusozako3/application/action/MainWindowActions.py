
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity

MESSAGES = {
    "main-window.toggle-maximized": "delta > main window toggle maximized",
    "main-window.toggle-fullscreen": "delta > main window toggle fullscreen",
    "main-window.close": "delta > quit"
    }


class DeltaMainWindowActions(DeltaEntity):

    def receive_transmission(self, action_id):
        if not action_id.startswith("main-window"):
            return
        if action_id in MESSAGES:
            self._raise(MESSAGES[action_id])

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
