
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .AccelKey import DeltaAccelKey
from .MainWindowActions import DeltaMainWindowActions
from .PropertyAction import DeltaPropertyAction


class DeltaAction(DeltaEntity):

    def _delta_call_action(self, action_id):
        self._transmitter.transmit(action_id)

    def _delta_call_register_action_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_accel_enabled(self, enabled):
        self._enabled = enabled

    def _delta_info_accel_enabled(self):
        return self._enabled

    def _delta_call_register_action(self, user_data):
        action_id, shortcut, description = user_data
        self._accel_key.register_action(action_id, shortcut)

    def __init__(self, parent):
        self._parent = parent
        self._enabled = True
        self._transmitter = FoxtrotTransmitter()
        self._accel_key = DeltaAccelKey(self)
        DeltaMainWindowActions(self)
        DeltaPropertyAction(self)
