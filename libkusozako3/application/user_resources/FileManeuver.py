
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

COPY_FLAGS = Gio.FileCopyFlags


class FoxtrotFileManeuver:

    def _get_destination_file(self, directory, source_path):
        basename = GLib.path_get_basename(source_path)
        unsafe_path = GLib.build_filenamev([directory, basename])
        if GLib.file_test(unsafe_path, GLib.FileTest.EXISTS):
           return None
        return Gio.File.new_for_path(unsafe_path)

    def _copy_directory(self, directory, source_path):
        dest_file = self._get_destination_file(directory, source_path)
        if dest_file is None:
            return
        dest_file.make_directory(None)
        dest_dir = dest_file.get_path()
        source_file = Gio.File.new_for_path(source_path)
        for file_info in source_file.enumerate_children("*", 0):
            names = [source_path, file_info.get_name()]
            path = GLib.build_filenamev(names)
            self.copy_recursive_async(dest_dir, path)

    def _copy_file(self, directory, source_path):
        source_file = Gio.File.new_for_path(source_path)
        dest_file = self._get_destination_file(directory, source_path)
        if dest_file is None:
            return
        source_file.copy_async(
            dest_file,
            COPY_FLAGS.ALL_METADATA | COPY_FLAGS.NOFOLLOW_SYMLINKS,
            GLib.PRIORITY_DEFAULT,
            None,       # gio cancellable
            None,       # progress callback
            ("",),      # user data for progress call back
            None,
            ("",),      # user data for _on_copy_finished
            )

    def copy_recursive_async(self, directory, source_path):
        if GLib.file_test(source_path, GLib.FileTest.IS_REGULAR):
            self._copy_file(directory, source_path)
        else:
            self._copy_directory(directory, source_path)
