
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .Resources import DeltaResources


class AlfaApplication(DeltaEntity):

    def _delta_call_loopback_start_application_here(self, parent_object):
        pass

    def _delta_info_application_library_directory(self):
        return GLib.path_get_dirname(__file__)

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)

    def _delta_call_register_application_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self):
        self._parent = None
        print("delta > welcome back {}.".format(GLib.get_user_name()))
        self._transmitter = FoxtrotTransmitter()
        DeltaResources(self)
        print("delta > bye...")
