
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotSharedResource:

    def get_content(self):
        if self._path is None:
            return ""
        _, bytes = GLib.file_get_contents(self._path)
        return bytes.decode("utf-8")

    def _get_system_path(self, name):
        for directory in GLib.get_system_data_dirs():
            names = [directory, "libkusozako3", "data", name]
            path = GLib.build_filenamev(names)
            if GLib.file_test(path, GLib.FileTest.EXISTS):
                return path
        return None

    def __init__(self, name):
        directory = GLib.get_user_data_dir()
        names = [directory, "libkusozako3", "data", name]
        path = GLib.build_filenamev(names)
        if GLib.file_test(path, GLib.FileTest.EXISTS):
            self._path = path
        else:
            self._path = self._get_system_path(name)
