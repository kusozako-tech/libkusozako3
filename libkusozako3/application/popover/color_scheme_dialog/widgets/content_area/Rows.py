
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Button import DeltaButton


class DeltaRows(DeltaEntity):

    def _get_label(self, row_data):
        label = Gtk.Label(row_data["label"], hexpand=True)
        label.set_size_request(-1, Unit(5))
        label.set_xalign(1)
        label.set_margin_right(Unit(2))
        return label

    def _get_row(self, model):
        return (
            self._get_label(model),
            DeltaButton.new(self, model["fg_key"], model["fg_default"]),
            DeltaButton.new(self, model["bg_key"], model["bg_default"])
            )

    def _attach_to_grid(self, widget, geometries):
        self._raise("delta > attach to grid", (widget, geometries))

    def __init__(self, parent):
        self._parent = parent
        y_position = 0
        for row_data in self._enquiry("delta > default scheme"):
            y_position += 1
            label, fg_button, bg_button = self._get_row(row_data)
            self._attach_to_grid(label, (0, y_position, 2, 1))
            self._attach_to_grid(fg_button, (2, y_position, 1, 1))
            self._attach_to_grid(bg_button, (3, y_position, 1, 1))
