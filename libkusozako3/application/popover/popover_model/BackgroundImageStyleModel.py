
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

BACKGROUND_IMAGE_STYLE_MODEL = {
    "page-name": "background-image-style",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "settings",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action",
            "title": _("Auto"),
            "message": "delta > settings",
            "user-data": ("css", "window_background_size", "auto"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("css", "window_background_size", "auto"),
            "check-value": "auto"
        },
        {
            "type": "check-action",
            "title": _("Contain"),
            "message": "delta > settings",
            "user-data": ("css", "window_background_size", "contain"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("css", "window_background_size", "auto"),
            "check-value": "contain"
        },
        {
            "type": "check-action",
            "title": _("Cover"),
            "message": "delta > settings",
            "user-data": ("css", "window_background_size", "cover"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("css", "window_background_size", "auto"),
            "check-value": "cover"
        }
    ]
}
