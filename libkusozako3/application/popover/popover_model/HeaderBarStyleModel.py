
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODEL = {
    "page-name": "header-bar-style",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "settings",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action",
            "title": _("Bold"),
            "message": "delta > settings",
            "user-data": ("header-bar", "style", "bold"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("header-bar", "style", "bold"),
            "check-value": "bold"
        },
        {
            "type": "check-action",
            "title": _("Medium"),
            "message": "delta > settings",
            "user-data": ("header-bar", "style", "medium"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("header-bar", "style", "bold"),
            "check-value": "medium"
        },
        {
            "type": "check-action",
            "title": _("Thin"),
            "message": "delta > settings",
            "user-data": ("header-bar", "style", "thin"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("header-bar", "style", "bold"),
            "check-value": "thin"
        },
        {
            "type": "check-action",
            "title": _("None"),
            "message": "delta > settings",
            "user-data": ("header-bar", "style", "none"),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("header-bar", "style", "bold"),
            "check-value": "none"
        }
    ]
}
