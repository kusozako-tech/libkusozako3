
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea


class DeltaUI(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_add_action_widget_start(self, widget):
        self._action_widget_container.add(widget)

    def _delta_call_register_action_widget_container(self, container):
        self._action_widget_container = container

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
        DeltaHeaderBar(self)
        DeltaContentArea(self)
