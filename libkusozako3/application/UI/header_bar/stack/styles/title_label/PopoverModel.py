
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Copy Title to Clipboard"),
            "message": "delta > copy to clipboard",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

MODEL = [MAIN_PAGE]
