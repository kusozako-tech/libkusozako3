
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaMenuButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button, popover):
        popover.popup_for_align(button, 0.5, 1.0)

    def __init__(self, parent):
        self._parent = parent
        popover = self._enquiry("delta > application popover")
        image = Gtk.Image.new_from_icon_name(
            self._enquiry("delta > application data", "icon-name"),
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=image
            )
        self.props.tooltip_text = _("Main Menu")
        self.set_margin_start(1)
        self.set_margin_top(1)
        self.set_margin_bottom(1)
        self.connect("clicked", self._on_clicked, popover)
        self._raise("delta > pack start", self)
        self._raise("delta > css", (self, "popover-button"))
