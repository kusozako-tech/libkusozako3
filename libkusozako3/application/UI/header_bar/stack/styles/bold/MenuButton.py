
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaMenuButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button, popover):
        popover.popup_for_align(button, 0.5, 1.0)

    def __init__(self, parent):
        self._parent = parent
        popover = self._enquiry("delta > application popover")
        image = Gtk.Image.new_from_icon_name(
            "open-menu-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=image
            )
        self.connect("clicked", self._on_clicked, popover)
        self._raise("delta > pack end", self)
        self._raise("delta > css", (self, "popover-button"))
