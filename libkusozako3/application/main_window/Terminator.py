
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-question-symbolic",
    "message": _("Close {} ?"),
    "buttons": (_("Cancel"), _("Close Application"))
    }


class DeltaTerminator(DeltaEntity):

    def is_closable(self):
        message = DIALOG_MODEL["message"]
        application_name = self._enquiry("delta > application data", "name")
        DIALOG_MODEL["message"] = message.format(application_name)
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        return response != 0

    def __init__(self, parent):
        self._parent = parent
