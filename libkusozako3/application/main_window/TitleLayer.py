
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .StatusLayer import DeltaStatusLayer


class DeltaTitleLayer(DeltaEntity):

    def _delta_call_register_application_window_title_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_info_application_window_title(self):
        window = self._enquiry("delta > application window")
        return window.get_title()

    def _delta_call_application_window_title(self, title):
        window = self._enquiry("delta > application window")
        window.set_title(title)
        self._transmitter.transmit(("title", title))

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaStatusLayer(self)
