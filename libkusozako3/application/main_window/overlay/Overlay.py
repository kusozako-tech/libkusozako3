
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
# from .stack.Stack import DeltaStack


class DeltaOverlay(Gtk.Overlay, DeltaEntity):

    @classmethod
    def new_for_application_window_as_parent(cls, application_window):
        overlay = cls(application_window)
        application_window.add(overlay)
        return overlay

    def receive_transmission(self, state):
        print("state", state)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self)
        self._inactive = Gtk.Label("INACTIVE")
        self.add_overlay(self._inactive)
        self._raise("delta > register window state object", self)
