
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Terminator import DeltaTerminator
from .TitleLayer import DeltaTitleLayer
from .geometries.Geometries import DeltaGeometries
# from .overlay.Overlay import DeltaOverlay


class DeltaWindow(Gtk.ApplicationWindow, DeltaEntity):

    def _on_close(self, window, event):
        return not self._terminator.is_closable()

    def _delta_call_register_terminator_object(self, terminator):
        self._terminator = terminator

    def _delta_info_default_terminator(self):
        return self._terminator

    def _delta_call_add_to_container(self, widget):
        self.add(widget)
        # self._overlay.add_overlay(widget)

    def _delta_call_quit(self):
        self._geometries.save()
        self.close()

    def _delta_info_application_window(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.ApplicationWindow.__init__(
            self,
            application=self._enquiry("delta > gtk application"),
            title=self._enquiry("delta > application data", "name"),
            icon_name=self._enquiry("delta > application data", "icon-name"),
            decorated=False
            )
        self.connect("delete-event", self._on_close)
        self._geometries = DeltaGeometries(self)
        self._terminator = DeltaTerminator(self)
        # self._overlay = DeltaOverlay.new_for_application_window_as_parent(self)
        DeltaTitleLayer(self)
        self.present()
        self.show_all()
