
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from libkusozako3.application.main_window.Window import DeltaWindow
from .RecentPaths import DeltaRecentPaths


class DeltaFilePaths(DeltaEntity):

    def _delta_info_application_recent_paths(self):
        return self._recent_paths.get_paths()

    def _delta_call_application_recent_paths(self, path=None):
        if path is not None:
            self._recent_paths.append_path(path)
        self._current_path = path
        self._transmitter.transmit(("path", path))

    def _delta_info_application_current_path(self):
        return self._current_path

    def _delta_call_register_application_file_path_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._current_path = None
        self._recent_paths = DeltaRecentPaths(self)
        if self._enquiry("delta > non gui"):
            self._raise("delta > loopback start application non gui", self)
        else:
            DeltaWindow(self)
