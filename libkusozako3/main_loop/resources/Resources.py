
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from .user_resources.UserResources import DeltaUserResources
from .settings.Settings import DeltaSettings
from .css.Css import DeltaCss


class DeltaResources(DeltaEntity):

    def _delta_call_loopback_user_resources_ready(self, parent):
        DeltaSettings(parent)

    def _delta_call_loopback_settings_ready(self, parent):
        DeltaCss(parent)

    def _delta_info_resource_path(self, resource_name):
        return self._get_fullpath(resource_name)

    def _delta_info_resource_string(self, resource_name):
        path = self._get_fullpath(resource_name)
        _, contents = GLib.file_get_contents(path)
        return contents.decode("utf-8")

    def _delta_info_resource_pixbuf(self, resource_name):
        path = self._get_fullpath(resource_name)
        return GdkPixbuf.Pixbuf.new_from_file(path)

    def _get_fullpath(self, resource_name):
        return GLib.build_filenamev([self._directory, resource_name])

    def __init__(self, parent):
        self._parent = parent
        names = [
            self._enquiry("delta > application library directory"),
            "resources"
            ]
        self._directory = GLib.build_filenamev(names)
        DeltaUserResources(self)
