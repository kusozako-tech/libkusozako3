
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from .Paths import DeltaPaths


class DeltaUserResources(DeltaEntity):

    def _delta_info_user_resource_path(self, resource_name):
        return self._paths.get_path_for_resource_name(resource_name)

    def __init__(self, parent):
        self._parent = parent
        self._paths = DeltaPaths(self)
        gio_file = Gio.File.new_for_path(self._paths.source_directory)
        file_enumerator = gio_file.enumerate_children("*", 0)
        for file_info in file_enumerator:
            if file_info.get_is_hidden():
                continue
            gio_file = file_enumerator.get_child(file_info)
            self._paths.copy(gio_file)
        self._raise("delta > loopback user resources ready", self)
