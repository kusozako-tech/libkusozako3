
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .CssReplacements import DeltaCssReplacements


class DeltaCss(DeltaEntity):

    def _delta_call_css(self, user_data):
        widget, css_class = user_data
        style_context = widget.get_style_context()
        style_context.add_class(css_class)

    def __init__(self, parent):
        self._parent = parent
        DeltaCssReplacements(self)
        self._raise("delta > loopback resources ready", self)
