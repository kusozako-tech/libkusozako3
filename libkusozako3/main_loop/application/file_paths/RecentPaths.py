
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaRecentPaths(DeltaEntity):

    def _save(self, paths):
        data = "recent-paths", "application", paths
        self._raise("delta > settings", data)

    def append_path(self, path):
        if path in self._paths:
            duplicated_index = self._paths.index(path)
            del self._paths[duplicated_index]
        self._paths.insert(0, path)
        del self._paths[20:]
        self._save(self._paths.copy())

    def get_paths(self):
        paths = []
        for path in self._paths:
            if GLib.file_test(path, GLib.FileTest.EXISTS):
                paths.append(path)
        self._save(paths.copy())
        return paths.copy()

    def __init__(self, parent):
        self._parent = parent
        query = "recent-paths", "application", []
        self._paths = self._enquiry("delta > settings", query)
