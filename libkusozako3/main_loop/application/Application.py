
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from gi.repository import Gio
from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .file_paths.FilePaths import DeltaFilePaths
from .command_line.CommandLine import DeltaCommandLine
from .Unique import DeltaUnique

FLAG_UNIQUE = Gio.ApplicationFlags.HANDLES_COMMAND_LINE
FLAG_NON_UNIQUE = FLAG_UNIQUE | Gio.ApplicationFlags.NON_UNIQUE


class DeltaApplication(Gtk.Application, DeltaEntity):

    def _delta_call_application_force_quit(self):
        self.quit()

    def _delta_call_application_release(self):
        self.release()

    def _delta_call_application_hold(self):
        self.hold()

    def _delta_info_gtk_application(self):
        return self

    def _delta_call_add_main_option(self, user_data):
        self.add_main_option(*user_data)

    def _on_activate(self, application, parent):
        if DeltaUnique.can_activate(parent, application):
            DeltaFilePaths(parent)

    def _delta_call_loopback_command_line_ready(self, parent):
        self.connect("activate", self._on_activate, parent)
        self.activate()

    def __init__(self, parent):
        self._parent = parent
        self._non_gui = False
        if self._enquiry("delta > application data", "unique-application"):
            application_flag = FLAG_UNIQUE
        else:
            application_flag = FLAG_NON_UNIQUE
        Gtk.Application.__init__(
            self,
            application_id=self._enquiry("delta > application data", "id"),
            flags=application_flag
            )
        DeltaCommandLine(self)
        self.run(sys.argv)
