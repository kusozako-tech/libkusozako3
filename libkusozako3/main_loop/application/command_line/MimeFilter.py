
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType


class DeltaMimeFilter(DeltaEntity):

    def matches(self, gio_file):
        mime = MimeType.from_gio_file(gio_file)
        for filter_ in self._filters:
            if GLib.regex_match_simple(filter_, mime, 0, 0):
                return True
        return False

    def _get_filters_from_model(self):
        filters = self._enquiry("delta > application data", "mime-type")
        # "*" never matched with any param.
        if filters is None:
            return ["*"]
        if isinstance(filters, str):
            return [filters]
        return filters

    def __init__(self, parent):
        self._parent = parent
        self._filters = self._get_filters_from_model()
