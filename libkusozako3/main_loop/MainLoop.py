
# (c) copyright 2021-202, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from .resources.Resources import DeltaResources
from .application.Application import DeltaApplication
from .window.Window import DeltaWindow


class AlfaMainLoop(DeltaEntity):

    def _delta_call_loopback_resources_ready(self, parent):
        DeltaApplication(parent)

    def _delta_call_loopback_application_ready(self, parent):
        DeltaWindow(parent)

    def _delta_call_loopback_main_window_ready(self, parent):
        pass

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)

    def _on_startup(self):
        user_name = GLib.get_user_name()
        print("delta > welcome back {}.".format(user_name))

    def _on_finished(self):
        print("delta > bye...")

    def __init__(self):
        self._parent = None
        self._on_startup()
        DeltaResources(self)
        self._on_finished()
