
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaAccelKey(DeltaEntity):

    def _on_key_pressed(self, event_controller_key, keyval, keycode, state):
        if not self._enquiry("delta > accel enabled"):
            return
        label = Gtk.accelerator_get_label(keyval, state)
        if label in self._registry:
            param = self._registry[label], None
            self._raise("delta > action", param)

    def register_action(self, signal, shortcut):
        if isinstance(signal, tuple):
            signal, _ = signal
        self._registry[shortcut] = signal

    def __init__(self, parent):
        self._parent = parent
        self._registry = {}
        window = self._enquiry("delta > application window")
        self._controller_key = Gtk.EventControllerKey.new(window)
        self._controller_key.connect("key-pressed", self._on_key_pressed)
