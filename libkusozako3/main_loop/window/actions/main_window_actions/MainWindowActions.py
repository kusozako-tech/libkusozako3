
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ToggleMaximized import DeltaToggleMaximized
from .ToggleFullScreen import DeltaToggleFullScreen
from .Iconfy import DeltaIconfy
from .Close import DeltaClose


class EchoMainWindowActions:

    def __init__(self, parent):
        DeltaToggleMaximized(parent)
        DeltaToggleFullScreen(parent)
        DeltaIconfy(parent)
        DeltaClose(parent)
