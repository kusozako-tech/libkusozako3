
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from libkusozako3.Entity import DeltaEntity


class DeltaIconfy(DeltaEntity):

    def _action(self, param=None):
        window = self._enquiry("delta > application window")
        window.iconify()

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == ApplicationSignals.WINDOW_ICONFY:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
