
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .AccelKey import DeltaAccelKey
from .main_window_actions.MainWindowActions import EchoMainWindowActions


class DeltaActions(DeltaEntity):

    def _delta_call_action(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_action_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_accel_enabled(self, enabled):
        self._enabled = enabled

    def _delta_info_accel_enabled(self):
        return self._enabled

    def _delta_call_register_action(self, user_data):
        action_id, shortcut, description = user_data
        self._accel_key.register_action(action_id, shortcut)

    def __init__(self, parent):
        self._parent = parent
        self._enabled = True
        self._transmitter = FoxtrotTransmitter()
        self._accel_key = DeltaAccelKey(self)
        EchoMainWindowActions(self)
        self._raise("delta > loopback actions ready", self)
