
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODEL = {
    "page-name": "settings",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Color Scheme"),
            "message": "delta > application set color scheme",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "label",
            "title": _("Header Bar:")
        },
        {
            "type": "switcher",
            "title": _("Style"),
            "message": "delta > switch stack to",
            "user-data": "header-bar-style",
        },
        {
            "type": "scale",
            "label-format": _("Opacity: {:.0%}"),
            "maximum": 1,
            "minimum": 0.1,
            "round": 2,
            "message": "delta > settings",
            "user-data": ("css", "header_opacity"),
            "query": "delta > settings",
            "query-data": ("css", "header_opacity", 0.9)
        },
        {
            "type": "separator"
        },
        {
            "type": "label",
            "title": _("Background :")
        },
        {
            "type": "switcher",
            "title": _("Style"),
            "message": "delta > switch stack to",
            "user-data": "background-image-style",
        },
        {
            "type": "simple-action",
            "title": _("Select Background Image"),
            "message": "delta > application select background image",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "scale",
            "label-format": _("Opacity: {:.0%}"),
            "maximum": 1,
            "minimum": 0.1,
            "round": 2,
            "message": "delta > settings",
            "user-data": ("css", "window_opacity"),
            "query": "delta > settings",
            "query-data": ("css", "window_opacity", 0.9)
        }
    ]
}
