
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from .popover_model.PopoverModel import MODEL
from .BackgroundDialog import DeltaBackgroundDialog
from .AboutDialog import DeltaAboutDialog
from libkusozako3.color_scheme_dialog.ColorSchemeDialog import (
    DeltaColorSchemeDialog
    )


class DeltaPopover(DeltaEntity):

    def _delta_info_application_popover(self):
        return self._popover

    def _delta_call_application_popover_add_item(self, user_data):
        box_id, item_model = user_data
        self._popover.add_item_for_model(box_id, item_model)

    def _delta_call_application_popover_add_page(self, model):
        self._popover.add_page_for_model(model)

    def _delta_call_application_set_color_scheme(self):
        DeltaColorSchemeDialog.run_dialog(self)

    def _delta_call_application_select_background_image(self):
        DeltaBackgroundDialog.run_dialog(self)

    def _delta_call_about(self):
        DeltaAboutDialog.run_dialog(self)

    def __init__(self, parent):
        self._parent = parent
        self._popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        self._raise("delta > loopback popover ready", self)
