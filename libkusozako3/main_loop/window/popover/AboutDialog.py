
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from libkusozako3.Ux import Unit

DIALOG_MODEL = {
    "default-response": 0,
    "pixel-size": Unit(32),
    "message": "<b>About</b>",
    "buttons": (_("Close"),)
    }


class DeltaAboutDialog(DeltaEntity):

    @classmethod
    def run_dialog(cls, parent):
        if "_dialog" not in dir(cls):
            cls._dialog = cls(parent)
        model = cls._dialog.get_model()
        DeltaMessageDialog.run_for_model(parent, model)

    def get_model(self):
        return self._model

    def _get_data(self, key):
        return self._enquiry("delta > application data", key)

    def _construct_message(self):
        return "<span size='x-large'>{}</span>\nversion: {}\n\n{}".format(
            self._get_data("name"),
            self._get_data("version"),
            self._get_data("long-description")
            )

    def __init__(self, parent):
        self._parent = parent
        self._model = DIALOG_MODEL.copy()
        icon_name = self._enquiry("delta > application data", "icon-name")
        self._model["icon-name"] = icon_name
        self._model["message"] = self._construct_message()
