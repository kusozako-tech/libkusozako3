
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaPosition(DeltaEntity):

    def save(self):
        window = self._enquiry("delta > application window")
        x, y = window.get_position()
        position_data = "window", "position", [x, y]
        self._raise("delta > settings", position_data)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        query = "window", "position", ["center", "center"]
        position = self._enquiry("delta > settings", query)
        if position[0] == "center":
            window.set_position(Gtk.WindowPosition.CENTER)
        else:
            window.move(position[0], position[1])
