
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .bold.Bold import DeltaBold
from .medium.Medium import DeltaMedium
from .Thin import DeltaThin
from .None_ import DeltaNone


class EchoStyles:

    def __init__(self, parent):
        DeltaBold(parent)
        DeltaMedium(parent)
        DeltaThin(parent)
        DeltaNone(parent)
