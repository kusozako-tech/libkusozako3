
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea


class DeltaWidgets(DeltaEntity):

    def _delta_info_current_color(self):
        return self._current_color

    def _delta_call_color_changed(self, color):
        self._current_color = color

    def _delta_call_color_button_clicked(self, user_data):
        self._key, self._current_color = user_data
        self._header_bar.set_visible_child_name("color-chooser")
        self._content_area.set_visible_child_name("color-chooser")

    def _delta_call_chooser_choosed(self):
        self._header_bar.set_visible_child_name("main-content")
        self._content_area.set_visible_child_name("main-content")
        self._raise("delta > buffer changed", (self._key, self._current_color))

    def _delta_call_chooser_cancelled(self):
        self._header_bar.set_visible_child_name("main-content")
        self._content_area.set_visible_child_name("main-content")

    def __init__(self, parent):
        self._parent = parent
        self._key = None
        self._current_color = None
        self._header_bar = DeltaHeaderBar(self)
        self._content_area = DeltaContentArea(self)
