
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

LABELS = {0: _("Type"), 2: _("Foreground"), 3: _("Background")}


class DeltaHeaderLabels(DeltaEntity):

    def _get_label(self, text):
        label = Gtk.Label(text)
        label.set_xalign(0.5)
        label.set_size_request(-1, Unit(4))
        self._raise("delta > css", (label, "secondary-theme-color-class"))
        return label

    def _attach_to_grid(self, widget, geometries):
        self._raise("delta > attach to grid", (widget, geometries))

    def __init__(self, parent):
        self._parent = parent
        for x_position, text in LABELS.items():
            label = self._get_label(text)
            width = 2 if x_position == 0 else 1
            self._attach_to_grid(label, (x_position, 0, width, 1))
