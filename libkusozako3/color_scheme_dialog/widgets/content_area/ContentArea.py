
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Grid import DeltaGrid
from .ColorChooserWidget import DeltaColorChooserWidget


class DeltaContentArea(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            margin=Unit(2),
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT
            )
        DeltaGrid(self)
        DeltaColorChooserWidget(self)
        self._raise("delta > css", (self, "content-area"))
        self._raise("delta > add to container", self)
