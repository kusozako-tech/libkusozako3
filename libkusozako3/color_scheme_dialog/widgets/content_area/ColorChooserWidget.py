
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity


class DeltaColorChooserWidget(Gtk.ColorSelection, DeltaEntity):

    def _on_color_changed(self, color_selection):
        gdk_rgba = color_selection.get_current_rgba()
        self._raise("delta > color changed", gdk_rgba.to_string())

    def _on_map(self, color_chooser_widget):
        current_color = self._enquiry("delta > current color")
        gdk_rgba = Gdk.RGBA()
        gdk_rgba.parse(current_color)
        self.set_current_rgba(gdk_rgba)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ColorSelection.__init__(self)
        self.connect("color-changed", self._on_color_changed)
        self.connect("map", self._on_map)
        self._raise("delta > add to stack named", (self, "color-chooser"))
