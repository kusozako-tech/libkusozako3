
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

NORMAL_CONTAIN = "kusozako_thumbnail_normal_contain"
NORMAL_FILL = "kusozako_thumbnail_normal_fill"
