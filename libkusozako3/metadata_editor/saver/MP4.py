
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from mutagen.mp4 import MP4Cover
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity

KEYS = {"\xa9nam": "music-title", "\xa9ART": "artist", "\xa9alb": "album"}


class DeltaMP4(DeltaEntity):

    def _get_pixbuf(self, cover_art_path, size):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(cover_art_path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = max(size/width, size/height)
        return pixbuf.scale_simple(
            width*scale,
            height*scale,
            GdkPixbuf.InterpType.HYPER
            )

    def _try_set_coverart(self, mutagen_file):
        cover_art_path = self._enquiry("delta > model", "cover-art-path")
        if cover_art_path is None:
            return
        pixbuf = self._get_pixbuf(cover_art_path, 300)
        success, bytes_ = pixbuf.save_to_bufferv("png", [], [])
        if success:
            cover = MP4Cover(bytes_, MP4Cover.FORMAT_PNG)
            mutagen_file["covr"] = [cover]

    def apply(self, gio_file):
        source_path = gio_file.get_path()
        mutagen_file = mutagen.File(source_path)
        for mutagen_key, model_key in KEYS.items():
            model_value = self._enquiry("delta > model", model_key)
            if model_value:
                mutagen_file[mutagen_key] = model_value
        self._try_set_coverart(mutagen_file)
        mutagen_file.save()

    def __init__(self, parent):
        self._parent = parent
