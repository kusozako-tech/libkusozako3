
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

MODELS = (
    ("music-title", _("Title"), (1, 1, 1, 1)),
    ("artist", _("Artist"), (1, 2, 1, 1)),
    ("album", _("Album"), (1, 3, 1, 1)),
    )


class DeltaEntries(DeltaEntity):

    def _timeout(self, entry, key, timeout_index):
        if self._timeout_index == timeout_index:
            self._raise("delta > model", (key, entry.get_text()))

    def _on_changed(self, entry, key):
        self._timeout_index += 1
        GLib.timeout_add(200, self._timeout, entry, key, self._timeout_index)

    def _on_activate(self, entry, key):
        self._raise("delta > model", (key, entry.get_text()))
        self._raise("delta > dialog response", Gtk.ResponseType.APPLY)

    def _set_entry_for_model(self, model):
        key, placeholder_text, geometries = model
        entry = Gtk.Entry()
        entry.set_placeholder_text(placeholder_text)
        entry.props.margin = Unit(1)
        text = self._enquiry("delta > model", key)
        if text:
            entry.set_text(text)
        self._raise("delta > attach to grid", (entry, geometries))
        entry.connect("changed", self._on_changed, key)
        entry.connect("activate", self._on_activate, key)

    def __init__(self, parent):
        self._parent = parent
        self._timeout_index = 0
        for model in MODELS:
            self._set_entry_for_model(model)
