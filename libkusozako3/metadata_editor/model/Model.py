
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from . import FileValidation
from .Loader import DeltaLoader


class DeltaModel(DeltaEntity):

    @classmethod
    def new_for_gio_file(cls, parent, gio_file):
        if not FileValidation.check_for_gio_file(gio_file):
            return None
        model = cls(parent)
        model.set_gio_file(gio_file)
        return model

    def _delta_call_set_model_value(self, user_data):
        key, value = user_data
        self._values[key] = value

    def __setitem__(self, key, value):
        self._values[key] = value

    def __getitem__(self, key):
        return self._values.get(key, None)

    def set_gio_file(self, gio_file):
        self._values["gio-file"] = gio_file
        DeltaLoader.load_for_gio_file(self, gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._values = {"title": _("Edit Metadata"), "cover-art-path": None}
