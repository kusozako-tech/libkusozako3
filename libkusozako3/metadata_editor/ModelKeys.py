
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

COVER_ART = "cover-art"
COVER_ART_URI = "cover-art-uri"
MUSIC_TITLE = "music-title"
ARTIST = "artist"
ALBUM = "album"
