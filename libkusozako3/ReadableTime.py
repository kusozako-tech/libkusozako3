
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

TIMESPAN_NANOSECOND = pow(1000, 3)


def from_nanoseconds(total_nanoseconds):
    seconds = total_nanoseconds/TIMESPAN_NANOSECOND
    return from_seconds(seconds)


def from_seconds(total_seconds):
    hours = int(total_seconds // 3600)
    minutes = int(total_seconds % 3600 // 60)
    seconds = int(total_seconds % 60)
    if hours == 0:
        return "{}:{:02}".format(minutes, seconds)
    return "{}:{:02}:{:02}".format(hours, minutes, seconds)
