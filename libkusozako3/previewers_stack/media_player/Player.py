
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from libkusozako3.Entity import DeltaEntity
from .UI.UI import DeltaUI
from .playbin.Playbin import DeltaPlaybin

SEEK_FLAGS = Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT


class DeltaPlayer(DeltaEntity):

    def _delta_call_drawing_area_realized(self, handle):
        self._playbin = self._playbin_factory.get_for_handle(handle)
        self._play()

    def _delta_call_seek(self, rate):
        _, duration = self._playbin.query_duration(Gst.Format.TIME)
        self._playbin.seek_simple(Gst.Format.TIME, SEEK_FLAGS, duration*rate)

    def _delta_call_stream_finished(self):
        self._playbin.seek_simple(Gst.Format.TIME, SEEK_FLAGS, 0)

    def _delta_call_player_state(self, state):
        self._playbin.set_state(state)

    def _delta_call_register_player_object(self, object_):
        self._playbin_factory.register_listener(object_)

    def _play(self):
        self._playbin.set_state(Gst.State.NULL)
        self._playbin.set_property("uri", self._enquiry("delta > uri"))
        self._playbin.set_state(Gst.State.PLAYING)

    def play(self):
        if "_playbin" in dir(self):
            self._play()

    def stop(self):
        if "_playbin" in dir(self):
            self._playbin.set_state(Gst.State.NULL)

    def __init__(self, parent):
        self._parent = parent
        self._playbin_factory = DeltaPlaybin(self)
        DeltaUI(self)
