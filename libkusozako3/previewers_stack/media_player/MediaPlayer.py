
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .Player import DeltaPlayer


class DeltaMediaPlayer(DeltaEntity):

    def _delta_info_uri(self):
        return self._uri

    def set_data(self, file_info, gio_file, mime_type):
        self._uri = gio_file.get_uri()
        self._transmitter.transmit(("uri", self._uri))
        self._player.play()
        self._raise("delta > switch previewer to", "media-player")

    def stop(self):
        self._player.stop()

    def _delta_call_destroy(self):
        self._player.stop()

    def _delta_call_register_uri_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_file_has_video(self, has_video):
        self._transmitter.transmit(("has-video", has_video))

    def __init__(self, parent):
        self._parent = parent
        self._uri = None
        self._transmitter = FoxtrotTransmitter()
        self._player = DeltaPlayer(self)
