
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaVideoViewer(Gtk.DrawingArea, DeltaEntity):

    def _on_realize(self, drawing_area):
        gdk_window = drawing_area.get_window()
        self._raise("delta > drawing area realized", gdk_window.get_xid())

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True, vexpand=True)
        self.connect("realize", self._on_realize)
        self._raise("delta > add to stack named", (self, "video-viewer"))
