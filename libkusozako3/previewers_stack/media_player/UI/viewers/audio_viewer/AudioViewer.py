
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .cover_art.CoverArt import DeltaCoverArt
from .tags.Tags import DeltaTags


class DeltaAudioViewer(Gtk.Box, DeltaEntity):

    def _get_orientation(self):
        rectangle, _ = self.get_allocated_size()
        if rectangle.width >= rectangle.height:
            return Gtk.Orientation.HORIZONTAL
        return Gtk.Orientation.VERTICAL

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_check_resized(self):
        orientation = self._get_orientation()
        self.set_orientation(orientation)
        self.set_homogeneous(orientation == Gtk.Orientation.HORIZONTAL)
        self._tags.reset_orientation()

    def _delta_call_queue_draw(self):
        self._cover_art.queue_draw()

    def _delta_info_is_vertically_long(self):
        return self._get_orientation() == Gtk.Orientation.VERTICAL

    def _on_destroy(self, widget):
        self._raise("delta > destroy")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_spacing(Unit(4))
        self._cover_art = DeltaCoverArt(self)
        self._tags = DeltaTags(self)
        self.connect("destroy", self._on_destroy)
        self._raise("delta > add to stack named", (self, "audio-viewer"))
