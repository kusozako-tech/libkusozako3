
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from gi.repository import Gio
from . import Keys


class FoxtrotMetadata:

    def _has_key(self, mutagen_file, key):
        try:
            has_key = key in mutagen_file
            return has_key
        except ValueError:
            return False

    def _get_tag(self, mutagen_file, keys, default=_("Unknown")):
        for key in keys:
            if self._has_key(mutagen_file, key):
                return mutagen_file[key][0]
        return default

    def _get_mutagen_file(self, uri):
        gio_file = Gio.File.new_for_uri(uri)
        try:
            return mutagen.File(gio_file.get_path())
        except mutagen.mp3.HeaderNotFoundError:
            return None

    def get_from_uri(self, uri):
        mutagen_file = self._get_mutagen_file(uri)
        if mutagen_file is None:
            return _("Unknown"), _("Unknown"), _("Unknown")
        title = self._get_tag(mutagen_file, Keys.TITLE)
        artist = self._get_tag(mutagen_file, Keys.ARTIST)
        album = self._get_tag(mutagen_file, Keys.ALBUM)
        return title, artist, album
