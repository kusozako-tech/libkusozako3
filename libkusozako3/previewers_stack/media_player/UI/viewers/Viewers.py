
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .VideoViewer import DeltaVideoViewer
from .audio_viewer.AudioViewer import DeltaAudioViewer


class DeltaViewers(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def receive_transmission(self, user_data):
        type_, has_video = user_data
        if type_ != "has-video":
            return
        name = "video-viewer" if has_video else "audio-viewer"
        self.set_visible_child_name(name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaVideoViewer(self)
        DeltaAudioViewer(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register uri object", self)
