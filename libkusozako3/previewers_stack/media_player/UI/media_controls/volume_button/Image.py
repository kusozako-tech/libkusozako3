
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity

ICONS = {
    "audio-volume-high-symbolic": 0.5,
    "audio-volume-medium-symbolic": 0.25,
    "audio-volume-low-symbolic": 0
    }


class DeltaImage(Gtk.Image, DeltaEntity):

    def _get_icon_name(self, value):
        for icon_name, comparison in ICONS.items():
            if value > comparison:
                return icon_name
        return "audio-volume-muted-symbolic"

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "player" and key == "file_chooser_software_volume":
            self._raise("delta > volume changed", value)
            self.set_property("icon-name", self._get_icon_name(value))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(
            self,
            icon_name="audio-volume-high-symbolic",
            icon_size=Gtk.IconSize.SMALL_TOOLBAR
            )
        query = "player", "file_chooser_software_volume", 0.1
        volume = self._enquiry("delta > settings", query)
        self.set_property("icon-name", self._get_icon_name(volume))
        self._raise("delta > add to container", self)
        self._raise("delta > register settings object", self)
