
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .play_pause_button.PlayPauseButton import DeltaPlayPauseButton
from .seek_bar.SeekBar import DeltaSeekBar
from .volume_button.VolumeButton import DeltaVolumeButton


class DeltaMediaControls(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaPlayPauseButton(self)
        DeltaSeekBar(self)
        DeltaVolumeButton(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
