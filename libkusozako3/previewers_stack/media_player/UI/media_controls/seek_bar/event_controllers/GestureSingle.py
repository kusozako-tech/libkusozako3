
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaGestureSingle(Gtk.GestureSingle, DeltaEntity):

    def _on_begin(self, gesture, event_sequence):
        _, x, y = gesture.get_point(event_sequence)
        size = self._enquiry("delta > allocated size")
        self._raise("delta > seek", x/size.width)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        Gtk.GestureSingle.__init__(self, widget=drawing_area)
        self.connect("begin", self._on_begin)
