
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Color import DeltaColor
from .Timings import DeltaTimings
from .PaintProgress import DeltaPaintProgress
from .PaintMarkup import DeltaPaintMarkup
from .PaintMotion import DeltaPaintMotion


class DeltaPainters(DeltaEntity):

    def _delta_info_rgba(self):
        return self._color.rgba

    def _delta_info_timing(self):
        return self._timings.timing

    def paint(self, cairo_context):
        self._progress.paint(cairo_context)
        self._motion.paint(cairo_context)
        self._markup.paint(cairo_context)

    def __init__(self, parent):
        self._parent = parent
        self._color = DeltaColor(self)
        self._timings = DeltaTimings(self)
        self._progress = DeltaPaintProgress(self)
        self._markup = DeltaPaintMarkup(self)
        self._motion = DeltaPaintMotion(self)
