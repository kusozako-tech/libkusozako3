
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaAudioVolume(DeltaEntity):

    @classmethod
    def new_for_playbin(cls, parent, playbin):
        audio_volume = cls(parent)
        audio_volume.set_playbin(playbin)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "player" and key == "file_chooser_software_volume":
            self._playbin.set_property("volume", value)

    def set_playbin(self, playbin):
        self._playbin = playbin
        self._raise("delta > register settings object", self)
        query = "player", "software_volume", 0.1
        volume = self._enquiry("delta > settings", query)
        self._playbin.set_property("volume", volume)

    def __init__(self, parent):
        self._parent = parent
