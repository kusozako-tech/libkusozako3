
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity


class DeltaGifAnimation(DeltaEntity):

    def _timeout(self, iter_, path):
        if iter_.advance() and path == self._path:
            self._refresh_pixbuf(iter_, path)
        return GLib.SOURCE_REMOVE

    def _refresh_pixbuf(self, iter_, path):
        pixbuf = iter_.get_pixbuf()
        delay = iter_.get_delay_time()
        self._raise("delta > refresh pixbuf", pixbuf.copy())
        GLib.timeout_add(delay, self._timeout, iter_, path)

    def _start_animation(self, path):
        animation = GdkPixbuf.PixbufAnimation.new_from_file(path)
        iter_ = animation.get_iter()
        self._refresh_pixbuf(iter_, path)

    def try_start_animation(self, mime_type, fullpath):
        self._path = fullpath
        if mime_type == "image/gif":
            self._start_animation(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._path = None
