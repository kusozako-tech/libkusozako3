
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from PIL import Image
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from .GifAnimation import DeltaGifAnimation


class DeltaModel(DeltaEntity):

    def _load_webp_image(self, fullpath):
        webp_image = Image.open(fullpath)
        width, height = webp_image.size
        glib_bytes = GLib.Bytes.new(webp_image.tobytes())
        return GdkPixbuf.Pixbuf.new_from_bytes(
            glib_bytes,                 # image data in 8-bit per sample
            GdkPixbuf.Colorspace.RGB,   # color space.
            False,                      # has alpha
            8,                          # bits per sample
            width,                      # width in pixel
            height,                     # height in pixel
            width*3                     # distance in bytes between row
            )

    def set_data(self, gio_file, mime_type):
        fullpath = gio_file.get_path()
        self._gif_animation.try_start_animation(mime_type, fullpath)
        if mime_type == "image/webp":
            pixbuf = self._load_webp_image(fullpath)
            self._raise("delta > refresh pixbuf", pixbuf.copy())
        elif mime_type not in ("image/gif", "image/webp"):
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(fullpath)
            self._raise("delta > refresh pixbuf", pixbuf.copy())

    def __init__(self, parent):
        self._parent = parent
        self._gif_animation = DeltaGifAnimation(self)
