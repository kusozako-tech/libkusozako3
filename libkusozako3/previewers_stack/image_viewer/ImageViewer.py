
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Model import DeltaModel
from .DrawingArea import DeltaDrawingArea
from .Label import DeltaLabel

UNIQUE_NAME = "image-viewer"


class DeltaImageViewer(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_pixbuf(self):
        return self._pixbuf

    def _delta_call_refresh_pixbuf(self, pixbuf):
        self._pixbuf = pixbuf
        self._drawing_area.queue_draw()
        self._label.set_pixbuf_data(pixbuf)

    def set_data(self, file_info, gio_file, mime_type):
        self._model.set_data(gio_file, mime_type)
        self._raise("delta > switch previewer to", UNIQUE_NAME)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._pixbuf = None
        self._model = DeltaModel(self)
        self._drawing_area = DeltaDrawingArea(self)
        self._label = DeltaLabel(self)
        self._raise("delta > add preview", (self, UNIQUE_NAME))
