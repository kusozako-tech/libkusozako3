
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from .Document import DeltaDocument
from .IconView import DeltaIconView


class DeltaPdfViewer(Gtk.ListStore, DeltaEntity):

    def _delta_call_append_row_data(self, row_data):
        self.append(row_data)

    def _delta_info_allocated_width(self):
        return self._icon_view.get_allocated_width()

    def _delta_info_model(self):
        return self

    def set_data(self, file_info, gio_file, mime_type):
        self.clear()
        self._document.load(gio_file)
        self._raise("delta > switch previewer to", "pdf-viewer")

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, int, GdkPixbuf.Pixbuf)
        self._document = DeltaDocument(self)
        self._icon_view = DeltaIconView(self)
