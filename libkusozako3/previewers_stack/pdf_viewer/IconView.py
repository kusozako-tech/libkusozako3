
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.IconView.__init__(self, model=self._enquiry("delta > model"))
        self.set_pixbuf_column(1)
        scrolled_window.add(self)
        self._raise("delta > add preview", (scrolled_window, "pdf-viewer"))
