
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from libkusozako3.Entity import DeltaEntity


class DeltaWebView(WebKit2.WebView, DeltaEntity):

    def set_data(self, file_info, gio_file, mime_type):
        uri = gio_file.get_uri()
        self.load_uri(uri)
        self._raise("delta > switch previewer to", "web-view")

    def __init__(self, parent):
        self._parent = parent
        WebKit2.WebView.__init__(self)
        self._raise("delta > add preview", (self, "web-view"))
