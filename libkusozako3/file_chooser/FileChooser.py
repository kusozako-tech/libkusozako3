
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .window.Window import DeltaWindow
from .selection.Selection import DeltaSelection


class DeltaFileChooser(DeltaEntity):

    @classmethod
    def run_for_model(cls, parent, model):
        chooser = cls(parent)
        return chooser.set_model(model)

    def _delta_info_model(self, key):
        return self._model[key] if key in self._model else None

    def _delta_call_register_file_manager_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_file_manager_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def set_model(self, model):
        self._model = model
        self._transmitter = FoxtrotTransmitter()
        self._selection = DeltaSelection(self)
        DeltaWindow(self)
        return self._selection.get_final_selection()

    def __init__(self, parent):
        self._parent = parent
