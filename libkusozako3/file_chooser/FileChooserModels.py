
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


SELECT_FILE = {
    "type": "select-file",
    "id": None,
    "title": _("Select File"),
    "directory": None,                          # should be home directory
    "filters": {_("text files"): "text/*"},     # do not use this
    "use-magic": False,
    "allow-read-only": False
    }

SELECT_FILES = {
    "type": "select-files",
    "id": None,
    "title": _("Select Files"),
    "directory": None,                          # should be home directory
    "filters": {_("text files"): "text/*"},     # do not use this
    "use-magic": False,
    "allow-read-only": False
    }

SELECT_DIRECTORY = {
    "type": "select-directory",
    "id": None,
    "title": _("Select Directory"),
    "directory": None,                          # should be home directory
    "read-write-only": True
    }

SAVE_FILE = {
    "type": "save-file",
    "id": None,
    "title": _("Save File"),
    "file-name": _("untitiled.txt"),
    "directory": None,                              # should be home directory
    "filters": {_("text files"): "text/plain"},     # do not use this
    "use-magic": True
    }
