
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .header_bar.HeaderBar import DeltaHeaderBar
from .paned.Paned import DeltaPaned


class EchoContentArea:

    def __init__(self, parent):
        DeltaHeaderBar(parent)
        DeltaPaned(parent)
