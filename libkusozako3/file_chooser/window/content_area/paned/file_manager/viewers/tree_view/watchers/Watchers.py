
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .RowActivation import DeltaRowActivation
from .Selection import DeltaSelection
from .QueryTooltip import DeltaQueryTooltip


class EchoWatchers:

    def __init__(self, parent):
        DeltaRowActivation(parent)
        DeltaSelection(parent)
        DeltaQueryTooltip(parent)
