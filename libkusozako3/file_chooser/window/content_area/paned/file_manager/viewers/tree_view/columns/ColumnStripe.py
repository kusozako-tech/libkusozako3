
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity


class DeltaColumnStripe(DeltaEntity):

    def _get_rgba(self, query):
        gdk_rgba = Gdk.RGBA()
        settings = self._enquiry("delta > settings", ("css", query, None))
        gdk_rgba.parse(settings)
        return gdk_rgba.copy()

    def set_background(self, renderer, is_even):
        if is_even:
            renderer.set_property("cell-background-rgba", self._primary)
        else:
            renderer.set_property("cell-background-rgba", self._secondary)

    def set_foreground(self, renderer, is_even):
        if is_even:
            renderer.set_property("foreground-rgba", self._primary_fg)
        else:
            renderer.set_property("foreground-rgba", self._secondary_fg)

    def _reset_colors(self):
        self._primary = self._get_rgba("primary_surface_bg_color")
        self._secondary = self._get_rgba("secondary_surface_bg_color")
        self._primary_fg = self._get_rgba("primary_surface_fg_color")
        self._secondary_fg = self._get_rgba("secondary_surface_fg_color")

    def __init__(self, parent):
        self._parent = parent
        self._reset_colors()
