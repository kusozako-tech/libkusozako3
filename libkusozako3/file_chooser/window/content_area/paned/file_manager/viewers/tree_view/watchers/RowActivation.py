
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals


class DeltaRowActivation(DeltaEntity):

    def _on_row_activated(self, tree_view, tree_path, column):
        model = tree_view.get_model()
        file_info = model[tree_path][FileManagerColumns.GIO_FILE_INFO]
        if file_info.get_content_type() == "inode/directory":
            directory = model[tree_path][FileManagerColumns.FULLPATH]
            param = FileManagerSignals.MOVE_DIRECTORY, directory
            self._raise("delta > file manager signal", param)
        else:
            print("activated ?", model[tree_path][FileManagerColumns.FULLPATH])

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("row-activated", self._on_row_activated)
