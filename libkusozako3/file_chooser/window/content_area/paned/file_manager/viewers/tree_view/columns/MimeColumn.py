
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.file_chooser import FileManagerColumns
from .ColumnSorter import DeltaColumnSorter
from .ColumnStripe import DeltaColumnStripe


class DeltaMimeColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, iter_, column_stripe):
        tree_path = model.get_path(iter_)
        is_even = (tree_path[0] % 2 == 0)
        column_stripe.set_background(renderer, is_even)
        column_stripe.set_foreground(renderer, is_even)

    def _delta_info_tree_view_column(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(xpad=Unit(1), xalign=1)
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            title=_("Content Type"),
            text=FileManagerColumns.CONTENT_TYPE
            )
        self.set_expand(False)
        self.set_sort_column_id(FileManagerColumns.CONTENT_TYPE)
        DeltaColumnSorter(self)
        column_stripe = DeltaColumnStripe(self)
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func, column_stripe)
