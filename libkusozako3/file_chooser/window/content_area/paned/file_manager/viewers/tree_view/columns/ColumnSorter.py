
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaColumnSorter(DeltaEntity):

    def _on_clicked(self, column):
        self._order += 1
        order = self._order % 2
        signal_param = column.get_sort_column_id(), order
        param = FileManagerSignals.MODEL_CHANGE_SORT_COLUMN, signal_param
        self._raise("delta > file manager signal", param)

    def __init__(self, parent):
        self._parent = parent
        self._order = 0
        tree_view_column = self._enquiry("delta > tree view column")
        tree_view_column.set_clickable(True)
        tree_view_column.connect("clicked", self._on_clicked)
