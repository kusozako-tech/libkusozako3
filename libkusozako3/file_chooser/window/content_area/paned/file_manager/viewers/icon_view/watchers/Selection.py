
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaSelection(DeltaEntity):

    def _reset_selection(self, icon_view):
        tree_paths = icon_view.get_selected_items()
        if tree_paths:
            model = icon_view.get_model()
            tree_row = model[tree_paths[0]]
            self._raise("delta > file selected", tree_row)

    def _on_selection_changed(self, icon_view):
        if self._model_loaded:
            self._reset_selection(icon_view)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.PARSER_PARSING_STARTED:
            self._model_loaded = False
        if signal == FileManagerSignals.PARSER_PARSING_FINISHED:
            self._model_loaded = True

    def __init__(self, parent):
        self._parent = parent
        self._model_loaded = False
        tree_view = self._enquiry("delta > icon view")
        tree_view.connect("selection-changed", self._on_selection_changed)
        self._raise("delta > register file manager object", self)
