
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .VAdjustment import DeltaVAdjustment
from .QueryTooltip import DeltaQueryTooltip
from .ItemActivated import DeltaItemActivated
from .Selection import DeltaSelection
from .Mapped import DeltaMapped


class EchoWatchers:

    def __init__(self, parent):
        DeltaVAdjustment(parent)
        DeltaQueryTooltip(parent)
        DeltaItemActivated(parent)
        DeltaSelection(parent)
        DeltaMapped(parent)
