
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals


class DeltaItemActivated(DeltaEntity):

    def _on_item_activated(self, icon_view, tree_path):
        model = icon_view.get_model()
        tree_row = model[tree_path]
        if "inode/directory" == tree_row[FileManagerColumns.MIME]:
            directory = tree_row[FileManagerColumns.FULLPATH]
            param = FileManagerSignals.MOVE_DIRECTORY, directory
            self._raise("delta > file manager signal", param)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect_after("item-activated", self._on_item_activated)
