
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals

DELAY = 50


class DeltaVAdjustment(DeltaEntity):

    def _timeout(self, id_, start_path, end_path):
        if self._id != id_:
            return GLib.SOURCE_REMOVE
        directory = self._enquiry("delta > current directory")
        signal_param = start_path, end_path, directory
        param = FileManagerSignals.MODEL_REQUEST_THUMBNAIL_TILE, signal_param
        self._raise("delta > file manager signal", param)
        return GLib.SOURCE_REMOVE

    def _on_changed(self, v_adjustment, icon_view):
        self._id += 1
        visible_range = icon_view.get_visible_range()
        if visible_range is None:
            return
        start_path, end_path = visible_range
        id_ = self._id
        GLib.timeout_add(DELAY, self._timeout, id_, start_path, end_path)

    def __init__(self, parent):
        self._parent = parent
        self._id = 0
        icon_view = self._enquiry("delta > icon view")
        v_adjustment = icon_view.get_vadjustment()
        v_adjustment.connect("value-changed", self._on_changed, icon_view)
        v_adjustment.connect("changed", self._on_changed, icon_view)
