
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaParserParsingFinished(AlfaAction):

    SIGNAL = FileManagerSignals.PARSER_PARSING_FINISHED

    def _action(self, directory):
        icon_view = self._enquiry("delta > icon view")
        visible_range = icon_view.get_visible_range()
        if visible_range is None:
            return
        start_path, end_path = visible_range
        signal_param = start_path, end_path, directory
        param = FileManagerSignals.MODEL_REQUEST_THUMBNAIL_TILE, signal_param
        self._raise("delta > file manager signal", param)
