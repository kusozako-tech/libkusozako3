
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .tree_view.TreeView import DeltaTreeView
from .icon_view.IconView import DeltaIconView


class DeltaViewers(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.VIEWERS_SWITCH_STACK_TO:
            self.set_visible_child_name(param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT
            )
        DeltaIconView(self)
        DeltaTreeView(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
