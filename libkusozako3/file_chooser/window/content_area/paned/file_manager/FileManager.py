
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileChooserTypes
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals
from .filter_model.FilterModel import DeltaFilterModel
from .filename_bar.FileNameBar import DeltaFileNameBar
from .navigation_bar.NavigationBar import DeltaNavigationBar
from .viewers.Viewers import DeltaViewers


class DeltaFileManager(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_file_selected(self, tree_row):
        type_ = self._enquiry("delta > model", "type")
        if type_ == FileChooserTypes.SAVE_FILE:
            self._filename_bar.set_text(tree_row[FileManagerColumns.NAME])
        else:
            param = FileManagerSignals.NEW_SELECTION, tree_row
            self._raise("delta > file manager signal", param)

    def _delta_info_directory_model(self):
        return self._filter_model.model

    def __init__(self, parent):
        self._parent = parent
        self._filter_model = DeltaFilterModel(self)
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._filename_bar = DeltaFileNameBar(self)
        DeltaNavigationBar(self)
        DeltaViewers(self)
        self._raise("delta > add to container", self)
