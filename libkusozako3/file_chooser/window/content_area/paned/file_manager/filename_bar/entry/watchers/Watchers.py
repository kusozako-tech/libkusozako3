
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Activate import DeltaActivate
from .Changed import DeltaChanged


class EchoWatchers:

    def __init__(self, parent):
        DeltaActivate(parent)
        DeltaChanged(parent)
