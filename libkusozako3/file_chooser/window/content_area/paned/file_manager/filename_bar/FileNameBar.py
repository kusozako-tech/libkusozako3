
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Label import DeltaLabel
from .entry.Entry import DeltaEntry


class DeltaFileNameBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def set_text(self, text):
        self._entry.set_text(text)

    def __init__(self, parent):
        self._parent = parent
        revealer = Gtk.Revealer()
        revealed = (self._enquiry("delta > model", "type") == "save-file")
        revealer.set_reveal_child(revealed)
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaLabel(self)
        self._entry = DeltaEntry(self)
        self.set_margin_bottom(Unit(2))
        revealer.add(self)
        self._raise("delta > add to container", revealer)
        self._raise("delta > css", (self, "primary-surface-color-class"))
