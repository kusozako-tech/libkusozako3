
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from libkusozako3.file_chooser import FileViewerType


class DeltaSwitchToIconViewButton(Gtk.Button, DeltaEntity):

    VIEWER_TYPE = FileViewerType.ICON_VIEW

    def _on_clicked(self, button):
        param = FileManagerSignals.VIEWERS_SWITCH_STACK_TO, self.VIEWER_TYPE
        self._raise("delta > file manager signal", param)

    def receive_transmission(self, user_data):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        image = Gtk.Image.new_from_icon_name(
            "view-grid-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        self.set_image(image)
        self.props.tooltip_text = _("IconView")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
