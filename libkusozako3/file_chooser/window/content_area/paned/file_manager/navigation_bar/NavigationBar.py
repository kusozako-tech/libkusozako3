
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .HomeButton import DeltaHomeButton
from .GoUpButton import DeltaGoUpButton
from .history_button.HistoryButton import DeltaHistoryButton
from .address_entry.AddressEntry import DeltaAddressEntry
from .ShowHiddenButton import DeltaShowHiddenButton
from .SwitchToIconViewButton import DeltaSwitchToIconViewButton
from .SwitchToTreeViewButton import DeltaSwitchToTreeViewButton
from .make_directory_button.MakeDirectoryButton import DeltaMakeDirectoryButton


class DeltaNavigationBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_margin_bottom(Unit(2))
        DeltaHomeButton(self)
        DeltaGoUpButton(self)
        DeltaHistoryButton(self)
        DeltaAddressEntry(self)
        DeltaMakeDirectoryButton(self)
        DeltaShowHiddenButton(self)
        DeltaSwitchToIconViewButton(self)
        DeltaSwitchToTreeViewButton(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
