
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .Directory import DeltaDirectory


class DeltaAddressEntry(Gtk.Entry, DeltaEntity):

    def _on_changed(self, entry):
        path = entry.get_text()
        directory, filter_ = self._directory.parse(path)
        if directory is not None:
            param = FileManagerSignals.MOVE_DIRECTORY, directory
            self._raise("delta > file manager signal", param)
        else:
            param = FileManagerSignals.FILTER_CHANGE_KEYWORD, filter_
            self._raise("delta > file manager signal", param)

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        self.set_text(directory)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self, hexpand=True)
        self._directory = DeltaDirectory(self)
        directory = self._enquiry("delta > current directory")
        self.set_text(directory)
        self.connect("changed", self._on_changed)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
