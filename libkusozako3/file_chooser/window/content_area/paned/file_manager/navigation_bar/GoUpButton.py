
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaGoUpButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        directory = self._enquiry("delta > current directory")
        parent_directory = GLib.path_get_dirname(directory)
        param = FileManagerSignals.MOVE_DIRECTORY, parent_directory
        self._raise("delta > file manager signal", param)

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        self.set_sensitive(directory != "/")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        image = Gtk.Image.new_from_icon_name(
            "go-up-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        self.set_image(image)
        self.props.tooltip_text = _("Parent Directory")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
