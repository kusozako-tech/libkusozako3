
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .ShowHiddenFilter import DeltaShowHiddenFilter
from .MimeFilter import DeltaMimeFilter
from .KeywordFilter import DeltaKeywordFilter


class DeltaFilters(DeltaEntity):

    @classmethod
    def new_for_filter_model(cls, parent, filter_model):
        filters = cls(parent)
        filters.initialize(filter_model)
        return filters

    def _visible_func(self, model, tree_iter, user_data=None):
        tree_row = model[tree_iter]
        if not self._show_hidden_filter.matches(tree_row):
            return False
        if not self._mime_filter.matches(tree_row):
            return False
        return self._keyword_filter.matches(tree_row)

    def set_keyword(self, keyword):
        self._keyword_filter.set_keyword(keyword)

    def initialize(self, filter_model):
        filter_model.set_visible_func(self._visible_func)

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden_filter = DeltaShowHiddenFilter(self)
        self._keyword_filter = DeltaKeywordFilter(self)
        self._mime_filter = DeltaMimeFilter(self)
