
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
# from libkusozako3.file_chooser import FileManagerSignals


class AlfaAction(DeltaEntity):

    SIGNAL = "define acceptable signal here."

    def _on_initialize(self):
        # optional
        pass

    def _action(self, param):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise("delta > register file manager object", self)
