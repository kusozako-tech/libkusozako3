
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .request_thumbnail.RequestThumbnail import DeltaRequestThumbnail


class EchoActions:

    def __init__(self, parent):
        DeltaRequestThumbnail(parent)
