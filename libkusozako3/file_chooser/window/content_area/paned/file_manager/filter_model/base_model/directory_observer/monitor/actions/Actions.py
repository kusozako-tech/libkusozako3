
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ParserParsingFinished import DeltaParserParsingFinished
from .FinalizeDialog import DeltaFinalizeDialog


class EchoActions:

    def __init__(self, parent):
        DeltaFinalizeDialog(parent)
        DeltaParserParsingFinished(parent)
