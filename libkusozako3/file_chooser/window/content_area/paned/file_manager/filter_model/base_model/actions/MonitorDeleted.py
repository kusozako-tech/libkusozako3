
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser import FileManagerSignals
from libkusozako3.file_chooser import FileManagerColumns
from .Action import AlfaAction


class DeltaMonitorDeleted(AlfaAction):

    SIGNAL = FileManagerSignals.MONITOR_DELETED

    def _get_tree_row_for_gio_file(self, gio_file, directory):
        if directory != self._enquiry("delta > current directory"):
            return None
        query = FileManagerColumns.FULLPATH, gio_file.get_path()
        return self._enquiry("delta > tree row", query)

    def _action(self, user_data):
        _, gio_file, _, _, directory = user_data
        response = self._get_tree_row_for_gio_file(gio_file, directory)
        if response is None:
            return
        model, tree_row = response
        model.remove(tree_row.iter)
