
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.mime_type import MimeType
from libkusozako3.file_chooser import FileManagerSignals
from libkusozako3.file_chooser import ThumbnailStatus
from libkusozako3.tile.IconTile import DeltaIconTile
from .Action import AlfaAction


class DeltaParserFileFound(AlfaAction):

    SIGNAL = FileManagerSignals.PARSER_FILE_FOUND

    def _get_name_for_sort(self, file_info):
        file_name = file_info.get_name()
        return GLib.utf8_collate_key_for_filename(file_name, -1)

    def _get_readable_time(self, file_info):
        date_time = file_info.get_modification_date_time()
        date_time_local = date_time.to_local()
        last_modified = date_time_local.format("%F")
        now = GLib.DateTime.new_now_local()
        if last_modified != now.format("%F"):
            return date_time_local.to_unix(), last_modified
        return date_time_local.to_unix(), date_time_local.format("%T")

    def _get_row_data(self, user_data):
        directory, gio_file_info, gio_file = user_data
        time_for_sort, readable_time = self._get_readable_time(gio_file_info)
        tile = self._icon_tile.build_for_file_info(gio_file_info)
        return (
            directory,
            gio_file_info,
            gio_file,
            gio_file.get_path(),                        # fullpath
            ThumbnailStatus.UNLOADED,                   # thumbnail status
            None,                                       # pixbuf
            tile,                                       # pixbuf
            tile,                                       # pixbuf
            gio_file_info.get_name(),                   # name
            self._get_name_for_sort(gio_file_info),     # name for sort
            gio_file_info.get_content_type(),           # content type
            MimeType.from_file_info(gio_file, gio_file_info),    # mime type
            readable_time,                          # last modified
            time_for_sort                           # last modified for sort
            )

    def _on_initialize(self):
        self._icon_tile = DeltaIconTile(self)

    def _action(self, param):
        directory, _, _ = param
        if directory != self._enquiry("delta > current directory"):
            return
        tree_row_data = self._get_row_data(param)
        self._raise("delta > append tree row data", tree_row_data)
