
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository.Gio import FileMonitorEvent as EVENT
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .actions.Actions import EchoActions

SIGNALS = {
    EVENT.CHANGED: FileManagerSignals.MONITOR_CHANGED,
    EVENT.CHANGES_DONE_HINT: FileManagerSignals.MONITOR_CHANGES_DONE,
    EVENT.DELETED: FileManagerSignals.MONITOR_DELETED,
    EVENT.CREATED: FileManagerSignals.MONITOR_CREATED,
    EVENT.ATTRIBUTE_CHANGED: FileManagerSignals.MONITOR_ATTRIBUTE_CHANGED,
    }


class DeltaMonitor(DeltaEntity):

    def _on_changed(self, *args):
        monitor, alfa, bravo, event_type, directory = args
        signal = SIGNALS.get(event_type, None)
        if signal is None or alfa.get_path() == directory:
            return
        self._raise("delta > file manager signal", (signal, args))

    def _delta_call_cancel_monitor(self):
        if self._monitor is not None:
            self._monitor.cancel()

    def _delta_call_restart(self, gio_file):
        self._monitor = gio_file.monitor_directory(Gio.FileMonitorFlags.NONE)
        directory = gio_file.get_path()
        self._monitor.connect("changed", self._on_changed, directory)

    def __init__(self, parent):
        self._parent = parent
        self._monitor = None
        EchoActions(self)
