
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaFileEnumeratorBuilder(DeltaEntity):

    def _on_enumerate_children_finished(self, gio_file, task, directory):
        self._raise("delta > parsing started", directory)
        param = FileManagerSignals.PARSER_PARSING_STARTED, directory
        self._raise("delta > file manager signal", param)
        file_enumerator = gio_file.enumerate_children_finish(task)
        user_data = file_enumerator, directory
        self._raise("delta > file enumerator built", user_data)

    def _set_directory(self, directory):
        gio_file = Gio.File.new_for_path(directory)
        gio_file.enumerate_children_async(
            "*",
            Gio.FileQueryInfoFlags.NONE,
            GLib.PRIORITY_DEFAULT_IDLE,
            None,
            self._on_enumerate_children_finished,
            directory
            )

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        if not GLib.file_test(directory, GLib.FileTest.EXISTS):
            self._raise("delta > dirctory not found", directory)
            return
        self._set_directory(directory)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file manager object", self)
