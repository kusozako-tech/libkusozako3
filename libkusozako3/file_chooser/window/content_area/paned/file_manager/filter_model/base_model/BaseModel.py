
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from .actions.Actions import EchoActions
from .directory_observer.DirectoryObserver import EchoDirectoryObserver
from .sort.Sort import DeltaSort

SORT_COLUMN_ID = FileManagerColumns.NAME_FOR_SORT


class DeltaBaseModel(Gtk.ListStore, DeltaEntity):

    def _delta_info_tree_row(self, user_data):
        column, comparison = user_data
        for tree_row in self:
            if tree_row[column] == comparison:
                return self, tree_row
        return None

    def _delta_call_parsing_started(self, directory):
        self.clear()

    def _delta_call_append_tree_row_data(self, tree_row_data):
        self.append(tree_row_data)

    def _delta_info_base_model(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *FileManagerColumns.TYPES)
        self.set_sort_column_id(SORT_COLUMN_ID, Gtk.SortType.ASCENDING)
        DeltaSort(self)
        EchoActions(self)
        EchoDirectoryObserver(self)
