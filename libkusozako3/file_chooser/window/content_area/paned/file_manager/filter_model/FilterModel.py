
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .actions.Actions import EchoActions
from .filters.Filters import DeltaFilters


class DeltaFilterModel(DeltaEntity):

    def _delta_call_request_refilter(self):
        self._filter_model.refilter()

    def __init__(self, parent):
        self._parent = parent
        base_model = DeltaBaseModel(self)
        self._filter_model = base_model.filter_new()
        self._filters = DeltaFilters.new_for_filter_model(
            self,
            self._filter_model
            )
        EchoActions(self)

    @property
    def model(self):
        return self._filter_model
