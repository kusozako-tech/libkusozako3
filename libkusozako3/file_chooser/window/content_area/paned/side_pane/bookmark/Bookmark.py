
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .model.Model import DeltaModel
from .tree_view.TreeView import DeltaTreeView


class DeltaBookmark(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel()
        DeltaTreeView(self)
