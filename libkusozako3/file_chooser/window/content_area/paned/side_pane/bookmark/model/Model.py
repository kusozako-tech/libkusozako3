
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import BookmarkColumns
from .XdgUserDirsParser import DeltaXdgUserDirsParser
from .Gtk3BookmarkParser import DeltaGtk3BookmarkParser


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _is_duplicated(self, row_data):
        fullpath = row_data[BookmarkColumns.FULLPATH]
        for existed_row_data in self:
            if existed_row_data[BookmarkColumns.FULLPATH] == fullpath:
                return row_data[BookmarkColumns.TYPE] == "gtk3-bookmark"
        return False

    def _delta_call_append(self, row_data):
        if not self._is_duplicated(row_data):
            self.append(row_data)

    def __init__(self):
        # terminal entity
        self._parent = None
        Gtk.ListStore.__init__(self, *BookmarkColumns.TYPES)
        DeltaXdgUserDirsParser(self)
        DeltaGtk3BookmarkParser(self)
