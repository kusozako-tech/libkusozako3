
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import HomeDirectory

ICON_NAMES = {
    0: "user-desktop-symbolic",
    1: "folder-documents-symbolic",
    2: "folder-download-symbolic",
    3: "folder-music-symbolic",
    4: "folder-pictures-symbolic",
    5: "folder-publicshare-symbolic",
    6: "folder-templates-symbolic",
    7: "folder-videos-symbolic",
}


class DeltaXdgUserDirsParser(DeltaEntity):

    def _parse(self):
        for index, icon_name in ICON_NAMES.items():
            fullpath = GLib.get_user_special_dir(index)
            row_data = (
                icon_name,
                GLib.filename_display_basename(fullpath),
                fullpath,
                HomeDirectory.shorten(fullpath),
                "xdg-user-dirs"
                )
            self._raise("delta > append", row_data)

    def __init__(self, parent):
        self._parent = parent
        self._parse()
