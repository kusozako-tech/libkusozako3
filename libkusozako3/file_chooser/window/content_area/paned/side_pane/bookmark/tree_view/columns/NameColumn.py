
from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.file_chooser import BookmarkColumns


class DeltaNameColumn(Gtk.TreeViewColumn, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(
            xpad=Unit(1),
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            xalign=0,
            )
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            text=BookmarkColumns.BASENAME
            )
        self.set_expand(True)
        self._raise("delta > insert column", self)
