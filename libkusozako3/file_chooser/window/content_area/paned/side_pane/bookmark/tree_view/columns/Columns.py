
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .IconColumn import DeltaIconColumn
from .NameColumn import DeltaNameColumn


class EchoColumns:

    def __init__(self, parent):
        DeltaIconColumn(parent)
        DeltaNameColumn(parent)
