
from gi.repository import Gtk
from libkusozako3.Ux import Unit
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import BookmarkColumns


class DeltaIconColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, tree_iter, user_data):
        renderer.set_property("height", Unit(5))
        name = model[tree_iter][BookmarkColumns.ICON_NAME]
        renderer.set_property("icon-name", name)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(self, cell_renderer=renderer)
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func)
