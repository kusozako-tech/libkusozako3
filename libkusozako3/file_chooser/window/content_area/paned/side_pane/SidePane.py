
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .bookmark.Bookmark import DeltaBookmark
from .previewers.Previewers import DeltaPreviewers


class DeltaSidePane(Gtk.Stack, DeltaEntity):

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == FileManagerSignals.MOVE_DIRECTORY:
            self.set_visible_child_name("bookmark")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.OVER_RIGHT_LEFT,
            hexpand=True
            )
        DeltaBookmark(self)
        DeltaPreviewers(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
