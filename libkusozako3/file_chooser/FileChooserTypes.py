
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


SELECT_FILE = "select-file"
SELECT_FILES = "select-files"
SELECT_DIRECTORY = "select-directory"
SAVE_FILE = "save-file"
