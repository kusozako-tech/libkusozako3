
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

ICON_NAME = 0
BASENAME = 1
FULLPATH = 2
TOOLTIP = 3
TYPE = 4

TYPES = (
    str,
    str,
    str,
    str,
    str
    )
