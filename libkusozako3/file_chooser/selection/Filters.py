
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns


class DeltaFilters(DeltaEntity):

    def matches(self, tree_iter):
        mime = tree_iter[FileManagerColumns.MIME]
        for filter_ in self._filters:
            if GLib.regex_match_simple(filter_, mime, 0, 0):
                return True
        return False

    def _convert_to_list(self, filters):
        if type(filters) is str:
            return [filters]
        if type(filters) is dict:
            return tuple(filters.values())
        return filters

    def _get_filters_from_model(self):
        filters = self._enquiry("delta > model", "filters")
        if filters is None:
            filters = ["*"]
        if self._enquiry("delta > model", "type") == "select-directory":
            filters = ["inode/directory"]
        return self._convert_to_list(filters)

    def __init__(self, parent):
        self._parent = parent
        self._filters = self._get_filters_from_model()
