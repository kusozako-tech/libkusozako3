
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .actions.Actions import EchoActions


class DeltaPaths(DeltaEntity):

    def _delta_call_replace_selection(self, paths):
        self._paths = paths
        param = FileManagerSignals.SELECTION_CHANGED, paths
        self._raise("delta > file manager signal", param)

    def _delta_call_clear_selection(self):
        self._paths.clear()
        param = FileManagerSignals.SELECTION_CHANGED, None
        self._raise("delta > file manager signal", param)

    def get_paths(self):
        if self._paths == []:
            return None
        if self._enquiry("delta > model", "type") != "select files":
            return self._paths[0]
        return self._paths

    def _delta_info_paths(self):
        return self._paths

    def __init__(self, parent):
        self._parent = parent
        self._paths = []
        EchoActions(self)
