
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals
from .Filters import DeltaFilters


class DeltaNewSelection(AlfaAction):

    SIGNAL = FileManagerSignals.NEW_SELECTION

    def _action(self, tree_row):
        pass

    def _on_initialize(self):
        self._filters = DeltaFilters(self)
