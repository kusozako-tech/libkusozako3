
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaClearSelection(AlfaAction):

    SIGNAL = FileManagerSignals.CLEAR_SELECTION

    def _action(self, param=None):
        self._raise("delta > clear selection")
