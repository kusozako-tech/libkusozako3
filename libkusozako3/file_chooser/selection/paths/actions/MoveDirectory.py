
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaMoveDirectory(AlfaAction):

    SIGNAL = FileManagerSignals.MOVE_DIRECTORY

    def _action(self, directory=None):
        if self._enquiry("delta > model", "type") == "select-directory":
            self._raise("delta > replace selection", [directory])
        else:
            self._raise("delta > clear selection")
