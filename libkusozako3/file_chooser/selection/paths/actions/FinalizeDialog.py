
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType
from libkusozako3.file_chooser import FileManagerSignals


class DeltaFinalizeDialog(DeltaEntity):

    def _save(self, id_, paths):
        path = paths[0]
        gio_file = Gio.File.new_for_path(path)
        if MimeType.from_gio_file(gio_file) == "inode/directory":
            directory = path
        else:
            parent = gio_file.get_parent()
            directory = parent.get_path()
        user_data = "file_chooser.{}".format(id_), "last-directory", directory
        self._raise("delta > settings", user_data)

    def save(self):
        id_ = self._enquiry("delta > model", "id")
        paths = self._enquiry("delta > paths")
        if not paths:
            return
        self._save(id_, paths)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != FileManagerSignals.FINALIZE_DIALOG:
            return
        self.save()

    def __init__(self, parent):
        self._parent = parent
        id_ = self._enquiry("delta > model", "id")
        if id_ is not None:
            self._raise("delta > register file manager object", self)
