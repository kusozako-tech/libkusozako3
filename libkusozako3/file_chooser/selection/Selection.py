
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .paths.Paths import DeltaPaths


class DeltaSelection(DeltaEntity):

    def get_final_selection(self):
        param = FileManagerSignals.FINALIZE_DIALOG, None
        self._raise("delta > file manager signal", param)
        return self._paths.get_paths()

    def __init__(self, parent):
        self._parent = parent
        self._paths = DeltaPaths(self)
