
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MOVE_DIRECTORY = 0              # fullpath(str)
PARSER_PARSING_STARTED = 1      # directory(str)
PARSER_FILE_FOUND = 2           # directory(str), file_info, gio_file
PARSER_PARSING_FINISHED = 3     # directory(str)
# every MONITOR_X signals paired with
# file_monitor, gio_file_alfa, gio_file_beta, event_type, directory(str)
MONITOR_CHANGED = 4
MONITOR_CHANGES_DONE = 5
MONITOR_DELETED = 6
MONITOR_CREATED = 7
MONITOR_ATTRIBUTE_CHANGED = 8
MODEL_REQUEST_THUMBNAIL_TILE = 9    # start_path, end_path, directory
MODEL_CHANGE_SORT_COLUMN = 10   # column_id(int), order(int as Gtk.SortType)
FILTER_CHANGE_KEYWORD = 11      # keyword(str)
PATH_TO_SAVE_DEFINED = 12       # fullpath(str)
NEW_SELECTION = 13              # tree_row
CLEAR_SELECTION = 14            # None
SELECTION_CHANGED = 15          # [fullpath(str)]
VIEWERS_SWITCH_STACK_TO = 16    # FileViewerType.ICON_VIEW or TREE_VIEW
VIEWERS_TOGGLE_HIDDEN = 17      # bool
FINALIZE_DIALOG = 18            # None

NUMBER_OF_SIGNALS = 19
