
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .factory.Factory import FoxtrotFactory


class FoxtrotNamedBoxes:

    def set_model(self, box_id, item_model):
        if box_id not in self._named_boxes:
            print("WARNING > named box id not found.")
            return
        factory = FoxtrotFactory.get_default()
        factory.make_item(self._named_boxes[box_id], item_model)

    def add_box(self, box, box_id):
        self._named_boxes[box_id] = box

    def __init__(self):
        self._named_boxes = {}
