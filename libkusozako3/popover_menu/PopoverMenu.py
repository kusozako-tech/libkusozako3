
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Stack import DeltaStack
from .Popup import DeltaPopup
from .NamedBoxes import FoxtrotNamedBoxes


class DeltaPopoverMenu(Gtk.Popover, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        popover = DeltaPopoverMenu(parent)
        popover.set_model(model)
        return popover

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_named_box_added(self, user_data):
        box, box_id = user_data
        self._named_boxes.add_box(box, box_id)

    def _delta_call_popdown(self):
        self.popdown()

    def _on_map(self, popover):
        self._raise("delta > accel enabled", False)

    def _on_closed(self, popover):
        self._raise("delta > accel enabled", True)

    def add_item_for_model(self, box_id, model):
        self._named_boxes.set_model(box_id, model)

    def add_page_for_model(self, model):
        self._stack.set_page_model(model)

    def popup_for_gesture(self, gesture):
        self._popup.popup_for_gesture(gesture)

    def popup_for_align(self, widget, xalign, yalign):
        self._popup.popup_for_align(widget, xalign, yalign)

    def popup_for_position(self, widget, x, y):
        self._popup.popup_for_position(widget, x, y)

    def set_model(self, model):
        self._stack = DeltaStack.new_for_model(self, model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Popover.__init__(self)
        self.connect("map", self._on_map)
        self.connect("closed", self._on_closed)
        self._popup = DeltaPopup(self)
        self._named_boxes = FoxtrotNamedBoxes()
        self.set_constrain_to(Gtk.PopoverConstraint.NONE)
