
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .factory.Factory import FoxtrotFactory


class DeltaStack(Gtk.Stack, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        stack = cls(parent)
        factory = FoxtrotFactory.get_default()
        for page_model in model:
            factory.make_page(stack, page_model)
        return stack

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, page_name):
        self.set_visible_child_name(page_name)

    def _on_unmap(self, widget):
        self.set_visible_child_name("main")

    def set_page_model(self, page_model):
        factory = FoxtrotFactory.get_default()
        factory.make_page(self, page_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self, hexpand=True, vexpand=True)
        self.set_transition_duration(250)
        self.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.set_homogeneous(False)
        self.connect("unmap", self._on_unmap)
        self._raise("delta > add to container", self)
