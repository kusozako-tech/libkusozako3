
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk


class DeltaPopup:

    def _popup(self, widget, gdk_rectangle, position=Gtk.PositionType.TOP):
        self._popover.set_position(position)
        self._popover.set_relative_to(widget)
        self._popover.set_pointing_to(gdk_rectangle)
        self._popover.show_all()
        self._popover.popup()

    def popup_for_gesture(self, gesture):
        _, rectangle = gesture.get_bounding_box()
        widget = gesture.get_widget()
        if widget.get_allocated_height()/2 > rectangle.y:
            position = Gtk.PositionType.BOTTOM
        else:
            position = Gtk.PositionType.TOP
        self._popup(widget, rectangle, position)

    def popup_for_position(self, widget, x, y):
        point = Gdk.Rectangle()
        point.x = x
        point.y = y
        if y/widget.get_allocated_height() > 0.5:
            position = Gtk.PositionType.TOP
        else:
            position = Gtk.PositionType.BOTTOM
        self._popup(widget, point, position)

    def popup_for_align(self, widget, xalign, yalign):
        point = Gdk.Rectangle()
        rectangle, _ = widget.get_allocated_size()
        point.x = rectangle.width*xalign
        point.y = rectangle.height*yalign
        if yalign > 0.8:
            position = Gtk.PositionType.BOTTOM
        else:
            position = Gtk.PositionType.TOP
        self._popup(widget, point, position)

    def __init__(self, popover):
        self._popover = popover
