
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .PopoverItem import DeltaPopoverItem


class DeltaRecentPaths(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        recent_paths = cls(parent)
        recent_paths.set_model(model)

    def _clear_child_items(self):
        for child_item in self.get_children():
            child_item.destroy()

    def _add_child_items(self):
        for path in self._enquiry(self._model["query"]):
            DeltaPopoverItem.new(self, self._model, path)
        self.show_all()

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _on_map(self, box):
        self._clear_child_items()
        self._add_child_items()

    def set_model(self, model):
        self._model = model

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
        self.connect("map", self._on_map)
