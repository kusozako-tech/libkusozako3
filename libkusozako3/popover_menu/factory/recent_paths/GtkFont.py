
from gi.repository import Pango
from gi.repository import Gtk


def get(font_size=0, weight=Pango.Weight.NORMAL):
    settings = Gtk.Settings.get_default()
    font = settings.get_property("gtk-font-name")
    font_description = Pango.font_description_from_string(font)
    size = font_description.get_size()/Pango.SCALE
    font_description.set_size((size+font_size)*Pango.SCALE)
    font_description.set_weight(weight)
    return font_description
