
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _on_changed(self, entry):
        self._raise("delta > text changed", entry.get_text())

    def _on_icon_press(self, *args):
        self.set_text("")

    def _on_activate(self, entry):
        self._raise("delta > make directory")

    def _on_map(self, entry):
        entry.grab_focus()

    def _on_unmap(self, entry):
        entry.set_text("")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(
            self,
            hexpand=True,
            secondary_icon_name="edit-clear-symbolic",
            secondary_icon_tooltip_text=_("Clear"),
            placeholder_text=_("Directory Name")
            )
        default_text = self._enquiry("delta > model", "default")
        if default_text is not None:
            self.set_text(default_text)
        default_width = self._enquiry("delta > model", "default-width")
        if default_width is None:
            default_width = Unit(40)
        self.grab_focus()
        self.set_size_request(default_width, Unit(3))
        self.connect("map", self._on_map)
        self.connect("unmap", self._on_unmap)
        self.connect("changed", self._on_changed)
        self.connect("icon-press", self._on_icon_press)
        self.connect("activate", self._on_activate)
        self._raise("delta > add to container", self)
