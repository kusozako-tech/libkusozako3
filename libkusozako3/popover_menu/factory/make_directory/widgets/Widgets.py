
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .PathHandler import DeltaPathHandler
from .Entry import DeltaEntry
from .Button import DeltaButton


class DeltaWidgets(DeltaEntity):

    def _delta_call_text_changed(self, text):
        is_valid = self._path_handler.set_basename(text)
        self._button.set_sensitive(is_valid)

    def _delta_call_make_directory(self):
        self._path_handler.try_make_directory()

    def __init__(self, parent):
        self._parent = parent
        self._path_handler = DeltaPathHandler(self)
        DeltaEntry(self)
        self._button = DeltaButton(self)
