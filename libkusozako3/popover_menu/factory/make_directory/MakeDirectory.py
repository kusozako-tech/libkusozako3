
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .widgets.Widgets import DeltaWidgets


class DeltaMakeDirectory(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        box = cls(parent)
        box.set_model(model)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_model(self, key):
        return self._model[key] if key in self._model else None

    def set_model(self, item_model):
        self._model = item_model
        DeltaWidgets(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            spacing=Unit(1),
            border_width=Unit(1),
            orientation=Gtk.Orientation.HORIZONTAL
            )
        self._raise("delta > add to container", self)
