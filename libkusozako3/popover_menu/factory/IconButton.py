
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .PopoverButton import AlfaPopoverButton

SIZE = Gtk.IconSize.MENU


class DeltaIconButton(AlfaPopoverButton):

    def set_model(self, item_model):
        image = Gtk.Image.new_from_icon_name(item_model["icon-name"], SIZE)
        self.set_image(image)
        self.props.tooltip_text = item_model.get("tooltip-text", None)
        self.connect("clicked", self._on_clicked, item_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("delta > css", (self, "popover-button"))
        self._raise("delta > add to container", self)
