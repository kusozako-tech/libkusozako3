
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaPathHandler(DeltaEntity):

    def _get_fullpath(self, text):
        if text == "" or "/" in text:
            return False, None
        query = self._enquiry("delta > model", "query")
        gio_file = self._enquiry(query)
        parent_gio_file = gio_file.get_parent()
        fullpath = GLib.build_filenamev([parent_gio_file.get_path(), text])
        is_valid = not GLib.file_test(fullpath, GLib.FileTest.EXISTS)
        return is_valid, fullpath

    def set_basename(self, basename):
        self._is_valid, self._fullpath = self._get_fullpath(basename)
        return self._is_valid

    def _on_progress(self, current_byte, total_byte, user_data):
        print("renaming progress", current_byte/total_byte)

    def try_rename(self):
        if not self._is_valid:
            return
        query = self._enquiry("delta > model", "query")
        source_gio_file = self._enquiry(query)
        destination_gio_file = Gio.File.new_for_path(self._fullpath)
        source_gio_file.move(
            destination_gio_file,
            Gio.FileCopyFlags.ALL_METADATA,
            None,
            self._on_progress,
            ("",)
            )
        self._raise("delta > popdown")

    def __init__(self, parent):
        self._parent = parent
        self._is_valid = False
        self._fullpath = ""
