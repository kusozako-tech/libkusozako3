
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _on_changed(self, entry):
        self._raise("delta > text changed", entry.get_text())

    def _on_icon_press(self, *args):
        self.set_text("")

    def _on_activate(self, entry):
        self._raise("delta > rename")

    def _on_map(self, entry):
        query = self._enquiry("delta > model", "query")
        gio_file = self._enquiry(query)
        if gio_file is None:
            return
        basename = gio_file.get_basename()
        stem = basename.rsplit(".", 1)[0]
        self.grab_focus()
        self.set_text(basename)
        self.select_region(0, len(stem))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(
            self,
            hexpand=True,
            secondary_icon_name="edit-clear-symbolic",
            secondary_icon_tooltip_text=_("Clear"),
            placeholder_text=_("New Name")
            )
        self.connect("map", self._on_map)
        self.set_size_request(Unit(40), Unit(3))
        self.connect("changed", self._on_changed)
        self.connect("icon-press", self._on_icon_press)
        self.connect("activate", self._on_activate)
        self._raise("delta > add to container", self)
