
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > rename")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, _("Rename"), sensitive=False)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
