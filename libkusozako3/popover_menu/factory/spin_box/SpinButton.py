
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaSpinButton(Gtk.SpinButton, DeltaEntity):

    def _set_value(self):
        query = self._enquiry("delta > model", "query")
        query_data = self._enquiry("delta > model", "query-data")
        self.set_value(self._enquiry(query, query_data))

    def _on_value_changed(self, spin_box):
        message = self._enquiry("delta > model", "message")
        user_data = self._enquiry("delta > model", "user-data")
        self._raise(message, user_data+(spin_box.get_value(),))

    def receive_transmission(self, user_data):
        group, key, value = user_data
        model_user_data = self._enquiry("delta > model", "user-data")
        if group != model_user_data[0] or key != model_user_data[1]:
            return
        self._set_value()

    def __init__(self, parent):
        self._parent = parent
        Gtk.SpinButton.__init__(self, margin=Unit(1), numeric=True)
        self.set_increments(*self._enquiry("delta > model", "increments"))
        self.set_range(*self._enquiry("delta > model", "range"))
        self._set_value()
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "popover-spin-button"))
        registration_message = self._enquiry("delta > model", "registration")
        self._raise(registration_message, self)
        self.connect("value-changed", self._on_value_changed)
