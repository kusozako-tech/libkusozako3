
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Ux import Unit
from .PopoverButton import AlfaPopoverButton


class DeltaSimpleAction(AlfaPopoverButton):

    def _on_clicked(self, widget, item_model):
        if item_model.get("close-on-clicked", False):
            self._raise("delta > popdown")
        self._raise(item_model["message"], item_model["user-data"])

    def _on_realize(self, widget):
        align = widget.get_child()
        align.set_halign(Gtk.Align.END)

    def set_model(self, item_model):
        self.set_label(item_model["title"])
        self.connect("clicked", self._on_clicked, item_model)
        self._try_register_shortcut(item_model)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(None, Gtk.IconSize.MENU)
        image.set_margin_end(Unit(1))
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self._raise("delta > css", (self, "popover-button"))
        self.connect("realize", self._on_realize)
        self._raise("delta > add to container", self)
