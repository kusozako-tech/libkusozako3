
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Ux import Unit
from .PopoverButton import AlfaPopoverButton


class DeltaCheckAction(AlfaPopoverButton):

    def _on_clicked(self, widget, item_model):
        self._raise(item_model["message"], item_model["user-data"])
        if item_model["close-on-clicked"]:
            self._raise("delta > popdown")

    def _on_realize(self, widget):
        align = widget.get_child()
        align.set_halign(Gtk.Align.END)
        self._reset_icon()

    def _reset_icon(self, *args):
        state = self._enquiry(self._model["query"], self._model["query-data"])
        checked = (state == self._model["check-value"])
        icon_name = "object-select-symbolic" if checked else None
        self._image.set_from_icon_name(icon_name, Gtk.IconSize.MENU)

    def receive_transmission(self, user_data):
        self._reset_icon()

    def set_model(self, item_model):
        self._model = item_model
        self.connect("realize", self._on_realize)
        self.set_label(item_model["title"])
        self.connect("clicked", self._on_clicked, item_model)
        self._try_register_shortcut(item_model)
        registration_signal = item_model.get("registration", None)
        if registration_signal is not None:
            self._raise(registration_signal, self)
        else:
            self.connect("map", self._reset_icon)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("delta > css", (self, "popover-button"))
        self._image = Gtk.Image.new_from_icon_name(
            "object-select-symbolic",
            Gtk.IconSize.MENU
            )
        self._image.set_margin_end(Unit(1))
        self.set_image(self._image)
        self._raise("delta > add to container", self)
