
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class AlfaPopoverButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, item_model):
        item = cls(parent)
        item.set_model(item_model)

    def _on_clicked(self, widget, item_model):
        self._raise(item_model["message"], item_model["user-data"])
        if item_model.get("close-on-clicked", False):
            self._raise("delta > popdown")

    def _try_register_shortcut(self, item_model):
        if "shortcut" not in item_model:
            return
        shortcut = item_model["shortcut"]
        self.props.tooltip_text = shortcut
        if item_model["message"] != "delta > action":
            return
        data = item_model["user-data"], shortcut, item_model["title"]
        self._raise("delta > register action", data)
