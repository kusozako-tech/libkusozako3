
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Ux import Unit
from libkusozako3.Entity import DeltaEntity

MARGIN = Unit(0.5)


class DeltaSeparator(Gtk.Separator, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        cls(parent)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Separator.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_margin_top(MARGIN)
        self.set_margin_bottom(MARGIN)
        self.set_margin_start(MARGIN)
        self.set_margin_end(MARGIN)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "popover-separator"))
