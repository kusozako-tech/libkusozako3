
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity

DELAY = 10


class DeltaDelay(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        delay = cls(parent)
        delay.set_model(model)
        return delay

    def _on_idle(self, rate, index):
        if index == self._index:
            self._raise(self._message, self._user_data+(rate,))
        return False

    def set_model(self, model):
        self._message = model["message"]
        self._user_data = model["user-data"]

    def start_idle(self, rate):
        index = self._index+1
        GLib.timeout_add(DELAY, self._on_idle, rate, index)
        self._index += 1

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
