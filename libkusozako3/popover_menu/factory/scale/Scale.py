
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Data import DeltaData
from .Box import DeltaBox


class DeltaScale(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        scale = cls(parent)
        scale.set_model(model)

    def _delta_info_rate(self):
        return self._data.rate

    def _delta_info_label(self):
        return self._data.label

    def _delta_call_rate(self, rate):
        self._data.set_rate(rate)

    def _delta_call_register_data_object(self, object_):
        self._data.register_listener(object_)

    def set_model(self, model):
        self._data = DeltaData.new_for_model(self, model)
        DeltaBox(self)

    def __init__(self, parent):
        self._parent = parent
