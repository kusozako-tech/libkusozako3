
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

RGBA_QUERY = "css", "primary_fg_color", "rgb(128,128,128)"
OFFSET = Unit(2)
LINE_WIDTH = Unit(1)/4
RADIUS = Unit(1)


class DeltaPainters(DeltaEntity):

    def _set_rgba(self, cairo_context):
        rgba_config = self._enquiry("delta > settings", RGBA_QUERY)
        rgba = Gdk.RGBA()
        rgba.parse(rgba_config)
        Gdk.cairo_set_source_rgba(cairo_context, rgba)

    def _paint_line(self, cairo_context, rectangle):
        y = rectangle.height/2
        cairo_context.set_line_width(LINE_WIDTH)
        cairo_context.move_to(OFFSET, y)
        cairo_context.line_to(rectangle.width-OFFSET, y)
        cairo_context.stroke()

    def _paint_arc(self, cairo_context, rectangle):
        rate = self._enquiry("delta > rate")
        value = (rectangle.width-OFFSET*2)*rate
        width = OFFSET+value
        height = rectangle.height/2
        cairo_context.arc(width, height, RADIUS, 0, 2*GLib.PI)
        cairo_context.fill()

    def _on_draw(self, drawing_area, cairo_context):
        self._set_rgba(cairo_context)
        rectangle, _ = drawing_area.get_allocated_size()
        self._paint_line(cairo_context, rectangle)
        self._paint_arc(cairo_context, rectangle)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        drawing_area.connect("draw", self._on_draw)
