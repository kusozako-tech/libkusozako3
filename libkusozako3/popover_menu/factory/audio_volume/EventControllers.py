
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

OFFSET = Unit(2)
MASK = Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_MOTION_MASK


class DeltaEventControllers(DeltaEntity):

    def _on_update(self, gesture, sequence):
        drawing_area = self._enquiry("delta > drawing area")
        drawing_area_height = drawing_area.get_allocated_height()
        _, _, y = gesture.get_point(sequence)
        value = drawing_area_height-(OFFSET+y)
        rate = value/(drawing_area_height-OFFSET*2)
        self._raise("delta > rate", rate)

    def _on_map(self, widget):
        # a fukin' hack to avoid losing gestures.
        rate = self._enquiry("delta > rate")
        self._raise("delta > rate", rate)

    def __init__(self, parent):
        self._parent = parent
        drawing_area = self._enquiry("delta > drawing area")
        gdk_window = drawing_area.get_window()
        gdk_window.set_events(MASK)
        drawing_area.connect("map", self._on_map)
        self._single = Gtk.GestureSingle(widget=drawing_area, button=1)
        self._single.connect("begin", self._on_update)
        self._drag = Gtk.GestureDrag(widget=drawing_area, button=1)
        self._drag.connect("update", self._on_update)
