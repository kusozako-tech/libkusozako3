
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Delay import DeltaDelay


class DeltaData(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        data = cls(parent)
        data.set_model(model)
        return data

    def set_rate(self, rate):
        self._rate = max(self._min, min(self._max, round(rate, self._round)))
        for listener in self._listeners:
            listener.receive_transmission()
        self._delay.start_idle(self._rate)

    def set_model(self, model):
        self._delay = DeltaDelay.new_for_model(self, model)
        self._max = model["maximum"]
        self._min = model["minimum"]
        self._round = model["round"]
        self._rate = self._enquiry(model["query"], model["query-data"])

    def register_listener(self, object_):
        self._listeners.append(object_)

    def __init__(self, parent):
        self._parent = parent
        self._listeners = []

    @property
    def rate(self):
        return self._rate
