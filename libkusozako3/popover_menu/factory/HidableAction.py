
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Ux import Unit
from .PopoverButton import AlfaPopoverButton


class DeltaHidableAction(AlfaPopoverButton):

    def _on_realize(self, widget):
        align = widget.get_child()
        align.set_halign(Gtk.Align.END)
        self._reset_revealed()

    def _reset_revealed(self):
        state = self._enquiry(self._model["query"], self._model["query-data"])
        revealed = (state == self._model["check-value"])
        if "check-inverted" in self._model and self._model["check-inverted"]:
            revealed = not revealed
        self._revealer.set_reveal_child(revealed)

    def receive_transmission(self, user_data):
        self._reset_revealed()

    def set_model(self, item_model):
        self._model = item_model
        self.connect("realize", self._on_realize)
        self.set_label(item_model["title"])
        self.connect("clicked", self._on_clicked, item_model)
        self._try_register_shortcut(item_model)
        self._raise(item_model["registration"], self)

    def __init__(self, parent):
        self._parent = parent
        self._revealer = Gtk.Revealer()
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("delta > css", (self, "popover-button"))
        self._image = Gtk.Image.new_from_icon_name(None, Gtk.IconSize.MENU)
        self._image.set_margin_end(Unit(1))
        self.set_image(self._image)
        self._revealer.add(self)
        self._revealer.set_reveal_child(True)
        self._raise("delta > add to container", self._revealer)
