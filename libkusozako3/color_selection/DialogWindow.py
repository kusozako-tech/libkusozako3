
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .header_bar.HeaderBar import DeltaHeaderBar
from .ColorSelectionWidget import DeltaColorSelectionWidget


class DeltaDialogWindow(Gtk.Dialog, DeltaEntity):

    def _delta_call_dialog_response(self, response):
        self.response(response)

    def _delta_call_add_to_container(self, widget):
        container = self.get_content_area()
        container.add(widget)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        Gtk.Dialog.__init__(self, "", window, Gtk.ResponseType.CANCEL)
        content_area = self.get_content_area()
        content_area.set_border_width(0)
        self.set_decorated(False)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_keep_above(True)
        DeltaHeaderBar(self)
        DeltaColorSelectionWidget(self)
        self.show_all()
