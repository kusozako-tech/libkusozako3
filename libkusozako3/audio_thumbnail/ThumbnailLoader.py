
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class AlfaThumbnailLoader:

    def _get_stream_for_path(self, path):
        raise NotImplementedError

    def _get_thumbnail_size(self, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        rate = min(width/self._maximum_size, height/self._maximum_size)
        return rate, (width/rate), (height/rate)

    def _get_scaled(self, stream):
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, None)
        rate, width, height = self._get_thumbnail_size(pixbuf)
        if 1 >= rate:
            return pixbuf
        return pixbuf.scale_simple(width, height, INTERP_TYPE)

    def load_thumbnail(self, path):
        stream = self._get_stream_for_path(path)
        if stream is not None:
            return self._get_scaled(stream)
        return None

    def __init__(self, maximum_size):
        self._maximum_size = maximum_size
