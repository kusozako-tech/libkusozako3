
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import FontDescription
from .DialogWindow import DeltaDialogWindow


class DeltaFontChooser(Gtk.Dialog, DeltaEntity):

    @classmethod
    def get_css_font(cls, parent, current_css_font=None):
        chooser = cls(parent)
        current_font = FontDescription.from_css_font(current_css_font)
        chooser.set_font(current_font)
        font_description = chooser.get_font_description()
        if font_description is None:
            return None
        return FontDescription.to_css_font(font_description)

    @classmethod
    def get_font(cls, parent, current_font=None):
        chooser = cls(parent)
        chooser.set_font(current_font)
        return chooser.get_font_description()

    def _delta_call_register_font_chooser_callback(self, callback):
        self._callback = callback

    def _delta_info_font_description(self):
        return self._font_description

    def set_font(self, current_font=None):
        self._font_description = current_font
        self._dialog_window = DeltaDialogWindow(self)
        self._dialog_window.show_all()

    def get_font_description(self):
        response_id = self._dialog_window.run()
        is_cancelled = (response_id != Gtk.ResponseType.APPLY)
        font = None if is_cancelled else self._callback()
        self._dialog_window.destroy()
        return font

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        Gtk.Dialog.__init__(self, "", window, Gtk.ResponseType.CANCEL)
