
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaContentArea(Gtk.FontChooserWidget, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.FontChooserWidget.__init__(self, vexpand=True, margin=Unit(2))
        font_description = self._enquiry("delta > font description")
        if font_description is not None:
            self.set_font_desc(font_description)
        self._raise("delta > add to container", self)
        self._raise(
            "delta > register font chooser callback",
            self.get_font_desc
            )
