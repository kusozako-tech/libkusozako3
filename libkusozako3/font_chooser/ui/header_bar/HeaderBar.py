
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Box import DeltaBox
from .event_controllers.EventControllers import DeltaEventControllers


class DeltaHeaderBar(Gtk.EventBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_event_box(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        DeltaEventControllers(self)
        DeltaBox(self)
        self._raise("delta > css", (self, "chooser-header"))
        self._raise("delta > add to container", self)
