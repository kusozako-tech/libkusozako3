
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import magic
from gi.repository import Gio


def __get_mime_by_magic(file_info, gio_file):
    try:
        mime = magic.Magic(mime=True)
        return mime.from_file(gio_file.get_path())
    except (PermissionError, FileNotFoundError):
        return file_info.get_content_type()


def from_file_info(gio_file, file_info):
    if Gio.FileType.DIRECTORY == file_info.get_file_type():
        return "inode/directory"
    return __get_mime_by_magic(file_info, gio_file)


def from_gio_file(gio_file):
    gio_file_info = gio_file.query_info("*", 0)
    return from_file_info(gio_file, gio_file_info)
