
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Labels import DeltaLabels
from .ImageButton import DeltaImageButton
from .Entries import DeltaEntries


class DeltaContentArea(Gtk.Grid, DeltaEntity):

    def _delta_call_attach_to_grid(self, user_data):
        widget, geometries = user_data
        self.attach(widget, *geometries)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(
            self,
            border_width=Unit(2),
            column_spacing=Unit(2),
            row_spacing=Unit(2)
            )
        DeltaLabels(self)
        DeltaImageButton(self)
        DeltaEntries(self)
        self._raise("delta > add to container", self)
        self.set_opacity(0.8)
        self._raise("delta > css", (self, "primary-surface-color-class"))
