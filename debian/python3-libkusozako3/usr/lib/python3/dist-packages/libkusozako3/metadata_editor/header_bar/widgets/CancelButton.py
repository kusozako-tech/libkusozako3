
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaCancelButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > dialog response", Gtk.ResponseType.CANCEL)

    def __init__(self, parent):
        self._parent = parent
        label = self._enquiry("delta > model", "cancel-button-label")
        if label is None:
            label = _("Cancel")
        Gtk.Button.__init__(self, label, relief=Gtk.ReliefStyle.NONE)
        self.set_size_request(-1, Unit(6))
        self._raise("delta > pack start", self)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > css", (self, "button-safe"))
