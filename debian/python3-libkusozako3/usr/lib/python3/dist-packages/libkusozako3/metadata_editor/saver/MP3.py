
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from mutagen.id3 import ID3
from mutagen.id3 import APIC
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from mutagen.id3 import TIT2
from mutagen.id3 import TPE1
from mutagen.id3 import TALB

KEYS = {"TIT2": "music-title", "TPE1": "artist", "TALB": "album"}
METHODS = {"TIT2": TIT2, "TPE1": TPE1, "TALB": TALB}


class DeltaMP3(DeltaEntity):

    def _get_pixbuf(self, cover_art_path, size):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(cover_art_path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = max(size/width, size/height)
        return pixbuf.scale_simple(
            width*scale,
            height*scale,
            GdkPixbuf.InterpType.HYPER
            )

    def _try_set_coverart(self, id3):
        cover_art_path = self._enquiry("delta > model", "cover-art-path")
        if cover_art_path is None:
            return
        pixbuf = self._get_pixbuf(cover_art_path, 300)
        success, bytes_ = pixbuf.save_to_bufferv("png", [], [])
        if success:
            id3["APIC"] = APIC(
                encoding=3,
                mime='image/png',
                type=3,
                desc=u'Cover',
                data=bytes_
                )

    def apply(self, gio_file):
        source_path = gio_file.get_path()
        id3 = ID3()
        for mutagen_key, model_key in KEYS.items():
            model_value = self._enquiry("delta > model", model_key)
            if model_value:
                method = METHODS[mutagen_key]
                id3[mutagen_key] = method(encoding=3, text=model_value)
        self._try_set_coverart(id3)
        id3.save(source_path)

    def __init__(self, parent):
        self._parent = parent
