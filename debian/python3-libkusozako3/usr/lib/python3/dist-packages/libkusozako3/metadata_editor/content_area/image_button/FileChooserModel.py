
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


MODEL = {
    "type": "select-file",
    "title": _("Select Cover Art Image"),
    "filters": {"PNG": "image/png", "JPEG": "image/jpeg"}
    }
