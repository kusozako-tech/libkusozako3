
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .model.Model import DeltaModel
from .saver.Saver import DeltaSaver
from .DialogWindow import DeltaDialogWindow


class DeltaMetadataEditor(DeltaEntity):

    @classmethod
    def new_for_gio_file(cls, parent, gio_file):
        metadata_editor = cls(parent)
        if not metadata_editor.setup_model(gio_file):
            return False
        return metadata_editor.open_dialog_window()

    def _delta_info_model(self, key):
        return self._model[key]

    def _delta_call_model(self, user_data):
        key, value = user_data
        self._model[key] = value

    def open_dialog_window(self):
        dialog_window = DeltaDialogWindow(self)
        dialog_window.show_all()
        response = dialog_window.run()
        if response == Gtk.ResponseType.APPLY:
            DeltaSaver.save(self)
        dialog_window.destroy()
        return response == Gtk.ResponseType.APPLY

    def setup_model(self, gio_file):
        self._model = DeltaModel.new_for_gio_file(self, gio_file)
        return self._model is not None

    def __init__(self, parent):
        self._parent = parent
