
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

MODELS = (
    (_("Cover Art :"), (0, 0, 1, 1)),
    (_("Title :"), (0, 1, 1, 1)),
    (_("Artist :"), (0, 2, 1, 1)),
    (_("Album :"), (0, 3, 1, 1)),
    )


class DeltaLabels(DeltaEntity):

    def _set_label_for_model(self, model):
        label_text, geometries = model
        label = Gtk.Label(label_text)
        label.props.margin = Unit(1)
        label.set_xalign(1)
        self._raise("delta > attach to grid", (label, geometries))

    def __init__(self, parent):
        self._parent = parent
        for model in MODELS:
            self._set_label_for_model(model)
