
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.mime_type import MimeType

ACCEPTABLE_TYPES = "audio/x-m4a", "audio/mpeg"


def check_for_gio_file(gio_file):
    if not gio_file.query_exists(None):
        return False
    return MimeType.from_gio_file(gio_file) in ACCEPTABLE_TYPES
