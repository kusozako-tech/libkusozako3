
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from .image_button.FileChooserModel import MODEL

VISIBLE_SIZE = 200


class DeltaImageButton(Gtk.Button, DeltaEntity):

    def _get_pixbuf(self, cover_art_path, size):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(cover_art_path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = max(size/width, size/height)
        return pixbuf.scale_simple(
            width*scale,
            height*scale,
            GdkPixbuf.InterpType.HYPER
            )

    def _on_clicked(self, button):
        path = DeltaFileChooser.run_for_model(self, MODEL)
        if path is None:
            return
        pixbuf = self._get_pixbuf(path, VISIBLE_SIZE)
        image = button.get_image()
        image.set_from_pixbuf(pixbuf)
        self._raise("delta > model", ("cover-art-path", path))

    def __init__(self, parent):
        self._parent = parent
        pixbuf = self._enquiry("delta > model", "cover-art")
        image = Gtk.Image.new_from_pixbuf(pixbuf)
        image.set_size_request(VISIBLE_SIZE, VISIBLE_SIZE)
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=image,
            border_width=Unit(1)
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > attach to grid", (self, (1, 0, 1, 1)))
