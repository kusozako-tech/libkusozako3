
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType
from .MP4 import DeltaMP4
from .MP3 import DeltaMP3


class DeltaSaver(DeltaEntity):

    @classmethod
    def save(cls, parent):
        saver = cls(parent)
        saver.apply_model()

    def apply_model(self):
        gio_file = self._enquiry("delta > model", "gio-file")
        mime_type = MimeType.from_gio_file(gio_file)
        if mime_type == "audio/x-m4a":
            self._mp4.apply(gio_file)
        elif mime_type == "audio/mpeg":
            self._mp3.apply(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._mp3 = DeltaMP3(self)
        self._mp4 = DeltaMP4(self)
