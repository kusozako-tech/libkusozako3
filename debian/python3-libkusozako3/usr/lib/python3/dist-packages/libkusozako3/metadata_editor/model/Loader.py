
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.audio_metadata.AudioMetadata import FoxtrotAudioMetadata
from libkusozako3.audio_thumbnail.AudioThumbnail import FoxtrotAudioThumbnail


class DeltaLoader(DeltaEntity):

    @classmethod
    def load_for_gio_file(cls, parent, gio_file):
        loader = cls(parent)
        loader.load(gio_file)

    def load(self, gio_file):
        uri = gio_file.get_uri()
        title, artist, album = self._audio_metadata.get_from_uri(uri, "")
        if title == "":
            title = gio_file.get_basename()
        self._raise("delta > set model value", ("music-title", title))
        self._raise("delta > set model value", ("artist", artist))
        self._raise("delta > set model value", ("album", album))
        cover_art = self._audio_thumbnail.get_for_uri(uri)
        self._raise("delta > set model value", ("cover-art", cover_art))

    def __init__(self, parent):
        self._parent = parent
        self._audio_metadata = FoxtrotAudioMetadata()
        self._audio_thumbnail = FoxtrotAudioThumbnail.new_for_maximum_size(150)
