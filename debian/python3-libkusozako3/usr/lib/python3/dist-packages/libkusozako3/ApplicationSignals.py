
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# do not use positive numbers for implemented signals

FILE_NEW = -1                       # None
FILE_OPEN = -2                      # None
FILE_SAVE = -3                      # None
FILE_SAVE_AS = -4                   # None
FILE_SELECTED = -5                  # gio_file as selected file
EDIT_UNDO = -6                      # None
EDIT_REDO = -7                      # None
EDIT_CUT = -8                       # None
EDIT_COPY = -9                      # None
EDIT_PASTE = -10                    # None
WINDOW_TOGGLE_MAXIMIZED = -11       # None
WINDOW_TOGGLE_FULLSCREEN = -12      # None
WINDOW_ICONFY = -13                 # None
WINDOW_CLOSE = -14                  # None
FINDER_TOGGLE = -15                 # None
FINDER_KEYWORD_CHANGED = -16        # str as keyword
