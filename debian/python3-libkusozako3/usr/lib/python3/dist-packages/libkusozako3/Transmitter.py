

class FoxtrotTransmitter:

    def transmit(self, user_data):
        for listener in self._listeners:
            listener.receive_transmission(user_data)

    def register_listener(self, object_):
        self._listeners.append(object_)

    def __init__(self):
        self._listeners = []
