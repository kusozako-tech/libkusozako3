
import re
from gi.repository import GLib
from gi.repository import Gio


def _get_stem_and_index(stem):
    match = re.search(r"\((\d+)\)\Z", stem)
    if match is None:
        return stem, 0
    matched_string = match.group()
    index = int(matched_string[1:-1])
    return stem[:-1*len(matched_string)], index


def _build(directory, stem, index, suffix):
    while True:
        index += 1
        basename = "{}({}){}".format(stem, index, suffix)
        path = GLib.build_filenamev([directory, basename])
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            return path


def _get_numbered_name(destination):
    gio_file = Gio.File.new_for_path(destination)
    directory = gio_file.get_parent().get_path()
    basename = gio_file.get_basename()
    names = basename.rsplit(".", 1)
    stem, index = _get_stem_and_index(names[0])
    suffix = "" if len(names) == 1 else "."+names[1]
    return _build(directory, stem, index, suffix)


def get_path(destination):
    if not GLib.file_test(destination, GLib.FileTest.EXISTS):
        return destination
    return _get_numbered_name(destination)
