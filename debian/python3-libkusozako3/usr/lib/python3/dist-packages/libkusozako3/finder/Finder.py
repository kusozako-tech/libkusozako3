
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Button import DeltaButton
from libkusozako3 import ApplicationSignals

MODEL = {
    "type": "simple-action",
    "title": _("Find"),
    "message": "delta > action",
    "user-data": (ApplicationSignals.FINDER_TOGGLE, None),
    "shortcut": "Ctrl+F",
    "close-on-clicked": True
    }


class DeltaFinder(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        DeltaButton(self)
        param = "main", MODEL
        self._raise("delta > application popover add item", param)
