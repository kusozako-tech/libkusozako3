
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .image_viewer.ImageViewer import DeltaImageViewer
from .media_player.MediaPlayer import DeltaMediaPlayer
from .text_viewer.TextViewer import DeltaTextViewer
from .pdf_viewer.PdfViewer import DeltaPdfViewer
from .WebView import DeltaWebView

PREVIEWERS = {
    "image": "_image_viewer",
    "audio": "_media_player",
    "video": "_media_player",
    "text/html": "_web_view",
    "text": "_text_viewer",
    "application/pdf": "_pdf_viewer"
}


class DeltaPreviewersStack(Gtk.Stack, DeltaEntity):

    def try_show_preview(self, file_info, gio_file, mime_type):
        self._media_player.stop()
        for key, previewer_name in PREVIEWERS.items():
            if mime_type.startswith(key):
                previewer = getattr(self, previewer_name)
                previewer.set_data(file_info, gio_file, mime_type)
                return True
        return False

    def _delta_call_switch_previewer_to(self, name):
        self.set_visible_child_name(name)

    def _delta_call_add_preview(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def stop_media(self):
        self._media_player.stop()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self._image_viewer = DeltaImageViewer(self)
        self._media_player = DeltaMediaPlayer(self)
        self._text_viewer = DeltaTextViewer(self)
        self._pdf_viewer = DeltaPdfViewer(self)
        self._web_view = DeltaWebView(self)
        self._raise("delta > add to container", self)
