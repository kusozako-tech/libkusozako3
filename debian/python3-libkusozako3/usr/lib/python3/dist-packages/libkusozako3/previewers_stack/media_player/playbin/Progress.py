
from gi.repository import Gst
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaProgress(DeltaEntity):

    @classmethod
    def new_for_playbin(cls, parent, playbin):
        progress = cls(parent)
        progress.set_playbin(playbin)

    def _timeout(self, playbin):
        _, duration = playbin.query_duration(Gst.Format.TIME)
        _, position = playbin.query_position(Gst.Format.TIME)
        self._raise("delta > position updated", (position, duration))
        return GLib.SOURCE_CONTINUE

    def set_playbin(self, playbin):
        GLib.timeout_add(200, self._timeout, playbin)

    def __init__(self, parent):
        self._parent = parent
