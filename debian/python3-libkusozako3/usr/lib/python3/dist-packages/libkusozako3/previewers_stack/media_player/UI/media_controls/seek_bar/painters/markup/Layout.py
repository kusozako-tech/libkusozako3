
from gi.repository import Pango
from gi.repository import PangoCairo
from libkusozako3.Entity import DeltaEntity
from . import GtkFont
from .Markup import DeltaMarkup

FONT_DESCRIPTION = GtkFont.get(font_size=0, weight=400)


class DeltaLayout(DeltaEntity):

    def _get_y_offset(self, pango_layout, height):
        _, rectangle_logical = pango_layout.get_extents()
        layout_height = rectangle_logical.height/Pango.SCALE
        return (height-layout_height)/2

    def get_for_context(self, cairo_context):
        size = self._enquiry("delta > allocated size")
        layout = PangoCairo.create_layout(cairo_context)
        layout.set_alignment(Pango.Alignment.CENTER)
        layout.set_markup(self._markup.get_current(), -1)
        layout.set_width(size.width*Pango.SCALE)
        layout.set_height(size.height*Pango.SCALE)
        layout.set_font_description(FONT_DESCRIPTION)
        return layout, self._get_y_offset(layout, size.height)

    def __init__(self, parent):
        self._parent = parent
        self._markup = DeltaMarkup(self)
