
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaPaintMotion(DeltaEntity):

    def paint(self, cairo_context):
        yuki_pointed = self._enquiry("delta > pointed position")
        if yuki_pointed is None:
            return
        yuki_size = self._enquiry("delta > allocated size")
        yuki_rgba = self._enquiry("delta > rgba")
        yuki_red, yuki_green, yuki_blue, yuki_alpha = yuki_rgba
        yuki_new_rgba = yuki_red, yuki_green, yuki_blue, yuki_alpha*0.5
        cairo_context.set_source_rgba(*yuki_new_rgba)
        cairo_context.rectangle(
            0,
            0,
            yuki_size.width * yuki_pointed,
            yuki_size.height
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
