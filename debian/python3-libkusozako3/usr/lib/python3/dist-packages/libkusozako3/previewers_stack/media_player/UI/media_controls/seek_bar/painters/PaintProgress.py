
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

HEIGHT = Unit(1)/2


class DeltaPaintProgress(DeltaEntity):

    def paint(self, cairo_context):
        size = self._enquiry("delta > allocated size")
        current, duration = self._enquiry("delta > timing")
        rate = current/duration
        rgba = self._enquiry("delta > rgba")
        cairo_context.set_source_rgba(*rgba)
        cairo_context.rectangle(
            0,
            size.height - HEIGHT,
            size.width * rate,
            HEIGHT
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
