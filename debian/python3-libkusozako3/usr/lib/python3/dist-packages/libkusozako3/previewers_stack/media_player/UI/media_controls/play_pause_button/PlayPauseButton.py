
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity

SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaPlayPauseButton(Gtk.Button, DeltaEntity):

    def _set_to_pause(self):
        image = self.get_image()
        image.set_from_icon_name("media-playback-pause-symbolic", SIZE)
        self._message_value = Gst.State.PAUSED
        self.props.tooltip_text = _("Pause")

    def _set_to_play(self):
        image = self.get_image()
        image.set_from_icon_name("media-playback-start-symbolic", SIZE)
        self._message_value = Gst.State.PLAYING
        self.props.tooltip_text = _("Play")

    def _reset(self, value):
        if value == Gst.State.PLAYING:
            self._set_to_pause()
        else:
            self._set_to_play()

    def _on_clicked(self, button):
        self._raise("delta > player state", self._message_value)

    def receive_transmission(self, user_data):
        type_, value = user_data
        if type_ == "state-changed":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image()
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register player object", self)
