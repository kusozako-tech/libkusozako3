
from gi.repository import Gdk
from gi.repository import PangoCairo
from libkusozako3.Entity import DeltaEntity
from .markup.Layout import DeltaLayout

COLOR_QUERY = "css", "primary_surface_fg_color", "rgba(1, 1, 1, 1)"


class DeltaPaintMarkup(DeltaEntity):

    def paint(self, cairo_context):
        Gdk.cairo_set_source_rgba(cairo_context, self._rgba)
        layout, y_offset = self._layout.get_for_context(cairo_context)
        cairo_context.move_to(0, y_offset)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)

    def _reset(self):
        rgba = Gdk.RGBA()
        rgba.parse(self._enquiry("delta > settings", COLOR_QUERY))
        self._rgba = rgba

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "css":
            self._reset()

    def __init__(self, parent):
        self._parent = parent
        self._reset()
        self._layout = DeltaLayout(self)
        self._raise("delta > register settings object", self)
