
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .painters.Painters import DeltaPainters
from .event_controllers.EventControllers import DeltaEventControllers


class DeltaSeekBar(Gtk.DrawingArea, DeltaEntity):

    def _delta_info_allocated_size(self):
        yuki_size, _ = self.get_allocated_size()
        return yuki_size

    def _delta_info_pointed_position(self):
        return self._event_controllers.pointer_position

    def _delta_info_drawing_area(self):
        return self

    def _delta_call_queue_draw(self):
        self.queue_draw()

    def _on_draw(self, drawing_area, cairo_context):
        self._painters.paint(cairo_context)

    def _on_realize(self, drawing_area):
        self._event_controllers = DeltaEventControllers(self)
        self._painters = DeltaPainters(self)
        self.connect("draw", self._on_draw)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.connect("realize", self._on_realize)
        self.set_hexpand(True)
        self.set_size_request(-1, Unit(5))
        self._raise("delta > add to container", self)
