
from libkusozako3.Entity import DeltaEntity


class DeltaMarkup(DeltaEntity):

    def _get_readable(self, nanosecond):
        yuki_second = nanosecond/pow(1000, 3)
        yuki_second_show = int(yuki_second % 60)
        if 10 > yuki_second_show:
            yuki_second_show = "0"+str(yuki_second_show)
        return "{}:{}".format(int(yuki_second//60), yuki_second_show)

    def get_current(self):
        yuki_current, yuki_duration = self._enquiry("delta > timing")
        yuki_pointed = self._enquiry("delta > pointed position")
        if yuki_pointed is not None:
            yuki_current = yuki_duration*yuki_pointed
        yuki_markup = "{} / {}".format(
            self._get_readable(yuki_current),
            self._get_readable(yuki_duration)
            )
        return yuki_markup

    def __init__(self, parent):
        self._parent = parent
