
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from .Image import DeltaImage
from .PopoverModel import MODEL

TOOLTIP_TEMPLATE = "Audio Volume: {:.0%}"


class DeltaVolumeButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._popover.popup_for_align(button, 0.5, 0.1)

    def _delta_call_add_to_container(self, image):
        self.set_image(image)

    def _delta_call_volume_changed(self, volume):
        self.props.tooltip_text = TOOLTIP_TEMPLATE.format(volume)

    def __init__(self, parent):
        self._parent = parent
        self._popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        query = "player", "file_chooser_software_volume"
        volume = self._enquiry("delta > settings", query)
        self.props.tooltip_text = TOOLTIP_TEMPLATE.format(volume)
        DeltaImage(self)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
