
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gst
from gi.repository import GstVideo
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .Bus import DeltaBus
from .Progress import DeltaProgress
from .AudioVolume import DeltaAudioVolume


class DeltaPlaybin(DeltaEntity):

    def _delta_call_position_updated(self, user_data):
        self._transmitter.transmit(("position-updated", user_data))

    def _delta_call_state_changed(self, new_state):
        self._transmitter.transmit(("state-changed", new_state))

    def get_for_handle(self, handle):
        playbin = Gst.ElementFactory.make("playbin", "player")
        DeltaBus.new_for_playbin(self, playbin, handle)
        DeltaProgress.new_for_playbin(self, playbin)
        DeltaAudioVolume.new_for_playbin(self, playbin)
        return playbin

    def register_listener(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Gst.init()
