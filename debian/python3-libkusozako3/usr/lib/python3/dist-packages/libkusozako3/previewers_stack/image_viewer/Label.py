
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

FORMAT = "{} x {}"


class DeltaLabel(Gtk.Label, DeltaEntity):

    def set_pixbuf_data(self, pixbuf):
        label = FORMAT.format(pixbuf.get_width(), pixbuf.get_height())
        self.set_text(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, Unit(4))
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
