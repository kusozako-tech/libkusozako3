
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.audio_thumbnail.AudioThumbnail import FoxtrotAudioThumbnail

THUMBNAIL_SIZE = Unit(30)


class DeltaThumbnail(DeltaEntity):

    def get_pixbuf(self):
        return self._current_thumbnail

    def receive_transmission(self, user_data):
        type_, uri = user_data
        if type_ == "uri":
            self._current_thumbnail = self._audio_thumbnail.get_for_uri(uri)

    def __init__(self, parent):
        self._parent = parent
        self._audio_thumbnail = FoxtrotAudioThumbnail(THUMBNAIL_SIZE)
        self._current_thumbnail = None
        self._raise("delta > register uri object", self)
