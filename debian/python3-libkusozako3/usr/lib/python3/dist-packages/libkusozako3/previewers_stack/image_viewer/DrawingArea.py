
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _get_size(self, pixbuf):
        rectangle, _ = self.get_allocated_size()
        pixbuf_width = pixbuf.get_width()
        pixbuf_height = pixbuf.get_height()
        width_rate = rectangle.width / pixbuf_width
        height_rate = rectangle.height / pixbuf_height
        rate = min(width_rate, height_rate)
        return pixbuf_width*rate, pixbuf_height*rate

    def _get_source(self):
        pixbuf = self._enquiry("delta > pixbuf")
        if pixbuf is None:
            return
        width, height = self._get_size(pixbuf)
        return pixbuf.scale_simple(width, height, INTERP_TYPE)

    def _on_draw(self, drawing_area, cairo_context):
        source = self._get_source()
        if source is None:
            return
        Gdk.cairo_set_source_pixbuf(cairo_context, source, 0, 0)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True, vexpand=True)
        self.connect("draw", self._on_draw)
        self._raise("delta > add to container", self)
