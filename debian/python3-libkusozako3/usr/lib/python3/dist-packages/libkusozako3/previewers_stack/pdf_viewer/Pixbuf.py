
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from libkusozako3.Ux import Unit
from libkusozako3.Entity import DeltaEntity

FORMAT = cairo.Format.ARGB32


class DeltaPixbuf(DeltaEntity):

    def _get_surface(self, size, rate):
        width, height = int(size.width*rate), int(size.height*rate)
        return cairo.ImageSurface(FORMAT, width, height)

    def _paint_background(self, cairo_context, size, rate):
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.rectangle(0, 0, size.width*rate, size.height*rate)
        cairo_context.fill()

    def _get_pixbuf(self, surface, size, rate):
        geometries = 0, 0, size.width*rate, size.height*rate
        return Gdk.pixbuf_get_from_surface(surface, *geometries)

    def get_from_page(self, page):
        max_width = self._enquiry("delta > allocated width")-Unit(3)
        page_size = page.get_size()
        scale = max(Unit(16), max_width)/page_size.width
        surface = self._get_surface(page_size, scale)
        cairo_context = cairo.Context(surface)
        self._paint_background(cairo_context, page_size, scale)
        cairo_context.scale(scale, scale)
        page.render_for_printing(cairo_context)
        return self._get_pixbuf(surface, page_size, scale)

    def __init__(self, parent):
        self._parent = parent
