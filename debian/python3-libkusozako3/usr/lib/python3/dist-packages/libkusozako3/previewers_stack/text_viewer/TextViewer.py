
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity
from .TextLoader import FoxtrotTextLoader
from .SourceView import DeltaSourceView


class DeltaTextViewer(DeltaEntity):

    def _delta_info_buffer(self):
        return self._buffer

    def set_data(self, file_info, gio_file, mime_type):
        self._file.set_location(gio_file)
        FoxtrotTextLoader(self._buffer, self._file)
        language = self._language_manager.guess_language(gio_file.get_path())
        if language is not None:
            self._buffer.set_language(language)
        self._raise("delta > switch previewer to", "text-viewer")

    def __init__(self, parent):
        self._parent = parent
        self._language_manager = GtkSource.LanguageManager.get_default()
        self._file = GtkSource.File()
        self._buffer = GtkSource.Buffer.new()
        style_scheme_manager = GtkSource.StyleSchemeManager.get_default()
        style_scheme = style_scheme_manager.get_scheme("oblivion")
        self._buffer.set_style_scheme(style_scheme)
        DeltaSourceView(self)
