
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Metadata import FoxtrotMetadata


class DeltaTags(Gtk.Label, DeltaEntity):

    def reset_orientation(self):
        if self._enquiry("delta > is vertically long"):
            self.set_xalign(0.5)
            template = _("Title:\n{}\n\nArtist:\n{}\n\nAlbum:\n{}")
        else:
            self.set_xalign(0)
            template = _("Title: {}\nArtist: {}\nAlbum: {}")
        self.set_label(template.format(*self._meta_tags))

    def receive_transmission(self, user_data):
        type_, uri = user_data
        if type_ == "uri":
            self._meta_tags = self._metadata.get_from_uri(uri)
            self._raise("delta > queue draw")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "foobar", vexpand=True, wrap=True)
        self.set_xalign(0)
        self._meta_tags = "", "", ""
        self._metadata = FoxtrotMetadata()
        self._raise("delta > add to container", self)
        self._raise("delta > register uri object", self)
