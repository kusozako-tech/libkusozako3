
from gi.repository import GLib
from gi.repository import GtkSource


class FoxtrotTextLoader:

    def _on_progress(self, current, total, user_data):
        pass

    def _on_finished(self, source_object, result, callback_data, user_data):
        pass

    def __init__(self, buffer_, file_):
        yuki_loader = GtkSource.FileLoader.new(buffer_, file_)
        yuki_loader.load_async(
            GLib.PRIORITY_LOW, None,
            self._on_progress, (None,),
            self._on_finished, None,
            None,
            )
