
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Thumbnail import DeltaThumbnail

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class DeltaCoverArt(Gtk.DrawingArea, DeltaEntity):

    def _get_scaled_pixbuf(self, max_width, pixbuf):
        pixbuf_width = pixbuf.get_width()
        if max_width > pixbuf_width:
            return pixbuf
        scale = max_width/pixbuf_width
        new_width = pixbuf_width*scale
        new_height = pixbuf.get_height()*scale
        return pixbuf.scale_simple(new_width, new_height, INTERP_TYPE)

    def _on_draw(self, drawing_area, cairo_context):
        self._raise("delta > check resized")
        pixbuf = self._thumbnail.get_pixbuf()
        if pixbuf is None:
            return
        rectangle, _ = self.get_allocated_size()
        max_width = rectangle.width-Unit(3)
        pixbuf = self._get_scaled_pixbuf(max_width, pixbuf)
        x = (rectangle.width-pixbuf.get_width())/2
        y = Unit(1.5)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True)
        self.set_size_request(-1, Unit(33))
        self._thumbnail = DeltaThumbnail(self)
        self._raise("delta > add to container", self)
        self.connect("draw", self._on_draw)
