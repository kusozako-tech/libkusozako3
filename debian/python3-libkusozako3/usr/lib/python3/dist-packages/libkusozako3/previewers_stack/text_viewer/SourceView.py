
from gi.repository import Gtk
from gi.repository import GtkSource
from libkusozako3.Entity import DeltaEntity


class DeltaSourceView(Gtk.ScrolledWindow, DeltaEntity):

    def _on_populate_popup(self, widget, popup):
        popup.destroy()
        # print("popup ?", popup)
        return True

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        buffer_ = self._enquiry("delta > buffer")
        self._view = GtkSource.View.new_with_buffer(buffer_)
        self._view.set_property("wrap-mode", Gtk.WrapMode.WORD_CHAR)
        self._view.props.populate_all = False
        self._view.connect("populate-popup", self._on_populate_popup)
        self.add(self._view)
        self._raise("delta > add preview", (self, "text-viewer"))
