
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Poppler
from libkusozako3.Entity import DeltaEntity
from .Pixbuf import DeltaPixbuf


class DeltaDocument(DeltaEntity):

    def _timeout(self, document, path):
        page = document.get_page(self._index)
        if page is None or path != self._path:
            return GLib.SOURCE_REMOVE
        pixbuf = self._pixbuf.get_from_page(page)
        row_data = self._index, pixbuf
        self._raise("delta > append row data", row_data)
        self._index += 1
        if self._index > 100:
            return GLib.SOURCE_REMOVE
        return document.get_n_pages() > self._index

    def load(self, gio_file):
        document = Poppler.Document.new_from_gfile(gio_file)
        self._index = 0
        self._path = gio_file.get_path()
        GLib.timeout_add(100, self._timeout, document, gio_file.get_path())

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = DeltaPixbuf(self)
