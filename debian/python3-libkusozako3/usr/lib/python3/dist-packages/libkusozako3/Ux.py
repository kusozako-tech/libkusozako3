
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SCALE_FACTOR = 1
UNIT = 8*SCALE_FACTOR
TILE_SIZE = UNIT*16


def Unit(arg):
    return 8*SCALE_FACTOR*arg
