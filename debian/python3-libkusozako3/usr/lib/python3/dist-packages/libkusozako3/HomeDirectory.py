
from gi.repository import GLib


def shorten(directory):
    home = GLib.get_home_dir()
    if not directory.startswith(home):
        return directory
    if directory == home:
        return "~/"
    return "~"+directory[len(home):]


def lengthen(directory):
    home = GLib.get_home_dir()
    if not directory.startswith("~/"):
        return directory
    return home+directory[1:]
