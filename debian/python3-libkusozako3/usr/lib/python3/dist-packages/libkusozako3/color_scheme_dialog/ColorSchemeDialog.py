
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .DialogWindow import DeltaDialogWindow
from .buffer.Buffer import DeltaBuffer


class DeltaColorSchemeDialog(DeltaEntity):

    @classmethod
    def run_dialog(cls, parent):
        dialog = cls(parent)
        return dialog.run()

    def _delta_info_default_scheme(self):
        return self._buffer.get_default_scheme()

    def _delta_call_apply_buffer(self):
        self._buffer.apply()

    def _delta_call_buffer_changed(self, user_data):
        key, color = user_data
        self._buffer.set_color(key, color)

    def _delta_call_register_buffer_object(self, object_):
        self._buffer.register_object(object_)

    def run(self):
        dialog_window = DeltaDialogWindow(self)
        dialog_window.show_all()
        response_id = dialog_window.run()
        dialog_window.destroy()
        return response_id

    def __init__(self, parent):
        self._parent = parent
        self._buffer = DeltaBuffer(self)
