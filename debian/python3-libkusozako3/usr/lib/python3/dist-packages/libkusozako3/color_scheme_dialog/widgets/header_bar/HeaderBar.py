
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .main_header_bar.MainHeaderBar import DeltaMainHeaderBar
from .chooser_header_bar.ChooserHeaderBar import DeltaChooserHeaderBar
from .event_controllers.EventControllers import DeltaEventControllers


class DeltaHeaderBar(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_info_event_box(self):
        return self._event_box

    def __init__(self, parent):
        self._parent = parent
        self._event_box = Gtk.EventBox()
        Gtk.Stack.__init__(self)
        self._event_box.add(self)
        DeltaEventControllers(self)
        DeltaMainHeaderBar(self)
        DeltaChooserHeaderBar(self)
        self._raise("delta > add to container", self._event_box)
        self._raise("delta > css", (self, "chooser-header"))
