
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PRIMARY_THEME_COLOR = {
    "label": _("Primary Theme Color"),
    "fg_key": "primary_fg_color",
    "fg_default": "White",
    "bg_key": "primary_bg_color",
    "bg_default": "MediumSlateBlue"
    }

SECONDARY_THEME_COLOR = {
    "label": _("Secondary Theme Color"),
    "fg_key": "secondary_fg_color",
    "fg_default": "White",
    "bg_key": "secondary_bg_color",
    "bg_default": "#60728B"
    }

HIGHLIGHT_COLOR = {
    "label": _("Highlight Color"),
    "fg_key": "highight_fg_color",
    "fg_default": "White",
    "bg_key": "highlight_bg_color",
    "bg_default": "Orange"
    }

PRIMARY_SURFACE_COLOR = {
    "label": _("Primary Surface Color"),
    "fg_key": "primary_surface_fg_color",
    "fg_default": "Black",
    "bg_key": "primary_surface_bg_color",
    "bg_default": "White"
    }

SECONDARY_SURFACE_COLOR = {
    "label": _("Secondary Surface Color"),
    "fg_key": "secondary_surface_fg_color",
    "fg_default": "Black",
    "bg_key": "secondary_surface_bg_color",
    "bg_default": "LightGray"
    }

INFORMATION_COLOR = {
    "label": _("Information Color"),
    "fg_key": "information_fg_color",
    "fg_default": "White",
    "bg_key": "information_bg_color",
    "bg_default": "Blue"
    }

WARNING_COLOR = {
    "label": _("Warning Color"),
    "fg_key": "warning_fg_color",
    "fg_default": "White",
    "bg_key": "warning_bg_color",
    "bg_default": "Red"
    }

DEFAULT_COLORS = (
    PRIMARY_THEME_COLOR,
    SECONDARY_THEME_COLOR,
    HIGHLIGHT_COLOR,
    PRIMARY_SURFACE_COLOR,
    SECONDARY_SURFACE_COLOR,
    # INFORMATION_COLOR,
    # WARNING_COLOR
    )
