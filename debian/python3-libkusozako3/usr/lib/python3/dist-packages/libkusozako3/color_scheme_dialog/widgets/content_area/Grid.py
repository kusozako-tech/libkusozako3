
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .HeaderLabels import DeltaHeaderLabels
from .Rows import DeltaRows


class DeltaGrid(Gtk.Grid, DeltaEntity):

    def _delta_call_attach_to_grid(self, user_data):
        widget, geometries = user_data
        self.attach(widget, *geometries)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self, column_homogeneous=True)
        DeltaHeaderLabels(self)
        DeltaRows(self)
        self._raise("delta > add to stack named", (self, "main-content"))
        self._raise("delta > css", (self, "primary-surface-color-class"))
