
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .Model import DEFAULT_COLORS


class DeltaBuffer(DeltaEntity):

    def _set_color_data(self, key, default):
        query = "css", key, default
        color = self._enquiry("delta > settings", query)
        self._buffer[key] = color

    def apply(self):
        for key, value in self._buffer.items():
            self._raise("delta > settings transaction", ("css", key, value))
        self._raise("delta > settings queue", ("css", "queue", "queue"))

    def set_color(self, key, color):
        self._buffer[key] = color
        self._transmitter.transmit((key, color))

    def get_default_scheme(self):
        return DEFAULT_COLORS

    def register_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._buffer = {}
        for data in DEFAULT_COLORS:
            self._set_color_data(data["fg_key"], data["fg_default"])
            self._set_color_data(data["bg_key"], data["bg_default"])
