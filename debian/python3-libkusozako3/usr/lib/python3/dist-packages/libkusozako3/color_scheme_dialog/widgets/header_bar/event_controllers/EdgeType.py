
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Ux import Unit

BORDER_WIDTH = Unit(1)


class FoxtrotEdgeType:

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def get(self, x, y):
        rect, _ = self._widget.get_allocated_size()
        x_index = self._get_index(rect.width, x)
        y_index = self._get_index(rect.height, y)
        return self._edge[y_index][x_index]

    def __init__(self, widget, edge):
        self._widget = widget
        self._edge = edge
