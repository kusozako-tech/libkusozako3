
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser import FileManagerSignals
from .Action import AlfaAction


class DeltaModelChangeSortColumn(AlfaAction):

    SIGNAL = FileManagerSignals.MODEL_CHANGE_SORT_COLUMN

    def _action(self, user_data):
        model = self._enquiry("delta > base model")
        model.set_sort_column_id(*user_data)
