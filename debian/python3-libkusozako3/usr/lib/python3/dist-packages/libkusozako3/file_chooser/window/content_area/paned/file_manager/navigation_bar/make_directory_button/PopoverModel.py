
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "make-directory",
            "default": None,
            "query": "delta > current directory",
            "callback-message": "delta > directory created"
        }
    ]
}

MODEL = [MAIN_PAGE]
