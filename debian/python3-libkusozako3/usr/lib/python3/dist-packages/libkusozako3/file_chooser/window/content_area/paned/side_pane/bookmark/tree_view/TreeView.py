
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import BookmarkColumns
from libkusozako3.file_chooser import FileManagerSignals
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_insert_column(self, column):
        self.append_column(column)

    def _on_changed(self, tree_selection):
        model, tree_paths = tree_selection.get_selected_rows()
        tree_path = tree_paths[0]
        path = model[tree_path][BookmarkColumns.FULLPATH]
        param = FileManagerSignals.MOVE_DIRECTORY, path
        self._raise("delta > file manager signal", param)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        scrolled_window.add(self)
        self.set_headers_visible(False)
        self.set_can_focus(False)
        self.set_tooltip_column(BookmarkColumns.TOOLTIP)
        self._raise("delta > css", (self, "secondary-theme-color-class"))
        selection = self.get_selection()
        selection.connect("changed", self._on_changed)
        EchoColumns(self)
        self._raise(
            "delta > add to stack named",
            (scrolled_window, "bookmark")
            )
