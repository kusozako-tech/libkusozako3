
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaSelectButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > dialog response", Gtk.ResponseType.APPLY)

    def receive_transmission(self, user_data):
        signal, selected_paths = user_data
        if signal == FileManagerSignals.SELECTION_CHANGED:
            self.set_sensitive(selected_paths is not None)

    def __init__(self, parent):
        self._parent = parent
        label = self._enquiry("delta > model", "select-button-label")
        if label is None:
            label = _("Select")
        Gtk.Button.__init__(self, label, relief=Gtk.ReliefStyle.NONE)
        self.connect("clicked", self._on_clicked)
        self.set_sensitive(False)
        self._raise("delta > pack end", self)
        self._raise("delta > register file manager object", self)
        self._raise("delta > css", (self, "button-safe"))
