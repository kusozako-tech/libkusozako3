
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser import FileManagerSignals
from .Watcher import AlfaWatcher


class DeltaChanged(AlfaWatcher):

    SIGNAL = "changed"

    def _on_signal(self, fullpath=None):
        if fullpath is not None:
            param = FileManagerSignals.PATH_TO_SAVE_DEFINED, fullpath
            self._raise("delta > file manager signal", param)
        else:
            param = FileManagerSignals.CLEAR_SELECTION, None
            self._raise("delta > file manager signal", param)
