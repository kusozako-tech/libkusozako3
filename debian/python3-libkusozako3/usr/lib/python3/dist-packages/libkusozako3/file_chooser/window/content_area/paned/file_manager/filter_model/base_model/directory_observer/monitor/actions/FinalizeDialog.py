
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaFinalizeDialog(AlfaAction):

    SIGNAL = FileManagerSignals.FINALIZE_DIALOG

    def _action(self, param):
        self._raise("delta > cancel monitor")
