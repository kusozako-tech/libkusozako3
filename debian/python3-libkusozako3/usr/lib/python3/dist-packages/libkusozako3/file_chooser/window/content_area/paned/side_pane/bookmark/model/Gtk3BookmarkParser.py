
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import HomeDirectory


class DeltaGtk3BookmarkParser(DeltaEntity):

    def _get_bookmark_file_path(self):
        yuki_names = [GLib.get_user_config_dir(), "gtk-3.0", "bookmarks"]
        return GLib.build_filenamev(yuki_names)

    def _append_data_for_uri(self, uri):
        gio_file = Gio.File.new_for_uri(uri)
        if not gio_file.query_exists():
            return
        fullpath = gio_file.get_path()
        row_data = (
            "folder-symbolic",
            GLib.filename_display_basename(fullpath),
            fullpath,
            HomeDirectory.shorten(fullpath),
            "gtk3-bookmark"
            )
        self._raise("delta > append", row_data)

    def _parse(self, path):
        gio_file = Gio.File.new_for_path(path)
        stream = Gio.DataInputStream.new(gio_file.read(None))
        while True:
            line, length = stream.read_line_utf8(None)
            if line is None:
                break
            self._append_data_for_uri(line.split(" ")[0])

    def __init__(self, parent):
        self._parent = parent
        path = self._get_bookmark_file_path()
        if GLib.file_test(path, GLib.FileTest.EXISTS):
            self._parse(path)
