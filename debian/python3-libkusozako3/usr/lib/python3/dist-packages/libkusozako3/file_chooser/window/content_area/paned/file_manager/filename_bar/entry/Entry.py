
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .watchers.Watchers import EchoWatchers


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _delta_info_entry(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self, hexpand=True)
        self.set_margin_top(Unit(1))
        self.set_margin_bottom(Unit(1))
        self.set_margin_start(Unit(1))
        self.set_margin_end(Unit(1))
        EchoWatchers(self)
        default = self._enquiry("delta > model", "default")
        if default is not None:
            self.set_text(default)
        self._raise("delta > add to container", self)
