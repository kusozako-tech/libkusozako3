
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaParserParsingFinished(AlfaAction):

    SIGNAL = FileManagerSignals.PARSER_PARSING_FINISHED

    def _action(self, directory):
        self._raise("delta > cancel monitor")
        gio_file = Gio.File.new_for_path(directory)
        self._raise("delta > restart", gio_file)
