
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.file_chooser import FileManagerSignals
from .Watcher import AlfaWatcher


class DeltaActivate(AlfaWatcher):

    SIGNAL = "activate"

    def _on_signal(self, fullpath=None):
        if fullpath is not None:
            param = FileManagerSignals.PATH_TO_SAVE_DEFINED, fullpath
            self._raise("delta > file manager signal", param)
            self._raise("delta > dialog response", Gtk.ResponseType.APPLY)
