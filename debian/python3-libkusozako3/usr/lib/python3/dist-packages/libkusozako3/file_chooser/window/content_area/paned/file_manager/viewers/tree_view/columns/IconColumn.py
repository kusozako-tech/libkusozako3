
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Ux import Unit
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from .ColumnStripe import DeltaColumnStripe


class DeltaIconColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, iter_, column_stripe):
        file_info = model[iter_][FileManagerColumns.GIO_FILE_INFO]
        renderer.set_property("height", Unit(4))
        renderer.set_property("gicon", file_info.get_icon())
        tree_path = model.get_path(iter_)
        is_even = (tree_path[0] % 2 == 0)
        column_stripe.set_background(renderer, is_even)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(self, cell_renderer=renderer)
        column_stripe = DeltaColumnStripe(self)
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func, column_stripe)
