
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .parser.Parser import DeltaParser
from .monitor.Monitor import DeltaMonitor


class EchoDirectoryObserver(DeltaEntity):

    def __init__(self, parent):
        DeltaParser(parent)
        DeltaMonitor(parent)
