
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaSelection(DeltaEntity):

    def _on_changed(self, tree_selection):
        if not self._model_loaded:
            return
        model, tree_iter = tree_selection.get_selected()
        self._raise("delta > file selected", model[tree_iter])

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.PARSER_PARSING_STARTED:
            self._model_loaded = False
        if signal == FileManagerSignals.PARSER_PARSING_FINISHED:
            self._model_loaded = True

    def __init__(self, parent):
        self._parent = parent
        self._model_loaded = False
        tree_view = self._enquiry("delta > tree view")
        selection = tree_view.get_selection()
        selection.connect("changed", self._on_changed)
        self._raise("delta > register file manager object", self)
