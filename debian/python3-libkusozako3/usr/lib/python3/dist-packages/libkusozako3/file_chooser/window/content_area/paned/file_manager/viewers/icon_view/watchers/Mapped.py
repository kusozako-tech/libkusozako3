
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaMapped(DeltaEntity):

    def _timeout(self, id_, start_path, end_path):
        if self._id != id_:
            return GLib.SOURCE_REMOVE
        directory = self._enquiry("delta > current directory")
        signal_param = start_path, end_path, directory
        param = FileManagerSignals.MODEL_REQUEST_THUMBNAIL_TILE, signal_param
        self._raise("delta > file manager signal", param)
        return GLib.SOURCE_REMOVE

    def _prepare_for_transition(self, icon_view):
        self._id += 1
        visible_range = icon_view.get_visible_range()
        if visible_range is None:
            return
        start_path, end_path = visible_range
        id_ = self._id
        GLib.timeout_add(20, self._timeout, id_, start_path, end_path)
        return GLib.SOURCE_REMOVE

    def _on_map(self, icon_view):
        GLib.timeout_add(250, self._prepare_for_transition, icon_view)

    def __init__(self, parent):
        self._parent = parent
        self._id = 0
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect_after("map", self._on_map)
