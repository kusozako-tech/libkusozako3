
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.file_chooser import FileManagerColumns as Columns
from .RowDataFactory import AlfaRowDataFactory


class DeltaEdit(AlfaRowDataFactory):

    def _get_tree_row_for_gio_file(self, gio_file, directory):
        if directory != self._enquiry("delta > current directory"):
            return None
        query = Types.FULLPATH, gio_file.get_path()
        return self._enquiry("delta > tree row", query)

    def _reset_last_modified(self, tree_row, gio_file):
        try:
            file_info = gio_file.query_info("*", 0)
            tree_row[Columns.GIO_FILE_INFO] = file_info
            tree_row[Columns.GIO_FILE] = gio_file
            time_for_sort, readable_time = self._get_readable_time(file_info)
            tree_row[Columns.LAST_MODIFIED] = readable_time
            tree_row[Columns.LAST_MODIFIED_FOR_SORT] = time_for_sort
        except GLib.Error:   # when file lost shortly
            return

    def edit_for_changed_event(self, user_data):
        gio_file, directory = user_data
        tree_row = self._get_tree_row_for_gio_file(gio_file, directory)
        if tree_row is None:
            return
        self._reset_last_modified(tree_row, gio_file)
