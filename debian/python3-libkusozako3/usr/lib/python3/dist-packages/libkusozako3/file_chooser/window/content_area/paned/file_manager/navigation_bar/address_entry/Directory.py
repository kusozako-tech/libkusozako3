
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaDirectory(DeltaEntity):

    def _get_cannonical(self, path):
        if path == ".":
            return "/"
        gio_file = Gio.File.new_for_path(path)
        return gio_file.get_path()

    def _get_separated(self, path):
        directory = self._get_cannonical(GLib.path_get_dirname(path))
        if GLib.file_test(directory, GLib.FileTest.IS_DIR):
            return directory, GLib.path_get_basename(path)
        else:
            return None, ""

    def _parse(self, path):
        if GLib.file_test(path, GLib.FileTest.IS_DIR):
            return self._get_cannonical(path), ""
        else:
            return self._get_separated(path)

    def parse(self, path):
        """ parse path string to get directory name and filter keyword.
        Args:
            path name (str): path name to parse
        Returns:
            directory (str or None): directory to move.
            filter keyword (str): filter keyword to set.
        Note:
            if path has no valid directory or same as current directory,
            this function returns None.

            if path has no filter keyword, this function returns '',
            not None.
        """
        directory, filter = self._parse(path)
        if directory == self._enquiry("delta > current directory"):
            directory = None
        return directory, filter

    def __init__(self, parent):
        self._parent = parent
