
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileViewerType
from libkusozako3.file_chooser import FileManagerColumns
from .actions.Actions import EchoActions
from .watchers.Watchers import EchoWatchers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def _delta_info_icon_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > directory model"),
            pixbuf_column=FileManagerColumns.TILE_VISIBLE
            )
        scrolled_window.add(self)
        EchoActions(self)
        EchoWatchers(self)
        data = scrolled_window, FileViewerType.ICON_VIEW
        self._raise("delta > add to stack named", data)
