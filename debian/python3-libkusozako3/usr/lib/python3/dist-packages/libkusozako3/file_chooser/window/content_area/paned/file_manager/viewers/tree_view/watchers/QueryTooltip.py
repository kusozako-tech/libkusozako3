
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns


class DeltaQueryTooltip(DeltaEntity):

    def _get_tree_row(self, tree_view, x, y):
        _, border = tree_view.get_border()
        model = tree_view.get_model()
        try:
            tree_path, _, _, _ = tree_view.get_path_at_pos(x, y-border.top)
            return model[tree_path]
        except TypeError:
            # when pointed at header.
            return None

    def _on_query_tooltip(self, tree_view, x, y, keyboard_mode, tooltip):
        tree_row = self._get_tree_row(tree_view, x, y)
        if tree_row is None:
            return False
        tooltip.set_icon(tree_row[FileManagerColumns.THUMBNAIL])
        tooltip.set_text(tree_row[FileManagerColumns.NAME])
        return True

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.props.has_tooltip = True
        tree_view.connect_after("query-tooltip", self._on_query_tooltip)
