
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GdkPixbuf

DIRECTORY = 0
GIO_FILE_INFO = 1
GIO_FILE = 2
FULLPATH = 3
THUMBNAIL_STATUS = 4
THUMBNAIL = 5
TILE_SOURCE = 6
TILE_VISIBLE = 7
NAME = 8
NAME_FOR_SORT = 9
CONTENT_TYPE = 10
MIME = 11
LAST_MODIFIED = 12
LAST_MODIFIED_FOR_SORT = 13

NUMBER_OF_COLUMNS = 14

TYPES = (
    str,                # 0: DIRECTORY
    Gio.FileInfo,       # 1: GIO_FILE_INFO
    Gio.File,           # 2: GIO_FILE
    str,                # 3: FULLPATH
    int,                # 4: THUMBNAIL_STATUS
    GdkPixbuf.Pixbuf,   # 5: THUMBNAIL
    GdkPixbuf.Pixbuf,   # 6: TILE_SOURCE
    GdkPixbuf.Pixbuf,   # 7: TILE_VISIBLE
    str,                # 8: NAME
    str,                # 9: NAME_FOR_SORT (GLib Collate Key for File)
    str,                # 10: CONENT_TYPE
    str,                # 11: MIME_TYPE
    str,                # 12: lAST_MODIFIED
    int                 # 13: LAST_MODIFIED_FOR_SORT
    )
