
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals
from libkusozako3.previewers_stack.PreviewersStack import DeltaPreviewersStack
from .BackButton import DeltaBackButton


class DeltaPreviewers(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _show(self, tree_row):
        success = self._previewers_stack.try_show_preview(
            tree_row[FileManagerColumns.GIO_FILE_INFO],
            tree_row[FileManagerColumns.GIO_FILE],
            tree_row[FileManagerColumns.MIME]
            )
        if success:
            self._raise("delta > switch stack to", "previewers")
        else:
            self._raise("delta > switch stack to", "bookmark")

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal == FileManagerSignals.NEW_SELECTION:
            self._show(tree_row)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=Unit(2)
            )
        DeltaBackButton(self)
        self._previewers_stack = DeltaPreviewersStack(self)
        self._raise("delta > add to stack named", (self, "previewers"))
        self._raise("delta > register file manager object", self)
