
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from libkusozako3.file_chooser import FileManagerSignals
from .PopoverModel import MODEL


class DeltaMakeDirectoryButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button, popover):
        popover.popup_for_align(button, 0.5, 0.9)

    def _delta_call_directory_created(self, directory):
        param = FileManagerSignals.MOVE_DIRECTORY, directory
        self._raise("delta > file manager signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        image = Gtk.Image.new_from_icon_name(
            "folder-new-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        self.set_image(image)
        self.props.tooltip_text = _("Make Directory")
        popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        self.connect("clicked", self._on_clicked, popover)
        self._raise("delta > add to container", self)
