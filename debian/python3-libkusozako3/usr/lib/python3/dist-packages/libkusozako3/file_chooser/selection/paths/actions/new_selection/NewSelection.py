
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals
from .Filters import DeltaFilters


class DeltaNewSelection(AlfaAction):

    SIGNAL = FileManagerSignals.NEW_SELECTION

    def _check_file_permission(self, fullpath):
        gio_file = Gio.File.new_for_path(fullpath)
        file_info = gio_file.query_info("*", 0)
        if self._enquiry("delta > model", "read-write-only"):
            return file_info.get_attribute_boolean("access::can-write")
        if self._enquiry("delta > model", "allow-read-only"):
            True
        return file_info.get_attribute_boolean("access::can-read")

    def _try_select_path(self, fullpath):
        if self._check_file_permission(fullpath):
            self._raise("delta > replace selection", [fullpath])

    def _action(self, tree_row):
        if not self._filters.matches(tree_row):
            self._raise("delta > clear selection")
        else:
            fullpath = tree_row[FileManagerColumns.FULLPATH]
            self._try_select_path(fullpath)

    def _on_initialize(self):
        self._filters = DeltaFilters(self)
