
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaHomeButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        param = FileManagerSignals.MOVE_DIRECTORY, GLib.get_home_dir()
        self._raise("delta > file manager signal", param)

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        self.set_sensitive(directory != GLib.get_home_dir())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        image = Gtk.Image.new_from_icon_name(
            "go-home-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        self.set_image(image)
        self.props.tooltip_text = _("Home Directory")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
