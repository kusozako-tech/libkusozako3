
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ParserFileFound import DeltaParserFileFound
from .MonitorCreated import DeltaMonitorCreated
from .MonitorDeleted import DeltaMonitorDeleted
from .ModelChangeSortColumn import DeltaModelChangeSortColumn


class EchoActions:

    def __init__(self, parent):
        DeltaParserFileFound(parent)
        DeltaMonitorCreated(parent)
        DeltaMonitorDeleted(parent)
        DeltaModelChangeSortColumn(parent)
