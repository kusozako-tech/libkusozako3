
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns

SORT_COLUMN_IDs = (
    FileManagerColumns.NAME_FOR_SORT,
    FileManagerColumns.LAST_MODIFIED_FOR_SORT,
    FileManagerColumns.CONTENT_TYPE
    )


class DeltaSort(DeltaEntity):

    def _sort_by_is_directory(self, alfa_row, bravo_row, order):
        alfa = (alfa_row[FileManagerColumns.MIME] == "inode/directory")
        bravo = (bravo_row[FileManagerColumns.MIME] == "inode/directory")
        if alfa == bravo:
            return 0
        order_coefficient = 1 if order == 0 else -1
        return -1*order_coefficient if alfa > bravo else 1*order_coefficient

    def _sort_func(self, model, alfa_iter, bravo_iter, user_data=None):
        # implement Gtk.TreeIterCompareFunc
        column_id, order = model.get_sort_column_id()
        alfa_row = model[alfa_iter]
        bravo_row = model[bravo_iter]
        response = self._sort_by_is_directory(alfa_row, bravo_row, order)
        if response != 0:
            return response
        return 1 if alfa_row[column_id] > bravo_row[column_id] else -1

    def __init__(self, parent):
        self._parent = parent
        model = self._enquiry("delta > base model")
        for sort_column_id in SORT_COLUMN_IDs:
            model.set_sort_func(sort_column_id, self._sort_func)
