
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3.file_chooser import FileManagerSignals
from .CurrentDirectory import DeltaCurrentDirectory
from .side_pane.SidePane import DeltaSidePane
from .file_manager.FileManager import DeltaFileManager
from .event_controllers.EventControllers import DeltaEventControllers


class DeltaPaned(Gtk.Paned, DeltaEntity):

    def _delta_info_current_directory(self):
        return self._current_directory.directory

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_event_box(self):
        return self._event_box

    def __init__(self, parent):
        self._parent = parent
        self._event_box = Gtk.EventBox()
        DeltaEventControllers(self)
        self._raise("delta > add to container", self._event_box)
        Gtk.Paned.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_border_width(Unit(2))
        self.set_wide_handle(True)
        self.set_position(Unit(30))
        self._current_directory = DeltaCurrentDirectory(self)
        DeltaSidePane(self)
        DeltaFileManager(self)
        directory = self._current_directory.directory
        param = FileManagerSignals.MOVE_DIRECTORY, directory
        self._raise("delta > file manager signal", param)
        self._event_box.add(self)
        self._raise("delta > css", (self, "content-area"))
