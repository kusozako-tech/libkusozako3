
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MoveDirectory import DeltaMoveDirectory
from .ClearSelection import DeltaClearSelection
from .new_selection.NewSelection import DeltaNewSelection
from .PathToSaveDefined import DeltaPathToSaveDefined
from .FinalizeDialog import DeltaFinalizeDialog


class EchoActions:

    def __init__(self, parent):
        DeltaMoveDirectory(parent)
        DeltaClearSelection(parent)
        DeltaNewSelection(parent)
        DeltaPathToSaveDefined(parent)
        DeltaFinalizeDialog(parent)
