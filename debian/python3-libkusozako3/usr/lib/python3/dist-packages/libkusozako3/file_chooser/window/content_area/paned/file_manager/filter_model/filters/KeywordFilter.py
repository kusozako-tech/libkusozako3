
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals


class DeltaKeywordFilter(DeltaEntity):

    def matches(self, tree_row):
        if self._keyword == "":
            return True
        return self._keyword in tree_row[FileManagerColumns.NAME]

    def set_keyword(self, keyword):
        self._keyword = keyword

    def _set_keyword(self, keyword):
        self._keyword = keyword
        self._raise("delta > request refilter")

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.MOVE_DIRECTORY:
            self._set_keyword("")
        if signal == FileManagerSignals.FILTER_CHANGE_KEYWORD:
            self._set_keyword(param)

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
        self._raise("delta > register file manager object", self)
