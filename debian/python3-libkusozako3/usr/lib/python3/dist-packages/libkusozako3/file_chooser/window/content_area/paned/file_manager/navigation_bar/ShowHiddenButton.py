
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaShowHiddenButton(Gtk.Button, DeltaEntity):

    def _reset_image(self):
        if self._show_hidden:
            icon_name = "view-conceal-symbolic"
        else:
            icon_name = "view-reveal-symbolic"
        self._image.set_from_icon_name(icon_name, Gtk.IconSize.SMALL_TOOLBAR)

    def _on_clicked(self, button):
        self._show_hidden = not self._show_hidden
        self._reset_image()
        param = FileManagerSignals.VIEWERS_TOGGLE_HIDDEN, self._show_hidden
        self._raise("delta > file manager signal", param)

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False
        self._image = Gtk.Image()
        self._reset_image()
        Gtk.Button.__init__(
            self,
            image=self._image,
            relief=Gtk.ReliefStyle.NONE,
            tooltip_text=_("Toggle Show Hidden")
            )
        self._raise("delta > add to container", self)
        self.connect("clicked", self._on_clicked)
