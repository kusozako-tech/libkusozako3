
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.tile.ThumbnailTile import DeltaThumbnailTile
from libkusozako3.file_chooser import FileManagerSignals
from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerColumns as Columns
from libkusozako3.file_chooser import ThumbnailStatus as Status
from .QueueBuilder import DeltaQueueBuilder


class DeltaRequestThumbnail(AlfaAction):

    SIGNAL = FileManagerSignals.MODEL_REQUEST_THUMBNAIL_TILE

    def _set_thumbnail(self, tree_row):
        gio_file = tree_row[Columns.GIO_FILE]
        thumbnail, tile = self._thumbnail_tile.build_for_gio_file(gio_file)
        if thumbnail is None:
            tree_row[Columns.THUMBNAIL_STATUS] = Status.FAILED
        else:
            tree_row[Columns.THUMBNAIL] = thumbnail
            tree_row[Columns.TILE_SOURCE] = tile
            tree_row[Columns.TILE_VISIBLE] = tile.copy()
            tree_row[Columns.THUMBNAIL_STATUS] = Status.LOADED

    def _timeout(self, queue):
        if queue.empty():
            return GLib.SOURCE_REMOVE
        self._set_thumbnail(queue.get())
        return GLib.SOURCE_CONTINUE

    def _delta_call_queue_built(self, queue):
        GLib.timeout_add(10, self._timeout, queue)

    def _on_initialize(self):
        self._thumbnail_tile = DeltaThumbnailTile(self)
        self._queue_builder = DeltaQueueBuilder(self)

    def _action(self, param):
        start_path, end_path, directory = param
        self._queue_builder.build_async(start_path, end_path, directory)
