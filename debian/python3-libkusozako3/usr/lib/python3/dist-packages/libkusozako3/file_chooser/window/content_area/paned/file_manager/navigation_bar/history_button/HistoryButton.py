
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from libkusozako3.file_chooser import FileManagerSignals
from .Model import MODEL


class DeltaHistoryButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._popover.popup_for_align(button, 0.5, 1)

    def _delta_info_history(self):
        return self._history

    def _delta_call_history_menu_clicked(self, directory):
        param = FileManagerSignals.MOVE_DIRECTORY, directory
        self._raise("delta > file manager signal", param)

    def _action(self, directory):
        if directory in self._history:
            self._history.remove(directory)
        self._history.insert(0, directory)
        if len(self._history) > 12:
            self._history.pop()
        self.set_sensitive(len(self._history) > 1)

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal == FileManagerSignals.MOVE_DIRECTORY:
            self._action(directory)

    def __init__(self, parent):
        self._parent = parent
        self._history = []
        image = Gtk.Image.new_from_icon_name(
            "document-open-recent-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Recent")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register file manager object", self)
        self._popover = DeltaPopoverMenu.new_for_model(self, MODEL)
