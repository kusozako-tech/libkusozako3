
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import queue
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns as Columns
from libkusozako3.file_chooser import ThumbnailStatus as Status


class DeltaQueueBuilder(DeltaEntity):

    def _is_reload_needed(self, tree_row):
        if Status.UNLOADED != tree_row[Columns.THUMBNAIL_STATUS]:
            return False
        return True

    def _build_async(self, start_path, end_path):
        tree_row_queue = queue.Queue()
        filter_model = self._enquiry("delta > directory model")
        base_model = filter_model.get_model()
        if base_model is None:
            return
        while start_path.compare(end_path) != 1:
            child_path = filter_model.convert_path_to_child_path(start_path)
            tree_row = base_model[child_path]
            if self._is_reload_needed(tree_row):
                tree_row[Columns.THUMBNAIL_STATUS] = Status.PENDING
                tree_row_queue.put(tree_row)
            start_path.next()
        self._raise("delta > queue built", tree_row_queue)

    def build_async(self, start_path, end_path, directory):
        if directory == self._enquiry("delta > current directory"):
            GLib.idle_add(self._build_async, start_path, end_path)

    def __init__(self, parent):
        self._parent = parent
