
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODEL = [
    {
        "page-name": "main",
        "items": [
            {
                "type": "recent-paths",
                "display-type": "shorten",
                "message": "delta > history menu clicked",
                "user-data": None,
                "query": "delta > history",
                "query-data": None,
                "close-on-clicked": True
            }
        ]
    }
]
