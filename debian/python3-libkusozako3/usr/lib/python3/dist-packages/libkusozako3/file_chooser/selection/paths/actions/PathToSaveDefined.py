
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.file_chooser.Action import AlfaAction
from libkusozako3.file_chooser import FileManagerSignals


class DeltaPathToSaveDefined(AlfaAction):

    SIGNAL = FileManagerSignals.PATH_TO_SAVE_DEFINED

    def _get_parent_directory(self, fullpath):
        gio_file = Gio.File.new_for_path(fullpath)
        parent_directory = gio_file.get_parent()
        if parent_directory is None:
            return None
        if not parent_directory.query_exists():
            return None
        return parent_directory

    def _action(self, fullpath):
        parent_directory = self._get_parent_directory(fullpath)
        file_info = parent_directory.query_info("access::can-write", 0)
        if not file_info.get_attribute_boolean("access::can-write"):
            return
        self._raise("delta > replace selection", [fullpath])
