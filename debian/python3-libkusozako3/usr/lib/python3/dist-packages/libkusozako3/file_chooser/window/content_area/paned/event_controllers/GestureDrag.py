
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .EdgeType import FoxtrotEdgeType

BUTTON = 1  # LEFT BUTTON
EDGE_TYPES = [
    [Gdk.WindowEdge.NORTH_WEST, "default", Gdk.WindowEdge.NORTH_EAST],
    ["default", "default", "default"],
    [Gdk.WindowEdge.SOUTH_WEST, "default", Gdk.WindowEdge.SOUTH_EAST]
    ]


class DeltaGestureDrag(Gtk.GestureDrag, DeltaEntity):

    def _get_root_position(self, gesture_drag, widget):
        _, start_x, start_y = gesture_drag.get_start_point()
        return widget.get_window().get_root_coords(start_x, start_y)

    def _get_edge_type(self, gesture):
        _, start_x, start_y = gesture.get_start_point()
        return self._edge_type.get(start_x, start_y)

    def _on_drag_update(self, gesture, offset_x, offset_y, widget):
        edge_type = self._get_edge_type(gesture)
        root_x, root_y = self._get_root_position(gesture, widget)
        drag_context = BUTTON, root_x, root_y, Gdk.CURRENT_TIME
        window = widget.get_toplevel()
        if edge_type == "default":
            window.begin_move_drag(*drag_context)
        else:
            window.begin_resize_drag(edge_type, *drag_context)

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry("delta > event box")
        self._edge_type = FoxtrotEdgeType(event_source, EDGE_TYPES)
        Gtk.GestureDrag.__init__(self, widget=event_source)
        self.connect("drag-update", self._on_drag_update, event_source)
