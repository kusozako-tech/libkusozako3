
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileManagerSignals


class DeltaShowHiddenFilter(DeltaEntity):

    def matches(self, tree_row):
        if self._show_hidden:
            return True
        file_info = tree_row[FileManagerColumns.GIO_FILE_INFO]
        return not file_info.get_is_hidden()

    def receive_transmission(self, user_data):
        signal, show_hidden = user_data
        if signal == FileManagerSignals.VIEWERS_TOGGLE_HIDDEN:
            self._show_hidden = show_hidden
            self._raise("delta > request refilter")

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False
        self._raise("delta > register file manager object", self)
