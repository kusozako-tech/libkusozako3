
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.file_chooser import FileManagerSignals
from .Action import AlfaAction


class DeltaMonitorCreated(AlfaAction):

    SIGNAL = FileManagerSignals.MONITOR_CREATED

    def _action(self, user_data):
        _, gio_file, _, _, directory = user_data
        file_info = gio_file.query_info("*", Gio.FileQueryInfoFlags.NONE)
        signal_param = directory, file_info, gio_file
        param = FileManagerSignals.PARSER_FILE_FOUND, signal_param
        self._raise("delta > file manager signal", param)
