
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns


class DeltaQueryTooltip(DeltaEntity):

    def _on_query_tooltip(self, icon_view, x, y, keyboard_mode, tooltip):
        model = icon_view.get_model()
        bin_x, bin_y = icon_view.convert_widget_to_bin_window_coords(x, y)
        response = icon_view.get_item_at_pos(bin_x, bin_y)
        if response is None:
            return False
        tree_path, cell_renderer = response
        tree_row = model[tree_path]
        thumbnail = tree_row[FileManagerColumns.THUMBNAIL]
        tooltip.set_icon(thumbnail)
        tooltip.set_text(tree_row[FileManagerColumns.NAME])
        icon_view.set_tooltip_cell(tooltip, tree_path, cell_renderer)
        return True     # True means show tooltip

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.props.has_tooltip = True
        icon_view.connect_after("query-tooltip", self._on_query_tooltip)
