
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class AlfaWatcher(DeltaEntity):

    SIGNAL = "define signal to connect here."

    def _is_valid(self, entry):
        basename = entry.get_text()
        if basename == "":
            return False
        return "/" not in basename

    def _get_fullpath(self, entry):
        if not self._is_valid(entry):
            return None
        current_directory = self._enquiry("delta > current directory")
        fullpath = GLib.build_filenamev([current_directory, entry.get_text()])
        exists = GLib.file_test(fullpath, GLib.FileTest.EXISTS)
        # do not return existed path to avoid overwrite existed file.
        return None if exists else fullpath

    def _on_signal(self, fullpath=None):
        raise NotImplementedError

    def _callback(self, entry):
        fullpath = self._get_fullpath(entry)
        self._on_signal(fullpath)

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect(self.SIGNAL, self._callback)
