
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals
from .FileEnumeratorBuilder import DeltaFileEnumeratorBuilder

NUMBER_OF_FILES = 8


class DeltaParser(DeltaEntity):

    def _parse_file_infos(self, file_enumerator, file_infos, directory):
        for file_info in file_infos:
            gio_file = file_enumerator.get_child(file_info)
            signal_param = directory, file_info, gio_file
            param = FileManagerSignals.PARSER_FILE_FOUND, signal_param
            self._raise("delta > file manager signal", param)
        self._on_next_files(file_enumerator, directory)

    def _on_next_files_finished(self, file_enumerator, task, directory):
        file_infos = file_enumerator.next_files_finish(task)
        if not file_infos:
            param = FileManagerSignals.PARSER_PARSING_FINISHED, directory
            self._raise("delta > file manager signal", param)
            file_enumerator.close()
        else:
            self._parse_file_infos(file_enumerator, file_infos, directory)

    def _on_next_files(self, file_enumerator, directory):
        file_enumerator.next_files_async(
            NUMBER_OF_FILES,
            GLib.PRIORITY_DEFAULT,
            None,
            self._on_next_files_finished,
            directory
            )

    def _delta_call_file_enumerator_built(self, user_data):
        file_enumerator, directory = user_data
        self._on_next_files(file_enumerator, directory)

    def __init__(self, parent):
        self._parent = parent
        self._file_enumerator_builder = DeltaFileEnumeratorBuilder(self)
