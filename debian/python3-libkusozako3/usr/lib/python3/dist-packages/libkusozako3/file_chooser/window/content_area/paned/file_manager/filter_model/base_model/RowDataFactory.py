
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class AlfaRowDataFactory(DeltaEntity):

    def _get_name_for_sort(self, file_info):
        file_type = file_info.get_file_type()
        is_directory = (file_type == Gio.FileType.DIRECTORY)
        header = "__directory__" if is_directory else "__file__"
        return (header+file_info.get_name()).lower()

    def _get_readable_time(self, file_info):
        date_time = file_info.get_modification_date_time()
        date_time_local = date_time.to_local()
        last_modified = date_time_local.format("%F")
        now = GLib.DateTime.new_now_local()
        if last_modified != now.format("%F"):
            return date_time_local.to_unix(), last_modified
        return date_time_local.to_unix(), date_time_local.format("%T")

    def __init__(self, parent):
        self._parent = parent
