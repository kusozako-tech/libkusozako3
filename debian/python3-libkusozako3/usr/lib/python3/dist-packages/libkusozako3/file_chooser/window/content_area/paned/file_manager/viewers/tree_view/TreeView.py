
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerColumns
from libkusozako3.file_chooser import FileViewerType
from .columns.Columns import EchoColumns
from .watchers.Watchers import EchoWatchers


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_insert_column(self, column):
        self.append_column(column)

    def _delta_info_tree_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        model = self._enquiry("delta > directory model")
        Gtk.TreeView.__init__(self, model=model)
        scrolled_window.add(self)
        self.set_headers_visible(True)
        self.set_can_focus(True)
        # self.set_tooltip_column(FileManagerColumns.FULLPATH)
        self.set_hexpand(True)
        self.set_vexpand(True)
        EchoColumns(self)
        EchoWatchers(self)
        data = scrolled_window, FileViewerType.TREE_VIEW
        self._raise("delta > add to stack named", data)
