
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser import FileManagerSignals


class DeltaCurrentDirectory(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, directory = user_data
        if signal != FileManagerSignals.MOVE_DIRECTORY:
            return
        gio_file = Gio.File.new_for_path(directory)
        self._current_directory = gio_file.get_path()

    def _get_directory_from_model(self):
        directory = self._enquiry("delta > model", "directory")
        if directory is not None:
            return directory
        return GLib.get_home_dir()

    def __init__(self, parent):
        self._parent = parent
        id_ = self._enquiry("delta > model", "id")
        directory = self._get_directory_from_model()
        if id_ is not None:
            query = "file_chooser.{}".format(id_), "last-directory", directory
            directory = self._enquiry("delta > settings", query)
        self._current_directory = directory
        self._raise("delta > register file manager object", self)

    @property
    def directory(self):
        return self._current_directory
