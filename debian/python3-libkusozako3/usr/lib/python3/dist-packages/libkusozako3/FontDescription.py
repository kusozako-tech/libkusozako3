
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango

TEMPLATE = '{} {} {}px "{}","Sans Serif Mono"'
STYLES = {0: "Normal", 1: "Oblique", 2: "Italic"}
STYLES_KEYS = {"Normal": 0, "Oblique": 1, "Italic": 2}


def to_css_font(font_description):
    style = STYLES[int(font_description.get_style())]
    weight = str(int(font_description.get_weight()))
    size = int(font_description.get_size())/Pango.SCALE*(96/72)
    family = font_description.get_family()
    return TEMPLATE.format(style, weight, size, family)


def from_css_font(css_font):
    default = css_font.split(",")[0]
    elements = default.split(" ", 3)
    if len(elements) != 4 or not elements[2].endswith("px"):
        return None
    font_description = Pango.FontDescription()
    if elements[0] in STYLES_KEYS:
        font_description.set_style(STYLES_KEYS[elements[0]])
    weight = elements[1]
    if weight.isdigit():
        font_description.set_weight(int(weight))
    size = float(elements[2].replace("px", ""))
    font_description.set_size(int(size/(96/72)*Pango.SCALE))
    font_description.set_family(elements[3].replace('"', ""))
    return font_description
