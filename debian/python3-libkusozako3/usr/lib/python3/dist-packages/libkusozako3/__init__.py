
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi
import gettext
import locale

gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')
gi.require_version('PangoCairo', '1.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Poppler', '0.18')
gi.require_version('GtkSource', '4')
gi.require_version('WebKit2', '4.0')

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    "libkusozako3",
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )
