
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf


class FoxtrotThumbnail:

    def _get_mtime(self, path):
        gio_file = Gio.File.new_for_path(path)
        file_info = gio_file.query_info("time::modified", 0)
        return gio_file, file_info.get_attribute_uint64("time::modified")

    def _get_thumbnail_info(self, source_path, md5_checksum):
        path = "{}/{}.png".format(self._directory, md5_checksum)
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            return None, path
        _, source_mtime = self._get_mtime(source_path)
        thumbnail_gio_file, thumbnail_mtime = self._get_mtime(path)
        if source_mtime >= thumbnail_mtime:
            thumbnail_gio_file.delete(None)
            return None, path
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        return pixbuf, path

    def load_from_cache(self, source_path, md5_checksum):
        return self._get_thumbnail_info(source_path, md5_checksum)

    def __init__(self, thumbnail_type):
        names = [GLib.get_user_cache_dir(), "thumbnails", thumbnail_type]
        self._directory = GLib.build_filenamev(names)
        if GLib.file_test(self._directory, GLib.FileTest.EXISTS):
            return
        gio_file = Gio.File.new_for_path(self._directory)
        gio_file.make_directory_with_parents(None)
