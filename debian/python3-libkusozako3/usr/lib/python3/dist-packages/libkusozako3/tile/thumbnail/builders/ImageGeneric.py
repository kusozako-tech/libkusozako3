
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from .Image import AlfaImage


class FoxtrotImageGeneric(AlfaImage):

    def get_thumbnail(self, gio_file, thumbnail_path):
        path = gio_file.get_path()
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        thumbnail = self._get_scaled(gio_file, pixbuf)
        self._save_async(thumbnail, thumbnail_path)
        return thumbnail
