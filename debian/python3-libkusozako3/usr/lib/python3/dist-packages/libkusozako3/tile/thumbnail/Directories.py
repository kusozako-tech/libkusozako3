
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

CACHE_DIR = GLib.get_user_cache_dir()
NORMAL_DIR = GLib.build_filenamev([CACHE_DIR, "thumbnails", "kusozako_normal"])


class FoxtrotDirectories:

    def get_thumbnail_path(self, gio_file):
        uri = gio_file.get_uri()
        uri_by_bytes = bytes(uri, encoding="utf-8")
        self._checksum.update(uri_by_bytes)
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        return GLib.build_filenamev([NORMAL_DIR, md5_checksum+".png"])

    def _ensure_directory(self, path):
        gio_file = Gio.File.new_for_path(path)
        if not gio_file.query_exists():
            gio_file.make_directory_with_parents()

    def __init__(self):
        self._ensure_directory(NORMAL_DIR)
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
