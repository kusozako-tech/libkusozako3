
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from libkusozako3.Ux import Unit

ICON_SIZE = Unit(8)
TILE_SIZE = Unit(16)
FLAG = Gtk.IconLookupFlags.GENERIC_FALLBACK


class FoxtrotThemedIcon:

    def _get_icon_pixbuf(self, icon_name):
        try:
            pixbuf = self._icon_theme.load_icon(icon_name, ICON_SIZE, FLAG)
            return pixbuf
        except GLib.Error:
            # when suitable icon data not found in icon theme.
            return self._get_icon_pixbuf("text-x-generic")

    def get_icon_pixbuf(self,file_info):
        gio_icon = file_info.get_icon()
        icon_names = gio_icon.get_names()
        icon_name = icon_names[0]
        return self._get_icon_pixbuf(icon_name)

    def __init__(self):
        self._icon_theme = Gtk.IconTheme.get_default()
