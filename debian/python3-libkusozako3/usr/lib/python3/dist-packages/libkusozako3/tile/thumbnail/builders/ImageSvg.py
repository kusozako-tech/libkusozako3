
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Ux import Unit

THUMBNAL_SIZE = Unit(16)
INTERP = GdkPixbuf.InterpType.BILINEAR
OPTION_KEYS = [
    "tEXt::Kusozako::PrimaryInfo"
    ]


class FoxtrotImageSvg:

    def get_thumbnail(self, gio_file, thumbnail_path):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(gio_file.get_path())
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(1, max(THUMBNAL_SIZE/width, THUMBNAL_SIZE/height))
        thumbnail = pixbuf.scale_simple(width*scale, height*scale, INTERP)
        option_values = ["scalable"]
        thumbnail.savev(thumbnail_path, "png", OPTION_KEYS, option_values)
        return thumbnail
