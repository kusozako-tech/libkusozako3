
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .thumbnail.Thumbnail import FoxtrotThumbnail
from .BaseTile import FoxtrotBaseTile
# from .ThemedIcon import FoxtrotThemedIcon
from .labels.Labels import FoxtrotLabels

TILE_SIZE = Unit(16)
TILE_GEOMETRY = 0, 0, TILE_SIZE, TILE_SIZE


class DeltaThumbnailTile(DeltaEntity):

    def build_for_gio_file(self, gio_file):
        thumbnail = self._thumbnail.load_from_gio_file(gio_file)
        if thumbnail is None:
            return None, None
        # print(thumbnail.get_width(), thumbnail.get_height())
        # print(thumbnail.get_option("tEXt::Kusozako::PrimaryInfo"))
        # print("basename", gio_file.get_basename())
        file_info = gio_file.query_info("*", 0)
        base_tile_surface = self._base_tile.from_pixbuf(thumbnail)
        self._labels.paint(file_info, base_tile_surface)
        tile = Gdk.pixbuf_get_from_surface(base_tile_surface, *TILE_GEOMETRY)
        return thumbnail, tile

    def __init__(self, parent=None):
        self._parent = parent
        self._thumbnail = FoxtrotThumbnail()
        self._base_tile = FoxtrotBaseTile()
        self._labels = FoxtrotLabels()
