
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .BaseTile import FoxtrotBaseTile
from .ThemedIcon import FoxtrotThemedIcon
from .labels.Labels import FoxtrotLabels

TILE_SIZE = Unit(16)
TILE_GEOMETRY = 0, 0, TILE_SIZE, TILE_SIZE


class DeltaIconTile(DeltaEntity):

    def build_for_file_info(self, file_info):
        icon_pixbuf = self._themed_icon.get_icon_pixbuf(file_info)
        base_tile = self._base_tile.from_pixbuf(icon_pixbuf)
        self._labels.paint(file_info, base_tile)
        return Gdk.pixbuf_get_from_surface(base_tile, *TILE_GEOMETRY)

    def __init__(self, parent=None):
        self._parent = parent
        self._themed_icon = FoxtrotThemedIcon()
        self._base_tile = FoxtrotBaseTile()
        self._labels = FoxtrotLabels()
