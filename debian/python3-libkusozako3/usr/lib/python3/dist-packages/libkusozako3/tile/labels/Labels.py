
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileNameLabel import FoxtrotFileNameLabel
from .SymlinkTag import FoxtrotSymlinkTag
from .PermissionTag import FoxtrotPermissionTag


class FoxtrotLabels:

    def paint(self, file_info, surface):
        cairo_context = self._file_name_label.paint(file_info, surface)
        self._symlink_tag.paint(cairo_context, file_info)
        self._permission_tag.paint(cairo_context, file_info)

    def __init__(self):
        self._file_name_label = FoxtrotFileNameLabel()
        self._symlink_tag = FoxtrotSymlinkTag()
        self._permission_tag = FoxtrotPermissionTag()
