
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from libkusozako3.Ux import Unit
from .VideoContext import FoxtrotVideoContext
from .SafePath import FoxtrotSafePath
from .FFMpegThumbnailer import FoxtrotFFMpegThumbnailer

THUMBNAL_SIZE = Unit(16)
INTERP = GdkPixbuf.InterpType.BILINEAR


class FoxtrotVideo:

    def _get_scaled(self, gio_file, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(1, max(THUMBNAL_SIZE/width, THUMBNAL_SIZE/height))
        thumbnail = pixbuf.scale_simple(width*scale, height*scale, INTERP)
        # primary_info = "{}x{}".format(width, height)
        # thumbnail.set_option("tEXt::Kusozako::PrimaryInfo", primary_info)
        # thumbnail.set_option("tExt::Thumb::Width", str(width))
        # thumbnail.set_option("tExt::Thumb::Height", str(height))
        thumbnail.set_option("tEXt::Thumb::URI", gio_file.get_uri())
        file_info = gio_file.query_info("time::modified", 0)
        mtime = file_info.get_attribute_as_string("time::modified")
        thumbnail.set_option("tEXt::Thumb::MTime", mtime)
        return thumbnail

    def get_thumbnail(self, gio_file, thumbnail_path):
        output_path = self._safe_path.get_safe_path()
        input_path = gio_file.get_path()
        video_context = FoxtrotVideoContext(input_path, output_path)
        success = self._ffmpeg_thumbnailer.try_create_preview(video_context)
        if not success:
            return None
        pixbuf = video_context.get_pixbuf()
        thumbnail = self._get_scaled(gio_file, pixbuf)
        thumbnail.savev(thumbnail_path, "png", [], [])
        return thumbnail

    def __init__(self):
        self._safe_path = FoxtrotSafePath()
        self._ffmpeg_thumbnailer = FoxtrotFFMpegThumbnailer()
