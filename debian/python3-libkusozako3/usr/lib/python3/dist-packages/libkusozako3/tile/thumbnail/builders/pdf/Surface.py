
import cairo
from gi.repository import Gdk

COLORS = (0.9, 0.9, 0.9, 1)


class FoxtrotSurface:

    def get_pixbuf(self):
        return Gdk.pixbuf_get_from_surface(self._surface, 0, 0, *self._size)

    def __init__(self, size):
        self._size = size
        self._surface = cairo.ImageSurface(cairo.Format.ARGB32, *self._size)
        self._context = cairo.Context(self._surface)
        self._context.set_source_rgba(*COLORS)
        self._context.rectangle(0, 0, *size)
        self._context.fill()

    @property
    def cairo_context(self):
        return self._context
