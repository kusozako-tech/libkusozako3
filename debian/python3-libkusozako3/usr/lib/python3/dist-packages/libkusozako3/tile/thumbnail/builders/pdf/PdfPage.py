
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Poppler


class FoxtrotPdfPage:

    def render_to_context(self, cairo_context):
        self._page.render(cairo_context)

    def get_size_int(self):
        width, height = self._page.get_size()
        return int(width), int(height)

    def __init__(self, fullpath):
        self._page = None
        self._number_of_pages = 0
        gio_file = Gio.File.new_for_path(fullpath)
        pdf_document = Poppler.Document.new_from_gfile(gio_file, None)
        if pdf_document is not None:
            self._page = pdf_document.get_page(0)
            self._number_of_pages = pdf_document.get_n_pages()

    @property
    def number_of_pages(self):
        return self._number_of_pages

    @property
    def is_valid(self):
        return self._page is not None
