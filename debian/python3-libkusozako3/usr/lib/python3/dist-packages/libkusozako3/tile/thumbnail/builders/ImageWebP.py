
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import WebP
from .Image import AlfaImage


class FoxtrotImageWebP(AlfaImage):

    def get_thumbnail(self, gio_file, thumbnail_path):
        path = gio_file.get_path()
        pixbuf = WebP.to_pixbuf(path)
        thumbnail = self._get_scaled(gio_file, pixbuf)
        self._save_async(thumbnail, thumbnail_path)
        return thumbnail
