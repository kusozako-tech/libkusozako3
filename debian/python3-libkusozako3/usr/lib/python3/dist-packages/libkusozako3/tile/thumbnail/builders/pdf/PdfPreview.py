
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from kusozako_files import FileManagerColumnTypes
from .PdfPage import FoxtrotPdfPage
from .Surface import FoxtrotSurface


class FoxtrotPdfPreview:

    def _get_source_pixbuf(self, tree_row):
        # gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        gio_file = tree_row[2]
        path = gio_file.get_path()
        page = FoxtrotPdfPage(path)
        page_size = page.get_size_int()
        surface = FoxtrotSurface(page_size)
        page.render_to_context(surface.cairo_context)
        special_info = "< {} pages >".format(page.number_of_pages)
        return surface.get_pixbuf(), special_info

    def get_preview(self, tree_row):
        source_pixbuf, special_info = self._get_source_pixbuf(tree_row)
        if source_pixbuf is None:
            return None, None
        return source_pixbuf, special_info

    def __init__(self):
        pass
