
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from libkusozako3.Ux import Unit

TILE_SIZE = Unit(16)


class FoxtrotBaseTile:

    def from_pixbuf(self, pixbuf):
        surface = cairo.ImageSurface(cairo.Format.ARGB32, TILE_SIZE, TILE_SIZE)
        cairo_context = cairo.Context(surface)
        cairo_context.set_source_rgba(1, 1, 1, 0.25)
        cairo_context.rectangle(0, 0, TILE_SIZE, TILE_SIZE)
        cairo_context.fill()
        x = (TILE_SIZE-pixbuf.get_width())/2
        y = min(Unit(1), (TILE_SIZE-pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()
        return surface
