
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Pango
from gi.repository import PangoCairo
from libkusozako3.Ux import Unit
from libkusozako3.gtk_font import GtkFont

FONT_DESCRIPTION = GtkFont.get_description(relative_size=-1)
MAXIMUM_TEXT_HEIGHT = Unit(8)
MARGIN = Unit(0.5)
ICON_SIZE = Unit(8)
TILE_SIZE = Unit(16)
FLAG = Gtk.IconLookupFlags.GENERIC_FALLBACK
SHADE_COLOR_RGBA = 25/256, 25/256, 112/256, 0.65


class FoxtrotFileNameLabel:

    def _get_layout(self, cairo_context):
        layout = PangoCairo.create_layout(cairo_context)
        layout.set_alignment(Pango.Alignment.CENTER)
        layout.set_font_description(FONT_DESCRIPTION)
        layout.set_width((TILE_SIZE-MARGIN*2)*Pango.SCALE)
        layout.set_height(MAXIMUM_TEXT_HEIGHT*Pango.SCALE)
        layout.set_wrap(Pango.WrapMode.WORD_CHAR)
        layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        return layout.copy()

    def paint(self, file_info, cairo_surface):
        cairo_context = cairo.Context(cairo_surface)
        layout = self._get_layout(cairo_context)
        label_text = file_info.get_name()
        layout.set_markup(GLib.markup_escape_text(label_text, -1))
        _, inner_height = layout.get_pixel_size()
        outer_height = inner_height+MARGIN*2
        cairo_context.set_source_rgba(*SHADE_COLOR_RGBA)
        geometry = 0, TILE_SIZE-outer_height, TILE_SIZE, outer_height
        cairo_context.rectangle(*geometry)
        cairo_context.fill()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, TILE_SIZE-inner_height-MARGIN)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)
        return cairo_context
