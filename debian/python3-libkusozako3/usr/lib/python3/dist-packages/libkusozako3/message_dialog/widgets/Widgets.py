
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Image import DeltaImage
from .Label import DeltaLabel
from .ButtonBox import DeltaButtonBox


class EchoWidgets:

    def __init__(self, parent):
        DeltaImage(parent)
        DeltaLabel(parent)
        DeltaButtonBox(parent)
