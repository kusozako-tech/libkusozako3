
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaButtonBox(Gtk.Box, DeltaEntity):

    def _on_clicked(self, button, index):
        self._raise("delta > dialog response", index)

    def _build_button(self, label):
        button = Gtk.Button(label, relief=Gtk.ReliefStyle.NONE)
        button.set_size_request(-1, Unit(6))
        index = len(self.get_children())
        button.connect("clicked", self._on_clicked, index)
        self._raise("delta > css", (button, "button-safe"))
        self.add(button)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        # Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_size_request(-1, Unit(6))
        self.set_homogeneous(True)
        self.set_spacing(Unit(1))
        for label in self._enquiry("delta > model", "buttons"):
            self._build_button(label)
        self._raise("delta > add to container", self)
