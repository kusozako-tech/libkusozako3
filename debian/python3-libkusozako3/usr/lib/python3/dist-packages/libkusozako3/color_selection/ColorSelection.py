
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .DialogWindow import DeltaDialogWindow


class DeltaColorSelection(DeltaEntity):

    @classmethod
    def get_color(cls, parent, model):
        color_selection = cls(parent)
        gdk_rgba = color_selection.run_for_model(model)
        return gdk_rgba.to_string() if gdk_rgba is not None else None

    def _delta_info_model(self, key):
        return self._model.get(key, None)

    def _delta_call_rgba_changed(self, gdk_rgba):
        self._current_rgba = gdk_rgba

    def run_for_model(self, model):
        self._model = model
        dialog_window = DeltaDialogWindow(self)
        response_id = dialog_window.run()
        dialog_window.destroy()
        if response_id == Gtk.ResponseType.APPLY:
            return self._current_rgba
        return None

    def __init__(self, parent):
        self._parent = parent
        self._current_rgba = None
