
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaColorSelectionWidget(Gtk.ColorSelection, DeltaEntity):

    def _on_color_changed(self, color_selection):
        gdk_rgba = color_selection.get_current_rgba()
        self._raise("delta > rgba changed", gdk_rgba)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ColorSelection.__init__(self, margin=Unit(2))
        current_color = self._enquiry("delta > model", "current-color")
        if current_color is not None:
            gdk_rgba = Gdk.RGBA()
            gdk_rgba.parse(current_color)
            self.set_current_rgba(gdk_rgba)
        self.connect("color-changed", self._on_color_changed)
        self._raise("delta > add to container", self)
