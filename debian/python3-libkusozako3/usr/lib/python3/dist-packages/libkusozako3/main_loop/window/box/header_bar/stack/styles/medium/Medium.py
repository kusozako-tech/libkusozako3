
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .MenuButton import DeltaMenuButton
from ..title_label.TitleLabel import DeltaTitleLabel
from .IconfyButton import DeltaIconfyButton
from .MaximizeButton import DeltaMaximizeButton
from .CloseButton import DeltaCloseButton


class DeltaMedium(Gtk.Box, DeltaEntity):

    def _delta_call_pack_start(self, widget):
        self.pack_start(widget, False, False, 0)

    def _delta_call_pack_center(self, widget):
        self.set_center_widget(widget)

    def _delta_call_pack_end(self, widget):
        self.pack_end(widget, False, False, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaMenuButton(self)
        DeltaTitleLabel(self)
        DeltaCloseButton(self)
        DeltaMaximizeButton(self)
        DeltaIconfyButton(self)
        self._raise("delta > add to stack named", (self, "medium"))
