
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from .MimeFilter import DeltaMimeFilter


class DeltaFiles(DeltaEntity):

    def _try_appned_gio_file(self, gio_file):
        if not gio_file.query_exists():
            # print("delta >", gio_file.get_path(), "does not exist")
            return
        if not self._mime_filter.matches(gio_file):
            # print("delta > can't handle", gio_file.get_path())
            return
        self._files.append(gio_file)

    def get_files(self):
        return self._files

    def parse(self, command_line):
        options = command_line.get_options_dict()
        files = options.lookup_value(GLib.OPTION_REMAINING)
        if files is None:
            return
        for file_ in files:
            gio_file = command_line.create_file_for_arg(file_)
            self._try_appned_gio_file(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._files = []
        self._mime_filter = DeltaMimeFilter(self)
