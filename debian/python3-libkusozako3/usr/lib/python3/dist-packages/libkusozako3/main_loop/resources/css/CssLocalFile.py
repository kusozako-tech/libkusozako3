# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .SharedResource import FoxtrotSharedResource

CSS = "application.css"
ADDITIONAL_CSS = ".additional_application.css"


class DeltaCssLocalFile(DeltaEntity):

    def get_from_resource(self):
        return self._css

    def __init__(self, parent):
        self._parent = parent
        shared = FoxtrotSharedResource(CSS)
        local = self._enquiry("delta > resource string", ADDITIONAL_CSS)
        self._css = shared.get_content()+local
