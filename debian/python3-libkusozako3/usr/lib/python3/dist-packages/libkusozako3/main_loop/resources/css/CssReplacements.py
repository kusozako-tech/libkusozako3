
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .CssLocalFile import DeltaCssLocalFile
from .CssYaml import DeltaCssYaml


class DeltaCssReplacements(DeltaEntity):

    def _set_to_application(self, css):
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(bytes(css, "utf-8"))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )

    def _reload(self):
        css = self._local_file.get_from_resource()
        css = self._yaml.replace_all(css)
        self._set_to_application(css)

    def receive_transmission(self, user_data):
        group, _, _ = user_data
        if group == "css":
            self._reload()

    def __init__(self, parent):
        self._parent = parent
        self._yaml = DeltaCssYaml(self)
        self._local_file = DeltaCssLocalFile(self)
        self._reload()
        self._raise("delta > register settings object", self)
