
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaTitleSignal(DeltaEntity):

    def receive_transmission(self, user_data):
        type_, data = user_data
        if type_ != "title":
            return
        label = self._enquiry("delta > label")
        label.set_label(data)
        label.props.tooltip_text = data

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application window title object", self)
