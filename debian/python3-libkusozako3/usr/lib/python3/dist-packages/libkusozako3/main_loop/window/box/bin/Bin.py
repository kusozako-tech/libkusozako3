
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .event_controllers.EventControllers import DeltaEventControllers


class DeltaBin(Gtk.Bin, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_event_box(self):
        return self._event_box

    def __init__(self, parent):
        self._parent = parent
        self._event_box = Gtk.EventBox(hexpand=True, vexpand=True)
        Gtk.Bin.__init__(self, border_width=Unit(2))
        self._event_box.add(self)
        DeltaEventControllers(self)
        self._raise("delta > add to container", self._event_box)
        self._raise("delta > css", (self, "content-area"))
        self._raise("delta > loopback main window ready", self)
