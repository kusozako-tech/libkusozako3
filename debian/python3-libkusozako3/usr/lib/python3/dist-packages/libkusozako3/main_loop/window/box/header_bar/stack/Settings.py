
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaSettings(DeltaEntity):

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "header-bar" and key == "style":
            self._raise("delta > visible child name", value)

    def __init__(self, parent):
        self._parent = parent
        query = "header-bar", "style", "bold"
        settings = self._enquiry("delta > settings", query)
        self._raise("delta > visible child name", settings)
        self._raise("delta > register settings object", self)
