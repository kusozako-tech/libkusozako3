
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .styles.Styles import EchoStyles
from .WindowState import DeltaWindowState
from .Settings import DeltaSettings


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_info_visible_child_name(self):
        return self.get_visible_child_name()

    def _delta_call_visible_child_name(self, name):
        self.set_visible_child_name(name)

    def _on_realize(self, stack):
        DeltaSettings(self)
        DeltaWindowState(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            vhomogeneous=False,
            transition_type=Gtk.StackTransitionType.SLIDE_UP
            )
        EchoStyles(self)
        self.connect("realize", self._on_realize)
        self._raise("delta > add to container", self)
