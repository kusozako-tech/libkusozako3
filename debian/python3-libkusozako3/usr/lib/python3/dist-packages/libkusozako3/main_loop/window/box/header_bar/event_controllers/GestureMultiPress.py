
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import ApplicationSignals


class DeltaGestureMultiPress(Gtk.GestureMultiPress, DeltaEntity):

    def _on_pressed(self, gesture, n_press, x, y):
        if n_press == 2:
            param = ApplicationSignals.WINDOW_TOGGLE_MAXIMIZED, None
            self._raise("delta > action", param)

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry("delta > event box")
        Gtk.GestureMultiPress.__init__(
            self,
            widget=event_source,
            button=1
            )
        self.connect("pressed", self._on_pressed)
