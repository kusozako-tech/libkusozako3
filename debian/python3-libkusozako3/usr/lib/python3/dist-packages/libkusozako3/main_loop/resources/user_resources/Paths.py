
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from .FileManeuver import FoxtrotFileManeuver


class DeltaPaths(DeltaEntity):

    def _initialize_destination_directory(self):
        self._destination_directory = GLib.build_filenamev([
            GLib.get_user_config_dir(),
            self._enquiry("delta > application data", "id")
            ])
        gio_file = Gio.File.new_for_path(self._destination_directory)
        if not gio_file.query_exists():
            gio_file.make_directory()

    def copy(self, source_gio_file):
        self._file_manuever.copy_recursive_async(
            self._destination_directory,
            source_gio_file.get_path()
            )
        """
        basename = source_gio_file.get_basename()
        path = GLib.build_filenamev([self._destination_directory, basename])
        target_gio_file = Gio.File.new_for_path(path)
        if target_gio_file.query_exists():
            return
        source_gio_file.copy(target_gio_file, Gio.FileCopyFlags.ALL_METADATA)
        """

    def get_path_for_resource_name(self, resource_name):
        names = [self._destination_directory, resource_name]
        return GLib.build_filenamev(names)

    def __init__(self, parent):
        self._parent = parent
        self._file_manuever = FoxtrotFileManeuver()
        self._source_directory = self._enquiry("delta > resource path", "")
        self._initialize_destination_directory()

    @property
    def source_directory(self):
        return self._source_directory
