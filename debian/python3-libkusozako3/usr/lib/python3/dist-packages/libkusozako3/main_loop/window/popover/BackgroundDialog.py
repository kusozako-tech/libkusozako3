
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser

MODEL = {
    "type": "select-file",
    "id": "background-image",
    "title": _("Select Background Image"),
    "filters": {"PNG": "image/png", "JPEG": "image/jpeg", "JPG": "image/jpg"}
    }


class DeltaBackgroundDialog(DeltaEntity):

    @classmethod
    def run_dialog(cls, parent):
        dialog = cls(parent)
        dialog.choose_background()

    def choose_background(self):
        path = DeltaFileChooser.run_for_model(self, MODEL)
        if path is None:
            return
        data = "css", "window_background_image", path
        self._raise("delta > settings", data)

    def __init__(self, parent):
        self._parent = parent
