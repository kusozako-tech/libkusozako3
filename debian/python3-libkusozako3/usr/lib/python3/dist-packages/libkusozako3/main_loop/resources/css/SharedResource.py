# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotSharedResource:

    def get_content(self):
        if self._path is None:
            return ""
        _, bytes = GLib.file_get_contents(self._path)
        return bytes.decode("utf-8")

    def __init__(self, name):
        names = [GLib.path_get_dirname(__file__), "data", name]
        self._path = GLib.build_filenamev(names)
