# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from .PopoverModel import MODEL


class DeltaGestureSingle(Gtk.GestureSingle, DeltaEntity):

    def _on_begin(self, gesture, sequence, popover):
        popover.popup_for_gesture(gesture)

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry("delta > event source")
        Gtk.GestureSingle.__init__(self, widget=event_source, button=3)
        popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        self.connect("begin", self._on_begin, popover)
