
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotKeyFile:

    def _try_get_value(self, value):
        try:
            return eval(value)
        except (SyntaxError, NameError, TypeError):
            return value

    def set(self, group, key, value):
        self._key_file.set_value(group, key, str(value))

    def get(self, group, key, default=""):
        try:
            value = self._key_file.get_value(group, key)
            return self._try_get_value(value)
        except GLib.Error:
            self._key_file.set_value(group, key, str(default))
            return self._try_get_value(default)

    def save_to_file(self, path):
        self._key_file.save_to_file(path)

    def __init__(self, path):
        self._key_file = GLib.KeyFile()
        self._key_file.load_from_file(path, GLib.KeyFileFlags.NONE)
