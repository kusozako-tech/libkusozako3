
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity

QUERY = "unique-application"


class DeltaUnique(DeltaEntity):

    @classmethod
    def can_activate(cls, parent, application):
        unique = cls(parent)
        return unique.check(application)

    def _try_present_window(self, windows):
        for window in windows:
            window.present()
            gdk_window = window.get_window()
            if "move_to_current_desktop" in dir(gdk_window):
                gdk_window.move_to_current_desktop()

    def check(self, application):
        if not self._enquiry("delta > application data", QUERY):
            return True
        windows = application.get_windows()
        self._try_present_window(windows)
        return len(windows) == 0

    def __init__(self, parent):
        self._parent = parent
