
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MainModel import MODEL as Main
from .SettingsModel import MODEL as Settings
from .HeaderBarStyleModel import MODEL as HeaderBarStyle
from .BackgroundImageStyleModel import BACKGROUND_IMAGE_STYLE_MODEL

MODEL = [Main, Settings, HeaderBarStyle, BACKGROUND_IMAGE_STYLE_MODEL]
