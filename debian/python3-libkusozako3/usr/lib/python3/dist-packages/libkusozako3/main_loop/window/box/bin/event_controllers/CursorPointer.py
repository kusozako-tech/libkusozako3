
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk


class FoxtrotCursorPointer:

    def set_for_name(self, cursor_name):
        display = Gdk.Display.get_default()
        cursor = Gdk.Cursor.new_from_name(display, cursor_name)
        self._gdk_window.set_cursor(cursor)

    def __init__(self, widget):
        self._gdk_window = widget.get_window()
