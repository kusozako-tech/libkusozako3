
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3 import ApplicationSignals

ICON_NAME = "window-close-symbolic"
ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaCloseButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        param = ApplicationSignals.WINDOW_CLOSE, None
        self._raise("delta > action", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
            )
        self.props.tooltip_text = _("Close Window")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > pack end", self)
        self._raise("delta > css", (self, "popover-button"))
