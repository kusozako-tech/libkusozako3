
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .application_window.ApplicationWindow import DeltaApplicationWindow
from .actions.Actions import DeltaActions
from .popover.Popover import DeltaPopover
from .box.Box import DeltaBox


class DeltaWindow(DeltaEntity):

    def _delta_call_loopback_application_window_ready(self, parent):
        DeltaActions(parent)

    def _delta_call_loopback_actions_ready(self, parent):
        DeltaPopover(parent)

    def _delta_call_loopback_popover_ready(self, parent):
        DeltaBox(parent)

    def __init__(self, parent):
        self._parent = parent
        DeltaApplicationWindow(self)
