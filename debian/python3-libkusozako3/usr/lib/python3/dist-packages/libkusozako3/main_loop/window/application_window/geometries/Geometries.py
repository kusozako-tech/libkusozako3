
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .Size import DeltaSize
from .Position import DeltaPosition

MAXIMIZED_FLAG = Gdk.WindowState.MAXIMIZED
FULLSCREEN_FLAG = Gdk.WindowState.FULLSCREEN


class DeltaGeometries(DeltaEntity):

    def _on_delete_event(self, window, event):
        gdk_window = window.get_window()
        state = gdk_window.get_state()
        is_maximized = MAXIMIZED_FLAG == state & MAXIMIZED_FLAG
        maximized_data = "window", "is_maximized", is_maximized
        self._raise("delta > settings", maximized_data)
        is_fullscreen = FULLSCREEN_FLAG == state & FULLSCREEN_FLAG
        fullscreen_data = "window", "is_fullscreen", is_fullscreen
        self._raise("delta > settings", fullscreen_data)
        self._size.save()
        self._position.save()

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        self._size = DeltaSize(self)
        self._position = DeltaPosition(self)
        maximized_query = "window", "is_maximized", False
        if self._enquiry("delta > settings", maximized_query):
            window.maximize()
        fullscreen_query = "window", "is_fullscreen", False
        if self._enquiry("delta > settings", fullscreen_query):
            window.fullscreen()
        window.connect("delete-event", self._on_delete_event)
