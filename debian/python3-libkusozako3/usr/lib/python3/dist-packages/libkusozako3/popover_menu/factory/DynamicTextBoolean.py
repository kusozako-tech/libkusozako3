
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .PopoverButton import AlfaPopoverButton


class DeltaDynamicTextBoolean(AlfaPopoverButton):

    def _on_clicked(self, widget, item_model):
        self._raise(item_model["message"], item_model["user-data"])
        if item_model["close-on-clicked"]:
            self._raise("delta > popdown")

    def _on_realize(self, widget):
        self._reset_label()

    def _reset_label(self):
        state = self._enquiry(self._model["query"], self._model["query-data"])
        if state == self._model["check-value"]:
            label = self._model["title"]
        else:
            label = self._model["title-false"]
        self.set_label(label)
        align = self.get_child()
        align.set_halign(Gtk.Align.END)

    def receive_transmission(self, user_data):
        self._reset_label()

    def set_model(self, item_model):
        self._model = item_model
        self.connect("realize", self._on_realize)
        self.connect("clicked", self._on_clicked, item_model)
        self._try_register_shortcut(item_model)
        self._raise(item_model["registration"], self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("delta > css", (self, "popover-button"))
        image = Gtk.Image.new_from_icon_name(None, Gtk.IconSize.MENU)
        image.set_margin_end(8)
        self.set_image(image)
        self._raise("delta > add to container", self)
