
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from natsort import humansorted
from gi.repository import Gtk
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3.mime_type import MimeType
from .PopoverItem import DeltaPopoverItem


class DeltaOpenFileWith(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        recent_paths = cls(parent)
        recent_paths.set_model(model)

    def _clear_child_items(self):
        for child_item in self.get_children():
            child_item.destroy()

    def _get_sorted_app_infos(self, gio_file):
        app_info_unsorted = {}
        mime_type = MimeType.from_gio_file(gio_file)
        for app_info in Gio.AppInfo.get_all_for_type(mime_type):
            app_info_unsorted[app_info.get_name()] = app_info
        return humansorted(app_info_unsorted.items())

    def _add_child_items(self):
        gio_file = self._enquiry(self._model["gio-file-query"])
        for _, app_info in self._get_sorted_app_infos(gio_file):
            DeltaPopoverItem.new(self, app_info, gio_file)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _on_map(self, box):
        self._clear_child_items()
        self._add_child_items()
        self.show_all()

    def set_model(self, model):
        self._model = model

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
        self.connect("map", self._on_map)
