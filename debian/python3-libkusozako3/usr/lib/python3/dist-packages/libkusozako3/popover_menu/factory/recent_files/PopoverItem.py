
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3 import HomeDirectory
from libkusozako3 import ApplicationSignals
from .Text import FoxtrotText


class DeltaPopoverItem(Gtk.Button, DeltaEntity):

    @classmethod
    def new(cls, parent, item_model, directory):
        item = cls(parent)
        item.set_model(item_model, directory)

    def _on_clicked(self, widget, item_model, path):
        gio_file = Gio.File.new_for_path(path)
        param = ApplicationSignals.FILE_SELECTED, gio_file
        self._raise("delta > action", param)
        if item_model["close-on-clicked"]:
            self._raise("delta > popdown")

    def set_model(self, item_model, path):
        label = self._text.get_text(GLib.path_get_basename(path))
        self.set_label(label)
        self.connect("clicked", self._on_clicked, item_model, path)
        self.props.tooltip_text = HomeDirectory.shorten(path)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(None, Gtk.IconSize.MENU)
        image.set_margin_end(Unit(1))
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self._text = FoxtrotText(self)
        self.set_alignment(0, 0.5)
        self._raise("delta > css", (self, "popover-button"))
        self._raise("delta > add to container", self)
