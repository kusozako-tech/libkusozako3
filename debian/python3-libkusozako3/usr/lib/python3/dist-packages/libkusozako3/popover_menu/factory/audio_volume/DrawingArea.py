
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .Painters import DeltaPainters
from .EventControllers import DeltaEventControllers


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _delta_info_drawing_area(self):
        return self

    def _on_realize(self, widget):
        DeltaEventControllers(self)

    def receive_transmission(self):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.connect("realize", self._on_realize)
        DeltaPainters(self)
        self.set_size_request(Unit(2), Unit(16))
        self._raise("delta > add to container", self)
        self._raise("delta > register data object", self)
