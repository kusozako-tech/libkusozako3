# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
from .Page import DeltaPage

CLASSES = {
    "switcher": ("Switcher", "DeltaSwitcher"),
    "back-switcher": ("BackSwitcher", "DeltaBackSwitcher"),
    "simple-action": ("SimpleAction", "DeltaSimpleAction"),
    "check-action": ("CheckAction", "DeltaCheckAction"),
    "check-action-boolean": ("CheckActionBoolean", "DeltaCheckActionBoolean"),
    "hidable-action": ("HidableAction", "DeltaHidableAction"),
    "dynamic-text-boolean": ("DynamicTextBoolean", "DeltaDynamicTextBoolean"),
    "separator": ("Separator", "DeltaSeparator"),
    "vertical-separator": ("VerticalSeparator", "DeltaVerticalSeparator"),
    "label": ("Label", "DeltaLabel"),
    "scale": ("scale.Scale", "DeltaScale"),
    "recent-paths": ("recent_paths.RecentPaths", "DeltaRecentPaths"),
    "recent-files": ("recent_files.RecentFiles", "DeltaRecentFiles"),
    "audio-volume": ("audio_volume.AudioVolume", "DeltaAudioVolume"),
    "make-directory": ("make_directory.MakeDirectory", "DeltaMakeDirectory"),
    "named-box": ("NamedBox", "DeltaNamedBox"),
    "horizontal-box": ("HorizontalBox", "DeltaHorizontalBox"),
    "icon-button": ("IconButton", "DeltaIconButton"),
    "spin-box": ("spin_box.SpinBox", "DeltaSpinBox"),
    "open-file-with": ("open_file_with.OpenFileWith", "DeltaOpenFileWith"),
    "rename": ("rename.Rename", "DeltaRename"),
    }


class FoxtrotFactory:

    @classmethod
    def get_default(cls):
        if "_unique_factory" not in dir(cls):
            cls._unique_factory = cls()
        return cls._unique_factory

    def make_item(self, parent, item_model):
        type_ = item_model["type"]
        if type_ not in CLASSES:
            return
        module_name, class_name = CLASSES[type_]
        module_name = "libkusozako3.popover_menu.factory."+module_name
        class_ = getattr(importlib.import_module(module_name), class_name)
        class_.new_for_model(parent, item_model)

    def make_page(self, parent, page_model):
        page = DeltaPage.new_for_model(parent, page_model)
        for item_model in page_model["items"]:
            self.make_item(page, item_model)
