
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .Box import DeltaBox


class DeltaSpinBox(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        scale = cls(parent)
        scale.set_model(model)

    def _delta_info_model(self, key):
        return self._model.get(key, None)

    def set_model(self, model):
        self._model = model
        DeltaBox(self)

    def __init__(self, parent):
        self._parent = parent
