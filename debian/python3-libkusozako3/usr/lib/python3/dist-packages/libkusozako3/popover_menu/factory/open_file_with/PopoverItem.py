
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaPopoverItem(Gtk.Button, DeltaEntity):

    @classmethod
    def new(cls, parent, app_info, gio_file):
        item = cls(parent)
        item.set_model(app_info, gio_file)

    def _on_clicked(self, widget, app_info, gio_file):
        app_info.launch([gio_file], None)
        self._raise("delta > popdown")

    def set_model(self, app_info, gio_file):
        self.set_label(app_info.get_name())
        self.connect("clicked", self._on_clicked, app_info, gio_file)
        self.props.tooltip_text = app_info.get_description()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self.set_alignment(1, 0.5)
        self._raise("delta > css", (self, "popover-button"))
        self._raise("delta > add to container", self)
