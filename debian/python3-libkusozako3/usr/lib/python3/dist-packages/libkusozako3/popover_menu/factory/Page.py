
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaPage(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, page_model):
        page = DeltaPage(parent)
        page.set_model(page_model)
        return page

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def set_model(self, page_model):
        name = page_model["page-name"]
        self._raise("delta > add to stack named", (self, name))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            vexpand=True,
            hexpand=True,
            orientation=Gtk.Orientation.VERTICAL
            )
