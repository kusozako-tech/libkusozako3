
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaLabel(Gtk.Label, DeltaEntity):

    def receive_transmission(self):
        self.set_label(self._enquiry("delta > label"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_use_markup(True)
        self.set_hexpand(True)
        self.set_xalign(1)
        self.set_margin_top(Unit(1))
        self.set_margin_bottom(Unit(1))
        self.set_margin_start(Unit(1))
        self.set_margin_end(Unit(1))
        self.set_label(self._enquiry("delta > label"))
        self._raise("delta > add to container", self)
        self._raise("delta > register data object", self)
