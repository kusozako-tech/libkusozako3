
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .PopoverButton import AlfaPopoverButton


class DeltaBackSwitcher(AlfaPopoverButton):

    def _on_clicked(self, widget, item_model):
        self._raise(item_model["message"], item_model["user-data"])

    def _on_realize(self, widget):
        align = widget.get_child()
        align.set_halign(Gtk.Align.END)

    def set_model(self, item_model):
        self._model = item_model
        self.connect("realize", self._on_realize)
        self.set_label(item_model["title"])
        self.connect("clicked", self._on_clicked, item_model)
        if "tooltip" in item_model:
            self.props.tooltip_text = item_model["tooltip"]

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("delta > css", (self, "popover-button"))
        self._image = Gtk.Image.new_from_icon_name(
            "pan-start-symbolic",
            Gtk.IconSize.MENU
            )
        self.set_image(self._image)
        self._raise("delta > add to container", self)
