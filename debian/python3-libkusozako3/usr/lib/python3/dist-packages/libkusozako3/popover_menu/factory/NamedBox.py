
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaNamedBox(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        box = cls(parent)
        box.set_model(model)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def set_model(self, model):
        self._raise("delta > named box added", (self, model["box-id"]))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
