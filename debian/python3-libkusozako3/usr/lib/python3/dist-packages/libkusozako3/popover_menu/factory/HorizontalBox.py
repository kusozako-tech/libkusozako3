
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.factory.Factory import FoxtrotFactory


class DeltaHorizontalBox(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        box = cls(parent)
        box.set_model(model)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def set_model(self, model):
        factory = FoxtrotFactory.get_default()
        for item_model in model["model"]["items"]:
            factory.make_item(self, item_model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._raise("delta > add to container", self)
