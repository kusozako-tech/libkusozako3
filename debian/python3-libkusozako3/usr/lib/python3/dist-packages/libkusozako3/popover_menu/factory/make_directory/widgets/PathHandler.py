
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaPathHandler(DeltaEntity):

    def _get_fullpath(self, text):
        if text == "" or "/" in text:
            return False, None
        query = self._enquiry("delta > model", "query")
        directory = self._enquiry(query)
        fullpath = GLib.build_filenamev([directory, text])
        is_valid = not GLib.file_test(fullpath, GLib.FileTest.EXISTS)
        return is_valid, fullpath

    def set_basename(self, basename):
        self._is_valid, self._fullpath = self._get_fullpath(basename)
        return self._is_valid

    def try_make_directory(self):
        if not self._is_valid:
            return
        gio_file = Gio.File.new_for_path(self._fullpath)
        gio_file.make_directory(None)
        self._raise("delta > popdown")
        callback_message = self._enquiry("delta > model", "callback-message")
        if callback_message is not None:
            self._raise(callback_message, self._fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._is_valid = False
        self._fullpath = ""
