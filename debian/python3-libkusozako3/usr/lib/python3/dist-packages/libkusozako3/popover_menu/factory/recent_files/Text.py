
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango
from libkusozako3.Ux import Unit
from . import GtkFont

FONT_DESCRIPTION = GtkFont.get()
MAXIMUM_TEXT_HEIGHT = Unit(8)


class FoxtrotText:

    def _get_names(self, label):
        names = label.rsplit(".", 1)
        if len(names) > 0 and 8 > len(names[1]):
            return names[0], names[1]
        else:
            return label, ""

    def _get_ellipsised_text(self, label):
        basename, extension = self._get_names(label)
        while len(basename) >= 1:
            basename = basename[:-1]
            ellipsised = basename+"..."+extension
            self._layout.set_text(ellipsised, -1)
            if self._layout.get_line_count() == 1:
                return ellipsised
        return label

    def get_text(self, label):
        self._layout.set_text(label, -1)
        if self._layout.get_line_count() == 1:
            return label
        return self._get_ellipsised_text(label)

    def __init__(self, button):
        self._layout = button.create_pango_layout()
        self._layout.set_wrap(Pango.WrapMode.CHAR)
        self._layout.set_alignment(Pango.Alignment.LEFT)
        self._layout.set_font_description(FONT_DESCRIPTION)
        self._layout.set_width(Unit(60)*Pango.SCALE)
        self._layout.set_height(MAXIMUM_TEXT_HEIGHT*Pango.SCALE)
        self._layout.set_wrap(Pango.WrapMode.CHAR)
        self._layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
