
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea


class EchoUI:

    def __init__(self, parent):
        DeltaHeaderBar(parent)
        DeltaContentArea(parent)
