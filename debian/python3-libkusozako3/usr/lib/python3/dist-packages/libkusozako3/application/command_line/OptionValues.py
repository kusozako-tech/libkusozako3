
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaOptionValues(DeltaEntity):

    def _fetch(self, options):
        for option in options:
            if self._options.lookup_value(option) is not None:
                return True
        return False

    def get_is_non_gui(self):
        options = self._enquiry("delta > application data", "non-gui-options")
        if options is None:
            return False
        return self._fetch(options)

    def get_option(self, key=None):
        if key is None:
            key = GLib.OPTION_REMAINING
        value = self._options.lookup_value(key)
        return None if value is None else value.unpack()

    def parse(self, options):
        self._options = options

    def __init__(self, parent):
        self._parent = parent
