
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaMaximizeButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > main window toggle maximized")

    def _reset_tooltip_text(self):
        if self._enquiry("delta > application window is maximized"):
            self.props.tooltip_text = _("Unmaximize Window")
        else:
            self.props.tooltip_text = _("Maximize Window")

    def receive_transmission(self, state):
        self._reset_tooltip_text()

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            "window-maximize-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self._reset_tooltip_text()
        self.connect("clicked", self._on_clicked)
        self._raise("delta > pack end", self)
        self._raise("delta > css", (self, "popover-button"))
        self._raise("delta > register window state object", self)
