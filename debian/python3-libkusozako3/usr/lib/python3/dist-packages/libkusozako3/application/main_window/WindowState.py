
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Transmitter import FoxtrotTransmitter
from libkusozako3.Entity import DeltaEntity

MAXIMIZED = Gdk.WindowState.MAXIMIZED
FULLSCREEN = Gdk.WindowState.FULLSCREEN


class DeltaWindowState(DeltaEntity):

    def _on_window_state_event(self, widget, event):
        gdk_window = widget.get_window()
        new_state = gdk_window.get_state()
        self._is_maximized = (MAXIMIZED == new_state & MAXIMIZED)
        self._is_fullscreen = (FULLSCREEN == new_state & FULLSCREEN)
        self._transmitter.transmit(new_state)

    def register_listener(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._is_fullscreen = False
        self._is_maximized = False
        window = self._enquiry("delta > application window")
        window.connect("window-state-event", self._on_window_state_event)
        self._transmitter = FoxtrotTransmitter()

    @property
    def is_maximized(self):
        return self._is_maximized

    @property
    def is_fullscreen(self):
        return self._is_fullscreen
