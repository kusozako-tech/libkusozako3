
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.application.action.Action import DeltaAction
from .WindowState import DeltaWindowState


class DeltaStatusLayer(DeltaEntity):

    def _delta_call_main_window_toggle_maximized(self):
        if self._state._is_maximized:
            self._window.unmaximize()
        else:
            self._window.maximize()

    def _delta_call_main_window_toggle_fullscreen(self):
        if self._state.is_fullscreen:
            self._window.unfullscreen()
        else:
            self._window.fullscreen()

    def _delta_call_register_window_state_object(self, object_):
        self._state.register_listener(object_)

    def _delta_info_application_window_is_maximized(self):
        return self._state.is_maximized

    def _delta_info_application_window_is_fullscreen(self):
        return self._state.is_fullscreen

    def _delta_call_application_window_iconify(self):
        self._window.iconify()

    def __init__(self, parent):
        self._parent = parent
        self._window = self._enquiry("delta > application window")
        self._state = DeltaWindowState(self)
        DeltaAction(self)
