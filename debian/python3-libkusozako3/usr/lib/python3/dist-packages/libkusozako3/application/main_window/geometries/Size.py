
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit


class DeltaSize(DeltaEntity):

    def save(self):
        window = self._enquiry("delta > application window")
        w, h = window.get_size()
        size_data = "window", "size", [w, h]
        self._raise("delta > settings", size_data)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        query = "window", "size", [Unit(100), Unit(80)]
        size = self._enquiry("delta > settings", query)
        window.set_default_size(size[0], size[1])
