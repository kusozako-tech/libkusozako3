
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from gi.repository import Gtk
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from ..file_paths.FilePaths import DeltaFilePaths
from .Options import DeltaOptions
from .Unique import DeltaUnique


class DeltaCommandLine(Gtk.Application, DeltaEntity):

    def _delta_info_command_line_option(self, key=None):
        return self._options.get_option(key)

    def _delta_info_command_line_files(self):
        return self._options.get_files()

    def _delta_info_non_gui(self):
        return self._options.get_is_non_gui()

    def _delta_call_application_force_quit(self):
        self.quit()

    def _delta_info_gtk_application(self):
        return self

    def _delta_call_add_main_option(self, user_data):
        self.add_main_option(*user_data)

    def _on_activate(self, application):
        if DeltaUnique.can_activate(self, application):
            DeltaFilePaths(self)

    def __init__(self, parent):
        self._parent = parent
        self._non_gui = False
        Gtk.Application.__init__(
            self,
            application_id=self._enquiry("delta > application data", "id"),
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE
            )
        self._options = DeltaOptions(self)
        self.connect("activate", self._on_activate)
        self.run(sys.argv)
