
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .event_controllers.EventControllers import DeltaEventControllers
from .stack.Stack import DeltaStack


class DeltaHeaderBar(Gtk.EventBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_info_event_box(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Bin.__init__(self)
        DeltaEventControllers(self)
        DeltaStack(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "kusozako-headerbar"))
