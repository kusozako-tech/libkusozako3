
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaAccelKey(DeltaEntity):

    def _on_key_pressed(self, event_controller_key, keyval, keycode, state):
        if not self._enquiry("delta > accel enabled"):
            return
        label = Gtk.accelerator_get_label(keyval, state)
        if label in self._registry:
            self._raise("delta > action", self._registry[label])

    def _on_key_press(self, widnow, event):
        if not self._enquiry("delta > accel enabled"):
            return
        label = Gtk.accelerator_get_label(event.keyval, event.state)
        if label in self._registry:
            self._raise("delta > action", self._registry[label])

    def register_action(self, action_id, shortcut):
        self._registry[shortcut] = action_id

    def __init__(self, parent):
        self._parent = parent
        self._registry = {}
        window = self._enquiry("delta > application window")
        self._controller_key = Gtk.EventControllerKey.new(window)
        self._controller_key.connect("key-pressed", self._on_key_pressed)
