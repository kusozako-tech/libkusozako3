
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaNonGuiOptions(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
