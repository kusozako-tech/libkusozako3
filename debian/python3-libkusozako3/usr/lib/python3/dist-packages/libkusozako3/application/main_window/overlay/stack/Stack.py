
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Transparent import DeltaTransparent
from .InactiveShade import DeltaInactiveShade


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        print("stack-child", user_data)
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)
        self.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            transition_duration=200
            )
        DeltaTransparent(self)
        DeltaInactiveShade(self)
        self.set_visible_child_name("inactive-shade")
        self._raise("delta > add to container", self)
