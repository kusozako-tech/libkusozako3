
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODEL = {
    "page-name": "main",
    "items": [
        {
            "type": "named-box",
            "box-id": "main"
        },
        {
            "type": "switcher",
            "title": _("Settings"),
            "message": "delta > switch stack to",
            "user-data": "settings",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Iconify"),
            "message": "delta > application window iconify",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "dynamic-text-boolean",
            "title": _("Maximize"),
            "title-false": _("Unmaximize"),
            "message": "delta > action",
            "user-data": "main-window.toggle-maximized",
            "shortcut": "Shift+F11",
            "close-on-clicked": True,
            "registration": "delta > register window state object",
            "query": "delta > application window is maximized",
            "query-data": None,
            "check-value": False
        },
        {
            "type": "dynamic-text-boolean",
            "title": _("Fullscreen"),
            "title-false": _("Unfullscreen"),
            "message": "delta > action",
            "user-data": "main-window.toggle-fullscreen",
            "shortcut": "F11",
            "close-on-clicked": True,
            "registration": "delta > register window state object",
            "query": "delta > application window is fullscreen",
            "query-data": None,
            "check-value": False
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("About"),
            "message": "delta > about",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Quit"),
            "message": "delta > action",
            "user-data": "main-window.close",
            "shortcut": "Ctrl+Q",
            "close-on-clicked": True
        }
    ]
}
