
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from libkusozako3.application.popover.Popover import DeltaPopover


class DeltaPropertyAction(DeltaEntity):

    def _delta_call_property_action(self, user_data):
        # action, property = user_data
        self._transmitter.transmit(user_data)

    def _delta_call_register_property_action_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaPopover(self)
