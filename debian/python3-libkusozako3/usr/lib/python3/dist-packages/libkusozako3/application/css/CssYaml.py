
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml
from libkusozako3.Entity import DeltaEntity
from .SharedResource import FoxtrotSharedResource

SHARED_RESOURCE = "css_replacements.yaml"
RESOURCE = "additional_css_replacements.yaml"


class DeltaCssYaml(DeltaEntity):

    def __getitem__(self, key):
        return self._yaml[key] if key in self._yaml else None

    def _replace(self, css, item):
        query = "css", item["key"], item["default"]
        settings = self._enquiry("delta > settings", query)
        if settings == "NAGATO_NULL_SETTINGS":
            return css
        replacement = item["template"].format(settings)
        return css.replace(item["target"], replacement)

    def replace_all(self, css):
        for item in self._yaml:
            css = self._replace(css, item)
        return css

    def __init__(self, parent):
        self._parent = parent
        shared = FoxtrotSharedResource(SHARED_RESOURCE)
        local = self._enquiry("delta > resource string", RESOURCE)
        data = shared.get_content()+local
        self._yaml = yaml.safe_load(data)
