
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity


class DeltaWindowState(DeltaEntity):

    def receive_transmission(self, state):
        if Gdk.WindowState.FULLSCREEN == state & Gdk.WindowState.FULLSCREEN:
            self._raise("delta > visible child name", "none")
        else:
            query = "header-bar", "style", "bold"
            settings = self._enquiry("delta > settings", query)
            self._raise("delta > visible child name", settings)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register window state object", self)
