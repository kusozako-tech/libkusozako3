
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Ux import Unit
from libkusozako3.Entity import DeltaEntity
from .EventHandler import DeltaEventHandler
from .PathSignal import DeltaPathSignal
from .TitleSignal import DeltaTitleSignal


class DeltaTitleLabel(Gtk.Label, DeltaEntity):

    def _delta_info_label(self):
        return self

    def _delta_info_event_source(self):
        return self._event_box

    def __init__(self, parent):
        self._parent = parent
        self._event_box = Gtk.EventBox()
        Gtk.Label.__init__(
            self,
            self._enquiry("delta > application window title"),
            ellipsize=Pango.EllipsizeMode.MIDDLE
            )
        self.set_size_request(-1, Unit(4))
        DeltaEventHandler(self)
        DeltaTitleSignal(self)
        DeltaPathSignal(self)
        self._event_box.add(self)
        self._raise("delta > pack center", self._event_box)
