
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .GestureSingle import DeltaGestureSingle


class DeltaEventHandler(DeltaEntity):

    def _copy(self):
        label = self._enquiry("delta > label")
        gdk_display = Gdk.Display.get_default()
        clipboard = Gtk.Clipboard.get_default(gdk_display)
        clipboard.set_text(label.get_text(), -1)

    def _on_populate_popup(seflf, label, menu):
        menu.destroy()

    def _delta_call_copy_to_clipboard(self):
        self._copy()

    def _on_copy_clipboard(self, label):
        self._copy()

    def __init__(self, parent):
        self._parent = parent
        label = self._enquiry("delta > label")
        # poplate-popup signal should be removed in GTK4
        label.connect("populate-popup", self._on_populate_popup)
        DeltaGestureSingle(self)
