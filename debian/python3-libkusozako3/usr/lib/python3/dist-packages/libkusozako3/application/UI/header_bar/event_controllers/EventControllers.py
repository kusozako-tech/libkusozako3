
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from .GestureDrag import DeltaGestureDrag
from .GestureMultiPress import DeltaGestureMultiPress
from .EventControllerMotion import DeltaEventControllerMotion


class DeltaEventControllers(DeltaEntity):

    def _on_realize(self, widget):
        window = widget.get_window()
        window.set_events(Gdk.EventMask.POINTER_MOTION_MASK)
        self._drag = DeltaGestureDrag(self)
        self._motion = DeltaEventControllerMotion(self)
        self._multi_press = DeltaGestureMultiPress(self)

    def __init__(self, parent):
        self._parent = parent
        event_box = self._enquiry("delta > event box")
        event_box.connect("realize", self._on_realize)
