
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .widgets.Widgets import DeltaWidgets


class DeltaDialogWindow(Gtk.Dialog, DeltaEntity):

    def _delta_call_dialog_response(self, response):
        if response == Gtk.ResponseType.APPLY:
            self._raise("delta > apply buffer")
        else:
            self.response(response)

    def _delta_call_add_to_container(self, widget):
        container = self.get_content_area()
        container.add(widget)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        Gtk.Dialog.__init__(self, "", window, Gtk.ResponseType.CANCEL)
        content_area = self.get_content_area()
        content_area.set_border_width(0)
        self.set_decorated(False)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_keep_above(True)
        DeltaWidgets(self)
