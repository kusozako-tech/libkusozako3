
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from ..title_label.TitleLabel import DeltaTitleLabel
from .MenuButton import DeltaMenuButton


class DeltaBold(Gtk.Box, DeltaEntity):

    def _delta_call_pack_start(self, widget):
        self.pack_start(widget, False, False, 0)

    def _delta_call_pack_center(self, widget):
        self.set_center_widget(widget)

    def _delta_call_pack_end(self, widget):
        self.pack_end(widget, False, False, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_border_width(Unit(1))
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.pack_start(box, False, False, 0)
        DeltaTitleLabel(self)
        DeltaMenuButton(self)
        self._raise("delta > register action widget container", box)
        self._raise("delta > add to stack named", (self, "bold"))
