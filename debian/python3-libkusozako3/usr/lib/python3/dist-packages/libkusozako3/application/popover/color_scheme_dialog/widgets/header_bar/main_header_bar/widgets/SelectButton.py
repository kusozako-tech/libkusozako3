
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaSelectButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > dialog response", Gtk.ResponseType.APPLY)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, _("Apply"), relief=Gtk.ReliefStyle.NONE)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > pack end", self)
        self._raise("delta > css", (self, "button-safe"))
