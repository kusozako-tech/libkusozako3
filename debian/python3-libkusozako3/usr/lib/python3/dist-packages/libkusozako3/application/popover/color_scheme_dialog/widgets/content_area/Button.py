
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

FORMAT = cairo.Format.ARGB32
WIDTH = Unit(6)
HEIGHT = Unit(4)


class DeltaButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new(cls, parent, key, default):
        button = cls(parent)
        button.set_model(key, default)
        return button

    def _on_clicked(self, button, key):
        self._raise("delta > color button clicked", (key, self._color))

    def _get_surface_for_color(self, color):
        surface = cairo.ImageSurface(FORMAT, WIDTH, HEIGHT)
        cairo_context = cairo.Context(surface)
        gdk_rgba = Gdk.RGBA()
        gdk_rgba.parse(color)
        Gdk.cairo_set_source_rgba(cairo_context, gdk_rgba)
        cairo_context.rectangle(0, 0, WIDTH, HEIGHT)
        cairo_context.fill()
        return surface

    def receive_transmission(self, user_data):
        key, color = user_data
        if self._key != key:
            return
        self._color = color
        image = self.get_image()
        image.set_from_surface(self._get_surface_for_color(color))

    def set_model(self, key, default):
        self._key = key
        self._color = self._enquiry("delta > settings", ("css", key, default))
        surface = self._get_surface_for_color(self._color)
        image = Gtk.Image.new_from_surface(surface)
        self.set_image(image)
        self.connect("clicked", self._on_clicked, key)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_margin_top(Unit(1))
        self.set_margin_bottom(Unit(1))
        self.set_margin_right(Unit(1))
        self._raise("delta > register buffer object", self)
