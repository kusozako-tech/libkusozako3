
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity

QUERY = "show-filename-on-header"


class DeltaPathSignal(DeltaEntity):

    def _set_path(self, path):
        label = self._enquiry("delta > label")
        application_name = self._enquiry("delta > application data", "name")
        if path is not None:
            basename = GLib.path_get_basename(path)
            # label.set_label(basename)
            # label.props.tooltip_text = path
            label_title = "{} - {}".format(basename, application_name)
        else:
            # label.set_label(application_name)
            # label.props.tooltip_text = application_name
            label_title = application_name
        self._raise("delta > application window title", label_title)

    def receive_transmission(self, user_data):
        type_, data = user_data
        if type_ == "path":
            self._set_path(data)

    def __init__(self, parent):
        self._parent = parent
        if self._enquiry("delta > application data", QUERY):
            self._raise("delta > register application file path object", self)
