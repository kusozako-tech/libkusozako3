# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .EdgeType import FoxtrotEdgeType
from .CursorPointer import FoxtrotCursorPointer

EDGE_TYPES = [
    ["on", "on", "on"],
    ["on", "off", "on"],
    ["on", "on", "on"]
    ]


class DeltaGestureSingle(Gtk.GestureSingle, DeltaEntity):

    def _on_begin(self, gesture, sequence):
        _, start_x, start_y = gesture.get_point(sequence)
        if "off" == self._edge_type.get(start_x, start_y):
            return
        self._cursor_pointer.set_for_name("default")
        popover = self._enquiry("delta > application popover")
        popover.popup_for_gesture(gesture)

    def __init__(self, parent):
        self._parent = parent
        event_source = self._enquiry("delta > event box")
        self._edge_type = FoxtrotEdgeType(event_source, EDGE_TYPES)
        self._cursor_pointer = FoxtrotCursorPointer(event_source)
        Gtk.GestureSingle.__init__(self, widget=event_source, button=3)
        self.connect("begin", self._on_begin)
